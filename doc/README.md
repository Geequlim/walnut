To build Walnut documentation
=======================

1. Install [Doxygen](http://www.stack.nl/~dimitri/doxygen/download.html)
2. Install [Graphviz](http://www.graphviz.org/Download.php) (dot tool)
    * If you don't want to generate the class diagrams this is not needed as long as you disable the `CLASS_DIAGRAMS` option to `false`. 
3. Run doxygen with doxyfile. 

```bash
$ doxygen doxyfile
```

---

The theme was cloned from [victorbotamedi/doxygen_with_bootstrap_theme](https://github.com/victorbotamedi/doxygen_with_bootstrap_theme)
