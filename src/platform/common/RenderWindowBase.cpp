//
//  RenderWindowBase.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/9.
//
//

#include "common.h"
#include <thread>

namespace Walnut
{
    RenderWindowBase::RenderWindowBase():super(),m_inputSource(this)
    {
        m_size               = zeroSize;
        m_actived            = false;
        m_pHostWnd           = nullptr;
        m_renderRatio        = 0U;
        m_lastRenderDuration = seconds_t{0};
    }

    RenderWindowBase::~RenderWindowBase()
    {
        m_pHostWnd = nullptr;
    }

    void RenderWindowBase::render()
    {
        auto start =  std::chrono::high_resolution_clock::now() ;
        //Check render context
      //  if(  makeContextCurrent() )
        {
            renderContent();//rendering

            m_lastRenderDuration = (std::chrono::high_resolution_clock::now() - start);
            if(m_renderRatio)
            {
                seconds_t requireDuration{ 1.0 / m_renderRatio };
                if( m_lastRenderDuration < requireDuration )
                {
                    std::this_thread::sleep_for( requireDuration - m_lastRenderDuration );
                    m_lastRenderDuration = std::chrono::high_resolution_clock::now() - start;
                }
            }
        }
    }
}
