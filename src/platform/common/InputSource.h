#ifndef INPUTSOURCE_H
#define INPUTSOURCE_H

namespace Walnut
{
        class RenderWindowBase;
        
        struct InputSource
        {
            vector<int> pressedKeys;
            vector<Point> touchers;
            Point mousePosition;
            
            InputSource(RenderWindowBase* window) { m_targetWindow = window; }
            ~InputSource() { m_targetWindow = nullptr; }
            
            void keyDown(int keyCode);
            void keyUp(int keyCode);
            void mouseButtonDown(int btn,int x , int y);
            void mouseButtonUp(int btn, int x , int y);
            void mouseMoved(int x, int y);
    private:
                RenderWindowBase * m_targetWindow;
        };
}

#endif // INPUTSOURCE_H
