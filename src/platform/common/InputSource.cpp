#include "platform/platform.h"

namespace Walnut
{
    void InputSource::keyDown(int keyCode)
    {
        if(m_targetWindow)
        {
                if(m_pressedKeys.end() == std::find(m_pressedKeys.begin(),m_pressedKeys.end(),keyCode) )
                {
                    KeyboardEvent ke(KeyboardEvent::KeyDown,keyCode,false,m_pressedKeys);
                    m_targetWindow->dispatchEvent(ke);
                    m_pressedKeys.push_back(keyCode);
                    
                    std::stringstream ss;
                    ss<<"Key "<<(Byte)keyCode <<" down with keys[";
                    for( Byte c : ke.addKeys() )
                        ss<< c<<" ";
                    ss<<"] ";
                    string log = ss.str();
                    LOGI("%s",log.c_str());
                }
                else
                {
                    KeyboardEvent ke(KeyboardEvent::KeyRepeat,keyCode,false,m_pressedKeys);
                    m_targetWindow->dispatchEvent(ke);
                    std::stringstream ss;
                    ss<<"Key "<<(Byte)keyCode <<" repeat with keys[";
                    for( Byte c : ke.addKeys() )
                        ss<< c<<" ";
                    ss<<"] ";
                    string log = ss.str();
                    LOGI("%s",log.c_str());
                }
            }
    }
    
    void InputSource::keyUp(int keyCode)
    {
        if(m_targetWindow)
        {
                auto it = std::find(m_pressedKeys.begin(),m_pressedKeys.end(),keyCode) ;
                if( it != m_pressedKeys.end()  )
                {
                    m_pressedKeys.erase(it);
                    KeyboardEvent ke(KeyboardEvent::KeyUp,keyCode,false,m_pressedKeys);
                    m_targetWindow->dispatchEvent(ke);
                    
                    std::stringstream ss;
                    ss<<"Key "<<(Byte)keyCode <<" up with keys[";
                    for( Byte c : ke.addKeys() )
                        ss<< c<<" ";
                    ss<<"] ";
                    string log = ss.str();
                    LOGI("%s",log.c_str());
                }
        }
    }
    
    void InputSource::mouseButtonDown(int btn,int x,int y)
    {
        
    }
    void InputSource::mouseButtonUp(int btn,int x,int y)
    {
        
    }
    
    void InputSource::mouseMoved(int x,int y)
    {
            
    }
    
}

