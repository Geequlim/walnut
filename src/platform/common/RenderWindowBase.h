//
//  RenderWindowBase.h
//  Walnut
//
//  Created by Geequlim on 15/8/9.
//
//

#ifndef __Walnut__RenderWindowBase__
#define __Walnut__RenderWindowBase__

namespace Walnut
{
    /// A window for rendering
    class RenderWindowBase : public EventDispatcher
    {
        friend class CoreEngine;
        using super = EventDispatcher ;
    protected:
        /// Window size
        Size m_size;    
        /// Window is actived 
        bool m_actived;  
        /// Native window pointer
        void* m_pHostWnd;
        /// The frequency of rendering in one second
        unsigned m_renderRatio;
        /// The duration during last rendering in seconds
        seconds_t m_lastRenderDuration;
        /// Input source
        InputSource m_inputSource;
        
    public:
        
        /// Default construction
        RenderWindowBase();
        
        virtual ~RenderWindowBase();
        
        /// Disable copy constructor
        RenderWindowBase(const RenderWindowBase&) = delete;
        
        /**
         *  @brief Get window size in pixel
         *  @return The size of the window
         */
        inline const Size& size()const  { return m_size;}
        
        /** Check if is the window is actived */
        virtual inline bool actived()const{return m_actived;}
        
        /**
         * @brief Set is window actived
         * @param actived Is the window is actived
         */
        virtual inline void setActived(bool actived){ m_actived = actived; }
        
        /*!
         @brief  Get writable native window pointer
         @return The pointer to native window object
         */
        inline void* hostWindow(){return m_pHostWnd;}
        
        /*!
         @brief  Get read only native window pointer
         @return The pointer to native window object
         */
        inline const void* hostWindow()const{ return m_pHostWnd;}
        
        /*!
         @brief  Get The frequency of rendering in one second
         @return The render ratio(in Hz,render time per second)
         */
        inline unsigned renderRatio()const { return m_renderRatio;}
        
        /*!
         @brief  Set The frequency of rendering in one second
         @param ratio The render ratio(in Hz,render time per second)
         */
        inline void setRenderRatio(unsigned ratio){ m_renderRatio = ratio; }
        
        /// Render the window
        void render();
        
        /// Get input source of the window         
        InputSource& inputSource(){  return m_inputSource; }
        
    protected:

        /*!
         @brief Initialize the window
         @return Is initialization succeed
         */
        virtual bool initialize() = 0;

        /*!
         @brief  Checkout context for rendering
         @return Is the render context valid
         */
        virtual bool makeContextCurrent() = 0;
        
        /// Render window content
        virtual void renderContent() = 0;

        /// Swapbuffers
        virtual void swapBuffers() = 0;
    };
}

#endif
