//
//  Language.h
//  Walnut
//
//  Created by Geequlim on 15/8/17.
//
//

#ifndef Walnut_Language_h
#define Walnut_Language_h

namespace Walnut
{
    /// Enumeration for languages
    enum class Language : unsigned short
    {
		/// Unknown, Unknown
        Unknown,    
		/// English, US
        en_US,      
		///  English, Britain
        en_GB,     
		/// Chinese, PRC
        zh_CN,      
		/// Chinese, Taiwan
        zh_TW,      
		/// Danish, Denmark
        da_DK,   
		/// French, France
        fr_FR,     
		///  German, Germany
        de_DE,     
		///  Greek, Greece
        el_GR,     
		///  Italian, Italy
        it_IT,      
		///  Japanese, Japan
        ja_JP,     
		///  Korean, Korea
        ko_KR,     
		///  Polish, Poland
        pl_PL,      
		///  Portuguese, Portugal
        pt_PT,      
		///  Romanian, Romania
        ro_RO,      
		///  Russian, Russia
        ru_RU,     
		///  Spanish, Spain
        es_ES,     
		///  Thai, Thailand
        th_TH       
    };
    
    /*!
      @brief Get current language of the system
      @return Return the language value for current system
     */
    Language systemLanguage();
}

#endif
