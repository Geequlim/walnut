//
//  desktop.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/11.
//
//

#include "platform/platform.h"
#if __GTK__
    #include <gtk/gtk.h>
#endif
namespace Walnut
{
    // Initialize desktop configrations
    bool platformInitialize(void*)
    {
        bool succeed = glfwInit();
        if(succeed)
        {
        #ifdef __USE_GLES__
            glfwWindowHint(GLFW_CLIENT_API,GLFW_OPENGL_ES_API);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
        #else
            glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
            glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
        #endif    
            glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
            glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
            glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
            glfwSetErrorCallback([](int code,const char* msg){ throw Exception(msg,code); });
            #if __GTK__
                gtk_init(nullptr,nullptr);
            #endif
        }
        return succeed;
    }

    // Uninitialize desktop configrations
    void platformUnnitialize(void*)
    {
        #if __GTK__
            iterateGTKEvents();
        #endif
        glfwTerminate();
    }

    // Set system clipboard text string
    void platformSetClipboardString(const string& str)
    {
        if(engine && engine->initialized())
            glfwSetClipboardString( nullptr ,str.c_str());
    }

    // Get system clipboard text string
    string platformGetClipboardString()
    {
        string result = nullstr;
        if(engine && engine->initialized())
            result = glfwGetClipboardString(nullptr);
        return result;
    }
    
    // Poll events from platform
    void pollPlatformEvents()
    {
        glfwPollEvents();
    }
    
#if __GTK__
    void iterateGTKEvents()
    {
        while (gtk_events_pending())
        gtk_main_iteration();
    }
#endif
}
