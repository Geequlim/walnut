#include "platform/platform.h"
namespace Walnut
{
    Language systemLanguage()
    {
			Language result;
			LCID localeID = GetUserDefaultLCID();
			unsigned short lang = localeID & 0xff;
			switch (lang)
			{
			case LANG_ENGLISH:
				result = Language::en_US;
				break;
			case LANG_BENGALI:
				result = Language::en_GB;
				break;
			case LANG_CHINESE_SIMPLIFIED:
				result = Language::zh_CN;
				break;
			case LANG_CHINESE_TRADITIONAL:
				result = Language::zh_TW;
				break;
			case LANG_FRENCH:
				result = Language::fr_FR;
				break;
			case LANG_JAPANESE:
				result = Language::ja_JP;
				break;
			case LANG_RUSSIAN:
				result = Language::ru_RU;
				break;
			case LANG_KOREAN:
				result = Language::ko_KR;
				break;
			case LANG_THAI:
				result = Language::th_TH;
				break;
			case LANG_SPANISH:
				result = Language::es_ES;
				break;
			case LANG_GREEK:
				result = Language::el_GR;
				break;
			case LANG_GERMAN:
				result = Language::de_DE;
				break;
			case LANG_POLISH:
				result = Language::pl_PL;
				break;
			case LANG_ROMANIAN:
				result = Language::ro_RO;
				break;
			case LANG_PORTUGUESE:
				result = Language::pt_PT;
				break;
			default:
				result = Language::Unknown;
				break;
			}
			return result;
    }
}