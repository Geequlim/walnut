#include "platform/platform.h"

namespace Walnut
{
    Language systemLanguage()
    {
        return Language::en_US;
    }
}