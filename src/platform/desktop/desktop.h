//
//  desktop.h
//  Walnut
//
//  Created by Geequlim on 15/8/9.
//
//

#ifndef Walnut_desktop_h
#define Walnut_desktop_h

#include "../common/common.h"
#include "DesktopWindow.h"

namespace Walnut
{
#if __GTK__
	/// Iterating GTK events
	void iterateGTKEvents();
#endif
}


#endif
