//
//  GLFWWindow.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#include "desktop.h"
#include "../../graphic/graphic.h"

namespace Walnut
{
    
    /*! @brief Regist event callbacks
        @param The window to regist event callbacks
    */
    void registCallbacks(GLFWwindow* window);


    DesktopWindow::DesktopWindow(const string& title, int width , int height):super()
    {
        m_title = title;
        m_size = Size(static_cast<float>(width), static_cast<float>(height) );
    }

    DesktopWindow::~DesktopWindow()
    {
        if(m_pHostWnd)
        {
            glfwDestroyWindow(static_cast<GLFWwindow*>(m_pHostWnd));
            m_pHostWnd = nullptr;
        }
    }
    
    
    // initialize window
    bool DesktopWindow::initialize()
    {
        auto window = glfwCreateWindow((int)m_size.width, (int)m_size.height, m_title.c_str(), nullptr, nullptr);
        if(window)
        {
            glfwSetWindowPos(window, (int)m_position.x, (int)m_position.y);
            registCallbacks(window);
            glfwMakeContextCurrent(window);
            // Load OpenGL with glad
            if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
            {
                glfwDestroyWindow(window);
                window = nullptr;
            }
        }
        m_pHostWnd = window;
        return m_pHostWnd != nullptr;
    }
    
    bool DesktopWindow::makeContextCurrent()
    {
        GLFWwindow * originWindow = static_cast<GLFWwindow*>(m_pHostWnd);
        if(originWindow)
        {
            glfwMakeContextCurrent(originWindow);
            if(m_renderRatio!=60)
                glfwSwapInterval(0);
        }
        return originWindow != nullptr;
    }
    
    // set window title text
    void DesktopWindow::setTitle(const string& title)
    {
        m_title = title;
        if(m_pHostWnd)
            glfwSetWindowTitle(static_cast<GLFWwindow*>(m_pHostWnd), title.c_str());
    }
    
    // set window position on the screen
    void DesktopWindow::setPosition(const Point& pos)
    {
        m_position = pos;
        if(m_pHostWnd)
            glfwSetWindowPos(static_cast<GLFWwindow*>(m_pHostWnd), (int)pos.x, (int)pos.y);
    }
    
    //set window size
    void DesktopWindow::setSize(const Size& size)
    {
        m_size = size;
        if(m_pHostWnd)
            glfwSetWindowSize(static_cast<GLFWwindow*>(m_pHostWnd), (int)size.width, (int)size.height);
    }
    
    //Check if the window is visible
    bool DesktopWindow::visible()
    {
        m_visible = glfwGetWindowAttrib( static_cast<GLFWwindow*>(m_pHostWnd) ,GLFW_VISIBLE) == GL_TRUE;
        return m_visible;
    }
    
    // Set the visibility of the window
    void DesktopWindow::setVisible(bool visible)
    {
        if(visible)
            glfwShowWindow(static_cast<GLFWwindow*>(m_pHostWnd));
        else
            glfwHideWindow(static_cast<GLFWwindow*>(m_pHostWnd));
        m_visible = visible;
    }
    
    void DesktopWindow::swapBuffers()
    {
       glfwSwapBuffers( static_cast<GLFWwindow*>(m_pHostWnd) );
    }
}


