//
//  GLFWWindow.h
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#ifndef __Walnut__GLFWWindow__
#define __Walnut__GLFWWindow__

#include "graphic/OpenGL/OpenGL.h"

namespace Walnut
{
    /// Desktop window of Walnut
    class DesktopWindow : public RenderWindowBase
    {
        using super = RenderWindowBase;
        friend class CoreEngine;
    protected:
        /// Visibility of the window
        bool m_visible;  
        /// Window position
        Point m_position;
        /// Title text of the window
        string m_title;  
    public:
        
        /**
         *  @brief Construct a window
         *  @param title Window title text
         *  @param width Window width
         *  @param height Window height
         */
        DesktopWindow(const string& title, int width , int height);
        DesktopWindow(const DesktopWindow&) = delete;
        virtual ~ DesktopWindow();
        
        /// Set window size
        void setSize(const Size& size);
        /// Set window size
        void setSize(int width , int height) { setSize(Size(width,height));}

        /// Set window position
        void setPosition(const Point& pos);
        /// Set window position        
        void setPosition(int x , int y){ setPosition(Point(x,y)); }
        
        /*!
         @brief  Set window title text
         @param title Window title text
         */
         void setTitle(const string& title);
        
         /// Get window title text
         string title()const{ return m_title;}
         
        /*!
         @brief  Check if the window is visible
         @return Is visible
         */
         bool visible();
        
        /*!
         @brief  Set the visibility of the window
         @param visible Is visible
         */
        virtual void setVisible(bool visible);
        
    protected:
        /*!
         @brief Initialize window
         @return Is initialization succeed
         */
        virtual bool initialize() override;
        
        /*!
         @brief  Checkout context for rendering
         @return Is rendering context valid
         */
        virtual bool makeContextCurrent() override;
        
        /// Swapbuffers
        virtual void swapBuffers() override;
        
    };
    
    /// Define DesktopWindow as the Window on Desktop platforms
    using Window = DesktopWindow;
}
#endif
