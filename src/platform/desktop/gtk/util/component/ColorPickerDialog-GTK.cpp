#include "util/util.h"
#include "platform/platform.h"
#include <gtk/gtk.h>
namespace Walnut
{
    namespace Util
    {
        // Show gtk color chooser dialog
        void ColorPickerDialog::show()
        {
            const int COLOR_SELECTED = -5;
            auto dialog = gtk_color_chooser_dialog_new(m_title.c_str(),nullptr);
            GdkRGBA  color{ m_color.redPercent(),
                            m_color.greenPercent(),
                            m_color.bluePercent(),
                            m_color.alphaPercent()
                          };
            gtk_color_chooser_set_rgba((GtkColorChooser *)dialog,&color);
            if( gtk_dialog_run(GTK_DIALOG (dialog) ) == COLOR_SELECTED )
            {
                gtk_color_chooser_get_rgba((GtkColorChooser *)dialog,&color);
                
                // Get selected color value
                m_color = Color( color.red , color.green , color.blue , color.alpha);
                
                // Dispatching confirmed event
                Event event(NormalEventType::CONFIRMED,false,false);
                dispatchEvent(event);
            }
            else
            {   // Dispatching canceled event
                Event event(NormalEventType::CANCELED,false,false);
                dispatchEvent(event);
            }
            gtk_widget_destroy(dialog);
			iterateGTKEvents();
        }
        
        ColorPickerDialog::~ColorPickerDialog(){}
    }
}