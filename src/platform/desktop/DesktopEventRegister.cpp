//
//  DesktopEventRegister.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/10.
//
//
#include "../../core/core.h"
#include "../../platform/platform.h"
#include <iostream>
#include "core/events/KeySource.h"

namespace Walnut
{
    /// Get KeyCode from GLFW key
    int getKeyCode(int glfwKey);
    
    void registCallbacks(GLFWwindow* window)
    {
        //Close window
        glfwSetWindowCloseCallback(window, [](GLFWwindow* w)
        {
           RenderWindowBase * window  = getWindowFromEngine(w);
           if(window)
           {
               Event e(NormalEventType::CLOSED,true,false);
               e.setTarget(window);
               window->dispatchEvent(e);
           }
        });
        
        //Window focus
        glfwSetWindowFocusCallback(window, [](GLFWwindow* w,int action )
        {
           RenderWindowBase * window  = getWindowFromEngine(w);
           if(window)
           {
               bool actived =false;
               int eventType = NormalEventType::LOST_FOCUS;
               if(action == GL_TRUE)
               {
                   actived   = true;
                   eventType = NormalEventType::OBTAIN_FOCUS;
               }
               window->setActived(actived);
               Event e(eventType,true,false);
               window->dispatchEvent(e);
           }
        });
        
        //Keyboard
        glfwSetKeyCallback(window,[](GLFWwindow* w, int key, int scancode, int action, int mods)
        {
            RenderWindowBase * window  = getWindowFromEngine(w);
            if(window)
            {
               int eventType = 0;
               if(action == GLFW_PRESS)
                   eventType = KeyboardEvent::KeyDown;
               else if (action == GLFW_RELEASE)
                   eventType = KeyboardEvent::KeyUp;
               else if (action == GLFW_RELEASE)
                   eventType = KeyboardEvent::KeyRepeat;
               if(eventType)
               {
                   vector<int> addKeys;
                   for (int k= GLFW_KEY_SPACE; k<=GLFW_KEY_LAST; ++k)
                   {
                       if (glfwGetKey(w, k) == GLFW_PRESS && k!=key)
                           addKeys.push_back( getKeyCode(k) );
                   }
                   
                   KeyboardEvent ke(eventType, getKeyCode(key),false,addKeys);
                   window->dispatchEvent(ke);
               }
            }
        });
        
        //Mouse buttons
        glfwSetMouseButtonCallback(window,[](GLFWwindow* w,int btn,int action,int mod)
        {
           RenderWindowBase * window  = getWindowFromEngine(w);
           if(window)
           {
               double x =0,y=0;
               glfwGetCursorPos(w, &x, &y);
               int eventType = 0;
               switch (btn)
               {
                   case GLFW_MOUSE_BUTTON_LEFT:
                       eventType = (action == GLFW_PRESS) ? MouseEvent::MOUSE_LDOWN : MouseEvent::MOUSE_LUP;
                       break;
                   case GLFW_MOUSE_BUTTON_RIGHT:
                       eventType = (action == GLFW_PRESS) ? MouseEvent::MOUSE_RDOWN : MouseEvent::MOUSE_RUP;
                       break;
                   case GLFW_MOUSE_BUTTON_MIDDLE:
                       eventType = (action == GLFW_PRESS) ? MouseEvent::MOUSE_MDOWN : MouseEvent::MOUSE_MUP;
                       break;
                   default:
                       break;
               }
               if(eventType)
               {
                   vector<int> addKeys;
                   for (int k= GLFW_KEY_SPACE; k<=GLFW_KEY_LAST; ++k)
                   {
                       if (glfwGetKey(w, k) == GLFW_PRESS)
                           addKeys.push_back( getKeyCode(k) );
                   }
                   MouseEvent me(eventType,false,Point(x,y),zeroPoint,addKeys);
                   window->dispatchEvent(me);
               }
               
           }
        });
        
        //Mouse cursor
        glfwSetCursorPosCallback(window, [](GLFWwindow* w,double x,double y)
        {
             RenderWindowBase * window  = getWindowFromEngine(w);
             if(window)
             {
                 vector<int> addKeys;
                 for (int k= GLFW_KEY_SPACE; k<=GLFW_KEY_LAST; ++k)
                 {
                     if (glfwGetKey(w, k) == GLFW_PRESS)
                         addKeys.push_back( getKeyCode(k) );
                 }
                 MouseEvent me(MouseEvent::MOUSE_MOVE,false,Point(x,y),zeroPoint,addKeys);
                 window->dispatchEvent(me);
             }
        });
        
        //Mouse enter/out window
        glfwSetCursorEnterCallback(window, [](GLFWwindow*w,int entered)
        {
           RenderWindowBase * window  = getWindowFromEngine(w);
           if(window)
           {
               double x =0 ,y=0;
               glfwGetCursorPos(w, &x, &y);
               int eventType = MouseEvent::MOUSE_OUT;
               if(entered)
                   eventType = MouseEvent::MOUSE_ENTER;
               vector<int> addKeys;
               for (int k= GLFW_KEY_SPACE; k<=GLFW_KEY_LAST; ++k)
               {
                   if (glfwGetKey(w, k) == GLFW_PRESS)
                       addKeys.push_back( getKeyCode(k) );
               }
               
               MouseEvent me(eventType,false,Point(x,y),zeroPoint,addKeys);
               window->dispatchEvent(me);
           }
        });
        
        //Mouse scroll
        glfwSetScrollCallback(window, [](GLFWwindow* w,double sx,double sy)
        {
          RenderWindowBase * window  = getWindowFromEngine(w);
          if(window)
          {
              double x =0 ,y=0;
              glfwGetCursorPos(w, &x, &y);
              
              vector<int> addKeys;
              for (int k= GLFW_KEY_SPACE; k<=GLFW_KEY_LAST; ++k)
              {
                  if (glfwGetKey(w, k) == GLFW_PRESS)
                      addKeys.push_back( getKeyCode(k) );
              }
              
              MouseEvent me(MouseEvent::MOUSE_SCROLL,false,Point(x,y),Point(sx,sy),addKeys);
              window->dispatchEvent(me);
          }
        });
        
        // Drop file into window
        glfwSetDropCallback(window,[](GLFWwindow* w, int count, const char** paths)
        {
            RenderWindowBase * window  = getWindowFromEngine(w);
            if(window)
            {
                //TODO:Drop files
                std::cout<<"drop in :\n";
                for(size_t i=0;i!=count;++i)
                    std::cout<<paths[i]<<std::endl;
                
            }
        });
    }

    // Get KeyCode from glfw key
    int getKeyCode(int glfwKey)
    {
        int code = KeyCode::Unknown;
        if( glfwKey >= GLFW_KEY_0 && glfwKey <= GLFW_KEY_9 )
            code = KeyCode::Number0 + (glfwKey - GLFW_KEY_0);
        else if( glfwKey >= GLFW_KEY_KP_0 && glfwKey <= GLFW_KEY_KP_EQUAL)
            code = KeyCode::Numpad0 + (glfwKey - GLFW_KEY_KP_0);
        else if( glfwKey >= GLFW_KEY_A && glfwKey <= GLFW_KEY_Z)
            code = KeyCode::A + (glfwKey - GLFW_KEY_A);
        else if( glfwKey >= GLFW_KEY_F1 && glfwKey <= GLFW_KEY_F25)
            code = KeyCode::F1 + (glfwKey - GLFW_KEY_F1);
        else if( glfwKey >= GLFW_KEY_LEFT_SHIFT && glfwKey <= GLFW_KEY_MENU )
            code = KeyCode::LeftShift + (glfwKey - GLFW_KEY_LEFT_SHIFT);
        else if( glfwKey >= GLFW_KEY_ESCAPE && glfwKey <= GLFW_KEY_PAUSE )
            code = KeyCode::Escape + (glfwKey - GLFW_KEY_ESCAPE);
        else
        {
            switch(glfwKey)
            {
                case GLFW_KEY_SPACE:
                    code = KeyCode::Space;
                    break;
                case GLFW_KEY_APOSTROPHE:
                    code = KeyCode::Apostrophe;
                    break;
                case GLFW_KEY_COMMA:
                    code = KeyCode::Comma;
                    break;
                case GLFW_KEY_MINUS:
                    code = KeyCode::Hyphen;
                    break;
                case GLFW_KEY_PERIOD:
                    code = KeyCode::Point;
                    break;
                case GLFW_KEY_SLASH:
                    code = KeyCode::Slash;
                    break;
                case GLFW_KEY_SEMICOLON:
                    code = KeyCode::Semicolon;
                    break;
                case GLFW_KEY_EQUAL:
                    code = KeyCode::Equals;
                    break;
                case GLFW_KEY_LEFT_BRACKET:
                    code = KeyCode::LeftBracket;
                    break;
                case GLFW_KEY_BACKSLASH:
                    code = KeyCode::BackSlash;
                    break;
                case GLFW_KEY_RIGHT_BRACKET:
                    code = KeyCode::RightBracket;
                    break;
                case GLFW_KEY_GRAVE_ACCENT:
                    code = KeyCode::Accent;
                    break;
                default:
                    code = KeyCode::Unknown;
            }
        }
        return code;
    }

}
