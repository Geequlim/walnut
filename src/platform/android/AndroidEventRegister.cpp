#include "platform/platform.h"
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
using namespace Walnut;

#include <android/keycodes.h>

void android_handle_event(struct android_app *app, int32_t cmd)
{
    AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(app->userData);
    switch (cmd)
    {
        case APP_CMD_INIT_WINDOW:
            if(window)
            {
                if( window->initialize() )
                {
                  window->makeContextCurrent();
                    // engine->start();
                    LOGI("OpenGL ES version:%s",glGetString(GL_VERSION));
                }
                else
                {
                    window->uninitialize();
                }
            }
            break;
        case APP_CMD_TERM_WINDOW:
            if(window)
            {
                window->terminate();
            }
            LOGI("Window Terminated.");
            break;
        case APP_CMD_DESTROY:
            LOGI("Destory application");
            engine->stop();
            app_dummy();
            break;
        case APP_CMD_WINDOW_RESIZED:
            if(window)
            {
                window->resize();
            }
            LOGI("Window resized");
            break;
        case APP_CMD_GAINED_FOCUS:
            if(window)
            {
                window->gainFocus();
            }
            LOGI("Gain focus");
            break;
        case APP_CMD_LOST_FOCUS:
            if(window)
            {
                window->lostFocus();
            }
            LOGI("Lost focus");
            break;
        case APP_CMD_LOW_MEMORY:
            LOGI("!!!Memery low!!!");
            break;
        case APP_CMD_PAUSE:
            if(window)
            {
                window->pause();
            }
            LOGI("Application paused");
            break;
        case APP_CMD_RESUME:
            LOGI("Staring resume application...");
            if (window)
            {
                window->resume();
            }
            LOGI("Application resumed");
            break;
        case APP_CMD_START:
            LOGI("Application started");
            break;
        case APP_CMD_STOP:
            LOGI("Application stoped");
            break;
        case APP_CMD_SAVE_STATE:
            break;
        default:
            break;
    }
}


/// Get KeyCode from Android key code
int getKeyCode(int androidKey );

int32_t android_handle_input(struct android_app *app, AInputEvent *event)
{
    AndroidNativeWindow* w = static_cast<AndroidNativeWindow*>(app->userData);
    if(w)
    {
        auto window = getWindowFromEngine(w);
        if(window)
        {
            InputSource& inputSource = window->inputSource();
              switch (AInputEvent_getType(event))
              {
                  case AINPUT_EVENT_TYPE_KEY:
                    {
                              int32_t code = AKeyEvent_getKeyCode((const AInputEvent*)event);
                              int32_t action = AKeyEvent_getAction((const AInputEvent*)event);
                              int32_t metaState = AKeyEvent_getMetaState((const AInputEvent*) event);
                              
                              LOGI("Meta state %d",metaState);
                              LOGI("native key code is %d",code);

                              
                              int  keyCode = getKeyCode(code);
                              if (action == AKEY_EVENT_ACTION_DOWN)
                                  inputSource.keyDown(keyCode);
                              else if(action == AKEY_EVENT_ACTION_UP)
                                  inputSource.keyUp(keyCode);
                    }
                      break;
                case AINPUT_EVENT_TYPE_MOTION:
                            switch(AInputEvent_getSource(event))
                            {
                                // Touch screen events
                                case AINPUT_SOURCE_TOUCHSCREEN:
                                {
                                    int action = AKeyEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK;
//                                    AMotionEvent_getAction();
//                                    AMotionEvent_getEventTime();
                                    size_t pointCount = AMotionEvent_getPointerCount(event);
                                    
                                    float x = AMotionEvent_getX(event,0);
                                    float y = AMotionEvent_getY(event,0);
                                    switch(action)
                                    {
                                        case AMOTION_EVENT_ACTION_POINTER_DOWN:
                                        case AMOTION_EVENT_ACTION_DOWN:
                                            LOGI("Touch down");
                                        break;
                                        case AMOTION_EVENT_ACTION_POINTER_UP:
                                        case AMOTION_EVENT_ACTION_UP:
                                            LOGI("Touch up");
                                        break;
                                        case AMOTION_EVENT_ACTION_MOVE:
                                            LOGI("Touch move");
                                        break;
                                        case AMOTION_EVENT_ACTION_CANCEL:
                                            LOGI("Touch cancel");
                                        break;
                                        case AMOTION_EVENT_ACTION_OUTSIDE:
                                            LOGI("Touch out");
                                        break;
                                    }
                                }
                                break;
                                // Mouse events                            
                                case AINPUT_SOURCE_MOUSE:
                                    {
                                    int action = AKeyEvent_getAction(event) & AMOTION_EVENT_ACTION_MASK;
                                    switch(action)
                                    {
                                        case AMOTION_EVENT_ACTION_DOWN:
                                            LOGI("Mouse down");
                                        break;
                                        case AMOTION_EVENT_ACTION_UP:
                                            LOGI("Mouse up");
                                        break;
                                        case AMOTION_EVENT_ACTION_MOVE:
                                            LOGI("Mouse move");
                                        break;
                                        case AMOTION_EVENT_ACTION_CANCEL:
                                            LOGI("Mouse cancel");
                                        break;
                                        case AMOTION_EVENT_ACTION_OUTSIDE:
                                            LOGI("Mouse out");
                                        break;
                                    }
                                }
                                break;
                                case AINPUT_SOURCE_KEYBOARD:
                                    LOGI("Keyboard event");
                                break;
                            } // end switch
                        break;
                  default:
                      return 0;
              }
          }
    }
    return engine->running();
}


int getKeyCode(int androidKey )
{
     int code = KeyCode::Unknown;
     if( androidKey>= AKEYCODE_0 && androidKey <= AKEYCODE_9 )
            code = KeyCode::Number0 + (androidKey - AKEYCODE_0);
    else if( androidKey >= AKEYCODE_A && androidKey <= AKEYCODE_Z )
            code = KeyCode::A + (androidKey - AKEYCODE_A);
    else if( androidKey >= AKEYCODE_STAR && androidKey <= AKEYCODE_CLEAR  )
            code = KeyCode::Star + ( androidKey - AKEYCODE_STAR );
    else if( androidKey >= AKEYCODE_SEARCH && androidKey <= AKEYCODE_MEDIA_FAST_FORWARD )
            code = KeyCode::Search + ( androidKey -  AKEYCODE_SEARCH );
    else
    {
        switch (androidKey) {
            case  AKEYCODE_BACK:
                code = KeyCode::Back;
                break;
            case AKEYCODE_MENU:
                code = KeyCode::Menu;
                break;
            case AKEYCODE_HOME:
                code = KeyCode::Home;
                break;
            case AKEYCODE_CALL:
                code = KeyCode::Call;
                break;
            case AKEYCODE_ENDCALL:
                code = KeyCode::EndCall;
                break;
            case AKEYCODE_COMMA:
                code = KeyCode::Comma;
                break;
            case AKEYCODE_ALT_LEFT:
                code = KeyCode::LeftAlt;
                break;
            case AKEYCODE_ALT_RIGHT:
                code = KeyCode::RightAlt;
                break;
            case AKEYCODE_SHIFT_LEFT:
                code = KeyCode::LeftShift;
                break;
            case AKEYCODE_SHIFT_RIGHT:
                code = KeyCode::RightShift;
                break;
            case AKEYCODE_TAB:
                code = KeyCode::Tab;
                break;
            case AKEYCODE_SPACE:
                code = KeyCode::Space;
                break;
            case AKEYCODE_ENTER:
                code = KeyCode::Return;
                break;
            case AKEYCODE_DEL:
                code = KeyCode::Delete;
                break;
            case AKEYCODE_MINUS:
                code = KeyCode::Hyphen;
                break;
            case AKEYCODE_EQUALS:
                code = KeyCode::Equals;
                break;
            case AKEYCODE_LEFT_BRACKET:
                code = KeyCode::LeftBracket;
                break;
            case AKEYCODE_RIGHT_BRACKET:
                code = KeyCode::RightBracket;
                break;
            case AKEYCODE_BACKSLASH:
                code = KeyCode::BackSlash;
                break;
            case AKEYCODE_SEMICOLON:
                code = KeyCode::Semicolon;
                break;
            case AKEYCODE_APOSTROPHE:
                code = KeyCode::Apostrophe;
                break;
            case AKEYCODE_SLASH:
                code = KeyCode::Slash;
                break;
            case AKEYCODE_MUTE:
                code = KeyCode::Mute;
                break;
            case AKEYCODE_PAGE_UP:
                code = KeyCode::PageUp;
                break;
            case AKEYCODE_PAGE_DOWN:
                code = KeyCode::PageDown;
                break;
            case AKEYCODE_PERIOD:
                code = KeyCode::Point;
                break;
            // Other keys unimplemented
            case AKEYCODE_PICTSYMBOLS:
            case AKEYCODE_SWITCH_CHARSET:
            case AKEYCODE_AT:
            case AKEYCODE_SYM:
            case AKEYCODE_EXPLORER:
            case AKEYCODE_ENVELOPE:
            case AKEYCODE_GRAVE:
            case AKEYCODE_NUM:
            case AKEYCODE_HEADSETHOOK:
            case AKEYCODE_FOCUS:
            case AKEYCODE_PLUS:
            case AKEYCODE_NOTIFICATION:
                code = KeyCode::Unknown;
                break;
        }
    }
    return code;
}
