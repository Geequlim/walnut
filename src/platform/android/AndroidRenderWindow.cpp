#include "platform/platform.h"
#include "graphic/graphic.h"
namespace Walnut
{

    AndroidRenderWindow::AndroidRenderWindow():super()
    {
    }

    AndroidRenderWindow::~AndroidRenderWindow()
    {
        if( AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(m_pHostWnd) )
        {
            m_pHostWnd = nullptr;
        }
    }

    bool AndroidRenderWindow::makeContextCurrent()
    {
        if( AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(m_pHostWnd) )
           return window->makeContextCurrent();
        else
            return false;
    }

    bool AndroidRenderWindow::initialize()
    {

        struct android_app* state = static_cast<struct android_app*>(engine->nativeState());
        auto window = new AndroidNativeWindow(state);
        m_pHostWnd =  window;
        state->userData = m_pHostWnd;
        return true;
    }
    void AndroidRenderWindow::pollEvents()
    {
        if( AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(m_pHostWnd) )
        {
            window->pollEvent();
        }
    }

    bool AndroidRenderWindow::actived()const
    {
        if( AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(m_pHostWnd) )
        {
            return window->active();
        }
        else
            return false;
    }

    void AndroidRenderWindow::swapBuffers()
    {
        if( AndroidNativeWindow* window = static_cast<AndroidNativeWindow*>(m_pHostWnd) )
        {
            window->swapBuffer();
        }
    }
}
