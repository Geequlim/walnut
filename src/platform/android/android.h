#ifndef __Walnut_Platform_Android_H__
#define __Walnut_Platform_Android_H__

#include <jni.h>

#include <memory>
#include <unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <android/log.h>

#include <EGL/egl.h>
#include <android/input.h>
#include <android/sensor.h>
#include <android_native_app_glue.h>

#include "AndroidNativeWindow.h"
#include "AndroidRenderWindow.h"


namespace Walnut
{
    
    void stopActivity();
    
/// Logcat information
#define LOGI(...) ((void)__android_log_print(ANDROID_LOG_INFO, "Walnut", __VA_ARGS__))
/// Logcat warnning
#define LOGW(...) ((void)__android_log_print(ANDROID_LOG_WARN, "Walnut", __VA_ARGS__))
/// Logcat debug
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_DEBUG, "Walnut", __VA_ARGS__))
/// Logcat verbose
#define LOGV(...) ((void)__android_log_print(ANDROID_LOG_VERBOSE, "Walnut", __VA_ARGS__))
/// Logcat error
#define LOGE(...) ((void)__android_log_print(ANDROID_LOG_ERROR, "Walnut", __VA_ARGS__))
/// Logcat fatal
#define LOGF(...) ((void)__android_log_print(ANDROID_LOG_FATAL, "Walnut", __VA_ARGS__))
}

#endif