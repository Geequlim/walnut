#include "platform/platform.h"

/// Android event handler
void android_handle_event(struct android_app *app, int32_t cmd);

/// Android input handler
int32_t android_handle_input(struct android_app *app, AInputEvent *event);

namespace Walnut
{
    bool platformInitialize(void* nativeSrc)
    {
        app_dummy();
        struct android_app* state = static_cast<struct android_app*>(nativeSrc);
        if( state )
        {
            state->onAppCmd = android_handle_event;
            state->onInputEvent = android_handle_input;
        }
        else
            return false;
        return true;
    }

    /// Poll events from platform
    void pollPlatformEvents()
    {
        if(engine && engine->windows().size())
        {
            AndroidRenderWindow * awindow = dynamic_cast<AndroidRenderWindow*>(*(engine->windows().begin()));
            if(awindow)
                awindow->pollEvents();
        }
    }
    
    void platformUnnitialize(void* nativeSrc)
    {

    }

    void platformSetClipboardString(const string& str)
    {


    }
    string platformGetClipboardString()
    {
        return nullstr;
    }
    
     void stopActivity()
     {
          ANativeActivity_finish( ((struct android_app*)Walnut::engine->nativeState() )->activity);
     }
}