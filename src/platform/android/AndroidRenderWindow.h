//
// Created by geequlim on 9/23/15.
//

#ifndef WALNUT_ANDROIDRENDERWINDOW_H
#define WALNUT_ANDROIDRENDERWINDOW_H

namespace Walnut
{
    class AndroidRenderWindow : public RenderWindowBase {
        using super = RenderWindowBase;
    public:
        AndroidRenderWindow();
        virtual ~AndroidRenderWindow();

        virtual bool initialize() override ;

        virtual bool actived()const override;
        virtual bool makeContextCurrent() override;

        virtual void swapBuffers() override;
        virtual void pollEvents() ;
    };

    using Window = AndroidRenderWindow;
}

#endif //WALNUT_ANDROIDRENDERWINDOW_H
