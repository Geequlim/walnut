//
// Created by geequlim on 9/22/15.
//

#include "platform/platform.h"
#include "graphic/graphic.h"

using namespace Walnut;

namespace Walnut
{
    AndroidNativeWindow::AndroidNativeWindow(android_app *appPtr)
            :m_pAndroidApp(appPtr),
             m_finished(false),
             m_active(false),
             m_initialized(false),
             m_display(EGL_NO_DISPLAY),
             m_surface(EGL_NO_SURFACE),
             m_context(EGL_NO_CONTEXT)
    {
        // Prepare to monitor accelerometer
        m_pSensorManager = ASensorManager_getInstance();

        m_pAccelerometerSensor = ASensorManager_getDefaultSensor(
                m_pSensorManager,
                ASENSOR_TYPE_ACCELEROMETER
        );

        if (m_pAccelerometerSensor)
        {
            m_pSensorEventQueue = ASensorManager_createEventQueue(
                    m_pSensorManager,
                    m_pAndroidApp->looper,
                    LOOPER_ID_USER,
                    nullptr,
                    nullptr
            );
        }
    }

    bool AndroidNativeWindow::initialize()
    {
        const EGLint attrib_list[] = {
                EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
                EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
                EGL_RED_SIZE, 8,
                EGL_GREEN_SIZE, 8,
                EGL_BLUE_SIZE, 8,
                EGL_ALPHA_SIZE, 8,
                EGL_DEPTH_SIZE, 8,
                EGL_NONE
        };

        // initialize EGL display
        EGLDisplay &display = m_display;
        if (display == EGL_NO_DISPLAY)
        {
            display = eglGetDisplay(EGL_DEFAULT_DISPLAY);
            eglInitialize(display, 0, 0);
        }

        EGLConfig config;
        EGLint nconfigs;
        eglChooseConfig(display, attrib_list, &config, 1, &nconfigs);

        // initialize EGL surface
        EGLSurface &surface = m_surface;
        if (surface == EGL_NO_SURFACE)
        {
            EGLint format;
            eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
            ANativeWindow_setBuffersGeometry(m_pAndroidApp->window, 0, 0, format);

            surface = eglCreateWindowSurface(display, config, m_pAndroidApp->window, nullptr);
            if (surface == EGL_NO_SURFACE)
            {
                LOGF("Create EGL window surface failed.");
                m_initialized = true;
                return false;
            }
        }

        // initialize EGL context
        EGLContext & context = m_context;
        if(EGL_NO_CONTEXT == context)
        {
            EGLint contextAttribs[] = { EGL_CONTEXT_CLIENT_VERSION, 2, EGL_NONE };
            context = eglCreateContext(display, config, EGL_NO_CONTEXT, contextAttribs);
            if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE)
            {
                LOGF("Create EGL context failed.");
                m_initialized = true;
                return false;
            }
        }

        // get window size
        eglQuerySurface(display, surface, EGL_WIDTH, &m_width);
        eglQuerySurface(display, surface, EGL_HEIGHT, &m_height);
        m_initialized = true;
        LOGI("EGLSurface initialized with size: %dx%d",m_width,m_height);
    }

    void AndroidNativeWindow::uninitialize()
    {
        m_active = false;
        m_finished = true;
        if (m_display != EGL_NO_DISPLAY)
        {
            eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
            if (m_context != EGL_NO_CONTEXT)
                eglDestroyContext(m_display, m_context);
            if (m_display != EGL_NO_SURFACE)
                eglDestroySurface(m_display,m_surface);
            eglTerminate(m_display);
        }
        m_surface = EGL_NO_SURFACE;
        m_display = EGL_NO_DISPLAY;
        m_context = EGL_NO_CONTEXT;
    }

    void AndroidNativeWindow::terminate()
    {
        m_finished = true;
        eglMakeCurrent(m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
        eglDestroySurface(m_display, m_surface);
        m_surface = EGL_NO_SURFACE;
    }

    void AndroidNativeWindow::pause()
    {
        m_finished = true;m_finished = true;
        if(m_active)
            lostFocus();
        eglMakeCurrent( m_display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
    }

    void AndroidNativeWindow::resume()
    {
        makeContextCurrent();
        m_finished = false;
        if(!m_active)
            gainFocus();
    }

    void AndroidNativeWindow::resize()
    {
        if( makeContextCurrent() )
        {
            eglQuerySurface(m_display, m_surface, EGL_WIDTH, &m_width);
            eglQuerySurface(m_display, m_surface, EGL_HEIGHT, &m_height);
        }
    }

    void AndroidNativeWindow::gainFocus()
    {
        if (m_pAccelerometerSensor)
            ASensorEventQueue_enableSensor(m_pSensorEventQueue,m_pAccelerometerSensor );
        m_active = true;

    }

    void AndroidNativeWindow::lostFocus()
    {
        m_active = false;
        if (m_pAccelerometerSensor)
            ASensorEventQueue_disableSensor(m_pSensorEventQueue,m_pAccelerometerSensor );
    }


    bool AndroidNativeWindow::makeContextCurrent()
    {
        LOGI("Make EGL context current");
        return !m_finished && eglMakeCurrent(m_display, m_surface, m_surface, m_context) == EGL_TRUE;
    }

    bool AndroidNativeWindow::pollEvent()
    {
        // Read all pending events.
        int ident;
        int events;
        struct android_poll_source* source;


        // If not actived, we will block forever waiting for events.
        while ((ident=ALooper_pollAll( m_active ? 0 : -1,
                                      nullptr,
                                      &events,
                                      (void**)&source) ) >= 0 )
        {

            // Process this event.
            if (source != nullptr) source->process(m_pAndroidApp, source);

            // If a sensor has data, process it now.
            if (ident == LOOPER_ID_USER)
            {
                if ( m_pAccelerometerSensor )
                {
                    ASensorEvent event;
                    while (ASensorEventQueue_getEvents(m_pSensorEventQueue, &event, 1) > 0)
                    {
                        // TODO: Dispatching acceleration event
                    }
                }
            }

            // Check if we are exiting.
            if (m_pAndroidApp->destroyRequested)
            {
                // stop mail loop now
                engine->m_boRunning = false;
                return false;
            }
        }
        return true;
    }

    void AndroidNativeWindow::swapBuffer()
    {
        eglSwapBuffers( m_display, m_surface);
    }
}
