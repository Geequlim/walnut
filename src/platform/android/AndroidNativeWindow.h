//
// Created by geequlim on 9/22/15.
//

#ifndef WALNUT_ANDROID_APP_H
#define WALNUT_ANDROID_APP_H

namespace Walnut
{
    /**
     * @brief Android native OpenGL ES window
     */
    class AndroidNativeWindow
    {
    private:
        // Native applictaion pointer
        android_app* m_pAndroidApp;

        // Sensor
        ASensorManager* m_pSensorManager;
        const ASensor*  m_pAccelerometerSensor;
        ASensorEventQueue* m_pSensorEventQueue;

        // EGL display
        EGLDisplay m_display;
        // EGL surface
        EGLSurface m_surface;
        // EGL context
        EGLContext m_context;
        // Window width
        int32_t m_width;
        // widow height
        int32_t m_height;
        // is window terminated
        bool m_finished;
        // is window actived
        bool m_active;
        
        bool m_initialized;
        
    public:
        /// Construct an native OpenGL ES window with android application pointer
        AndroidNativeWindow(android_app* appPtr);

        /// Get EGL Surface
        inline  EGLSurface& surface() { return m_surface;}
        /// Get EGL Display
        inline  EGLDisplay& display() { return m_display;}
        /// Get EGL context
        inline  EGLContext& context() { return m_context;}

        inline bool initialized()const{ return m_initialized;}
        /// Check is window terminated
        inline bool finished()const { return m_finished;}

        /// Check is window actived
        inline bool active()const{ return m_active;}

        /// Initialize native window
        bool initialize();
        /// Destory native window
        void uninitialize();
        /// Pause window
        void pause();
        /// Resume window
        void resume();
        /// Resize window
        void resize();
        /// Terminate window
        void terminate();
        /// Focus the window
        void gainFocus();
        /// Window lost focus
        void lostFocus();

        /// Make current EGL context to OpenGL Context
        bool makeContextCurrent();

        /// Poll some events of the natvie window
        bool pollEvent();

        void swapBuffer();
    };
}
#endif //WALNUT_ANDROID_APP_H
