//
//  platform.h
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#ifndef Walnut_platform_h
#define Walnut_platform_h

#include "Macros.h"
#include "common/common.h"

namespace Walnut
{
    /*!
     @brief  Initialize platform configrations,implemented differently in different platform
     @return Is initialization succeed
     */
    bool platformInitialize(void* nativeSrc);
    
    /// Uninitialize platform configrations,implemented differently in different platform
    void platformUnnitialize(void* nativeSrc);
    
    /// Poll events from platform
    void pollPlatformEvents();
    
    /*!
     @brief  Set system clipboard text string,implemented differently in different platform
     @param str The string content to set onto clipboard
     */
    void platformSetClipboardString(const string& str);
    
    /*!
     @brief  Get system clipboard text string,implemented differently in different platform
     @return The system clipboard text content
     @note If get system clipboard text failed,return Walnut::nullstr
     */
    string platformGetClipboardString();
}

#if __APPLE__
    #include "apple/apple.h"
#endif

#if __DESKTOP__
    #include "desktop/desktop.h"
#endif

#if __WINDOWS__
	#include <Windows.h>
#endif

#if __LINUX__
	#include "desktop/linux/linux.h"
#endif

#ifdef __ANDROID__
    #include "android/android.h"
#endif

#endif
