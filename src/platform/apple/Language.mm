//
//  Language.m
//  Walnut
//
//  Created by Geequlim on 15/8/17.
//
//

#include "apple.h"
#import <Foundation/Foundation.h>

namespace Walnut
{
    // Get current language of the system
    Language systemLanguage()
    {
        Language result = Language::Unknown;
        string appleResult = nullstr;
        
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
        NSString *currentLanguage = [languages objectAtIndex:0];
        if ([currentLanguage length])
        {
            appleResult = [currentLanguage UTF8String];
        }
        
        if( appleResult == "en")
            result = Language::en_US;
        else if (appleResult == "fr")
            result = Language::fr_FR;
        else if (appleResult == "de")
            result = Language::de_DE;
        else if (appleResult == "zh-Hans")
            result = Language::zh_CN;
        else if (appleResult == "zh-Hant")
            result = Language::zh_TW;
        else if (appleResult == "ja")
            result = Language::ja_JP;
        else if (appleResult == "es")
            result = Language::es_ES;
        else if (appleResult == "it")
            result = Language::it_IT;
        else if (appleResult == "ko")
            result = Language::ko_KR;
        else if (appleResult == "pt")
            result = Language::pt_PT;
        else if (appleResult == "da")
            result = Language::da_DK;
        else if (appleResult == "pl")
            result = Language::pl_PL;
        else if (appleResult == "ru")
            result = Language::ru_RU;
        else if (appleResult == "th")
            result = Language::th_TH;
        else if (appleResult == "el")
            result = Language::el_GR;
        else if (appleResult == "ro")
            result = Language::ro_RO;
        else if ( string(appleResult.c_str(),0,2) == "zh" )
            result = Language::zh_TW;
        else if ( string(appleResult.c_str(),0,2) == "en" )
            result = Language::en_GB;
        return result;
    }
}
