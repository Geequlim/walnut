//
//  Macros.h
//  Walnut
//
//  Created by Geequlim on 15/8/11.
//
//

#ifndef Walnut_Macros_h
#define Walnut_Macros_h

// Android
#ifdef __ANDROID__
//Window64
#elif _WIN64
    #ifndef __WIN64__
    #define __WIN64__ 1
    #endif
    #ifndef __WINDOWS__
    #define __WINDOWS__ 1
    #endif
//Window32
#elif _WIN32
    #ifndef __WIN32__
    #define __WIN32__ 1
    #endif
    #ifndef __WINDOWS__
    #define __WINDOWS__ 1
    #endif

#elif __APPLE__
    #include "TargetConditionals.h"
    //iOS Simulator
    #if TARGET_OS_IPHONE && TARGET_IPHONE_SIMULATOR
        #ifndef __IOS__
        #define __IOS__ 1
        #endif
        #ifndef __IOS_SIMULATOR__
        #define __IOS_SIMULATOR__ 1
    #endif
    //iOS
    #elif TARGET_OS_IPHONE
        #ifndef __IOS__
        #define __IOS__ 1
        #endif
    //Mac OSX
    #else
        #ifndef __OSX__
        #define __OSX__ 1
        #endif
        #ifndef __MAC_OSX__
        #define __MAC_OSX__ 1
        #endif
    #endif
//Linux
#elif __linux
    #ifndef __LINUX__
    #define __LINUX__ 1
    #endif
//Unix
#elif __unix // all unices not caught above
    #ifndef __UNUX__
    #define __UNUX__ 1
    #endif
//Posix
#elif __posix
    #ifndef __POSIX__
    #define __POSIX__ 1
    #endif
#endif


#if __IOS__ || __ANDROID__
    #ifndef __MOBILE__
    #define __MOBILE__ 1
    #endif
#elif __WINDOWS__ || __LINUX__ || __MAC_OSX__ || __UNUX__
    #ifndef __DESKTOP__
    #define __DESKTOP__ 1
    #endif
    #ifndef __PC__
    #define __PC__ 1
    #endif
#endif

#if __LINUX__ || __UNUX__
    #ifndef __GTK__
    #define __GTK__ 1
    #endif
#endif

#include <memory>
#include <functional>
#include <string>
using std::string;
using std::wstring;
#include <vector>
using std::vector;
#include <list>
using std::list;
#include <map>
using std::map;
#include <sstream>
using std::stringstream;
#include <initializer_list>
using std::initializer_list;
#include <ratio>
#include<chrono>
using std::chrono::duration_cast;
#include <climits>
#include <algorithm>



namespace Walnut
{

    template < typename T > std::string to_string( const T& n )
    {
        std::ostringstream stm ;
        stm << n ;
        return stm.str() ;
    }

/// Enumeration for platforms
enum class Platform : unsigned char
{
    Unix,
    Posix,
    Linux,
    MacOSX,
    Windows,
    iOS,
    Android
};

/// The current platform value
extern const Platform currentPlatform;

/// The nullstr can be treated as an empty string
extern const string nullstr;

/// The nullwstr can be treated as an empty string
extern const wstring nullwstr;

/// The Number type definition of the engine
using Number = float;

/// The Byte type definition
using Byte = unsigned char;

/// Seconds duration type
using seconds_t = std::chrono::duration<double>;
/// Minutes duration type
using minutes_t = std::chrono::duration<double,std::ratio<60> >;
/// Hours duration type
using hours_t = std::chrono::duration<double,std::ratio<3600> >;
/// Milliseconds duration type
using milliseconds_t = std::chrono::milliseconds;
/// Nanoseconds duration type
using nanoseconds_t = std::chrono::nanoseconds;
/// Microseconds duration type
using microseconds_t = std::chrono::microseconds;
}
#endif
