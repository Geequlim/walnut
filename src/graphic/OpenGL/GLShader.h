//
//  GLSLShader.h
//  Walnut
//
//  Created by Geequlim on 15/8/5.
//
//

#ifndef __Walnut__GLSLShader__
#define __Walnut__GLSLShader__
#include <climits>

namespace Walnut
{
    namespace OpenGL
    {
        /// OpenGL Shader Type
        enum GLShaderType : GLuint
        {
            /// Undifined Shader type
            stUndifined      = 0U,
            /// Vertex Shader
            stVertexShader   = GL_VERTEX_SHADER,
            /// Fragment Shader
            stFragmentShader = GL_FRAGMENT_SHADER,
        };
        
        /// OpenGL Shader class
        class GLShader : public Object
        {
            typedef Object super;
        public:
            /// Construct an undefined shader
            GLShader():super(),m_type(GLShaderType::stUndifined),m_strSourceCode(nullstr){}
            
            /*!
             @brief  Construct a shader with shader type
             @param _type Shader type
             */
            GLShader(GLShaderType _type):super(),m_type(_type),m_strSourceCode(nullstr){}
            
            /**
             * @brief Construct a shader with shader type and shader source code
             *
             * @param _type Shade type
             * @param _sourceCode GLSL source code
             */
            GLShader(GLShaderType _type , const string& _sourceCode):super()
            {
                m_type = _type;
                m_strSourceCode = _sourceCode;
            }
            
            virtual ~GLShader(){}
            
            /*!
             @brief  Get shader type
             @return The shader type
             */
            inline GLShaderType type()const { return m_type;}
            
            /*!
             @brief  Set shader type ,that will invalidate this shader
             @param _type The type to set
             */
            inline void setType(GLShaderType _type) { m_type = _type; }
            
            /*!
             @brief  Get the GLSL source code of the shader
             @return
             */
            inline const string & sourceCode()const { return m_strSourceCode;}
            
            /*!
             @brief  Set the GLSL source code of the shader
             @param _src The GLSL source code to set
             */
            inline void setSourceCode(const string& _src){ m_strSourceCode = _src;}
            
            
            /** Check if is the shader is valid simplely
             *
             *  @return Return true if the type of the shader is defined and the source of the shader is not nullstr
             */
            inline bool valid()const
            {
                return m_type != GLShaderType::stUndifined && sourceCode().length();
            }
            
            /** 
             *  @brief Compile the shader source code
             *  @param shaderID The shader object ID compile to
             *  @throws Throw compile errors
             */
            void compile(GLuint shaderID)const;
            
            /**
             *  @brief Load GLSL Source code from file
             *  @param path The path of the source code file
             *  @throws Throw I/O exceptions
             *  @todo Load files with file system
             */
            void loadSourceFromeFile(const string& path);
            
        protected:
            string m_strSourceCode; ///< GLSL source code of the shader
            GLShaderType m_type;    ///< Type of the shader
        };
    }
}

#endif
