#ifndef __Walnut__Shader__H__
#define __Walnut__Shader__H__

namespace Walnut
{
    namespace OpenGL
    {
        /**
         * @brief GLSL program in OpenGL pipeline
         * @par Each GLProgram object has its only OpenGL program object ID,it won't be change by copying or assigning.
         */
        class GLProgram : public Object
        {
        public:
            
            /// Construct a GLProgram
            GLProgram()
            {
                m_programID = glCreateProgram();
            }
            
            /// Construct a GLProgram with shader map
            GLProgram( const std::map<GLShaderType,GLShader>& _shaders )
            {
                m_programID = glCreateProgram();
                setShaders(_shaders);
            }
            
            /// Construct a GLProgram with shaders
            GLProgram( const std::initializer_list<GLShader>& _shaders )
            {
                m_programID = glCreateProgram();
                for( const GLShader& cs: _shaders)
                    m_shaders[cs.type()] = cs;
            }
            
            /*!
             @brief Copy construct a GLProgram with its shaders
             @param another The shader copy from
             @note The created Program has it own OpenGL program ID
             */
            GLProgram(const GLProgram& another)
            {
                m_programID = glCreateProgram();
                m_shaders = another.m_shaders;
            }
            
            /*!
             @brief  Assign to another GLProgram object
             @param another The program assign from
             @note OpenGL program object ID will not be changed
             */
            inline GLProgram& operator=(const GLProgram& another)
            {
                m_shaders = another.m_shaders;
                return *this;
            }
            
            virtual ~GLProgram(){ glDeleteProgram(m_programID); }
            
            /*!
             @brief  Set Shaders with shader map
             @param _shaders  The shader map
             */
            void setShaders(const std::map<GLShaderType,GLShader>& _shaders)
            {
                for( const auto & curPair : _shaders)
                    m_shaders[curPair.first] = GLShader(curPair.first, curPair.second.sourceCode());
            }
            
            /**
             *  @brief Get shader with shader type
             *  @param _type Shader type
             *  @return Return the shader the same type, if has no such type shader create one and add it to program then return it
             */
            inline GLShader& operator[](GLShaderType _type)
            {
                GLShader& shader = m_shaders[_type];
                shader.setType(_type);//if has no such type shader create one and add it to program
                return shader;
            }
            
            /*!
             @brief  Get the OpenGL program object id
             @return The OpenGL program object id
             */
            inline GLuint programID() const { return m_programID; }
            
            /*!
             @brief  Get shader map
             @return The map contains shaders in type-id format
             */
            inline  const std::map<GLShaderType,GLuint>& shaders(){return m_shaderIDS;}
            
            /**
             *  @brief Complie shaders and link program together
             *  @throws Throw the errors occured in compiling or linking
             */
            void validate();
            
            /// Rebind to a new OpenGL program object , the old one will be deleted
            void rebind()
            {
                glDeleteShader(m_programID);
                m_programID = glCreateProgram();
            }
            
            /// Use the OpenGL program
            inline void use() const { glUseProgram(m_programID); }
            
            /*!
             @brief  Set 4x4 matrix Uniform value of the program
             @param _uniformName The uniform variable name
             @param _mat4        The matrix to set
             */
            void setMatrix4Uniform(const string & _uniformName , const  Matrix4& _mat4);
            
            /*!
             @brief  Set 3D vector Uniform value of the program
             @param _uniformName The uniform variable name
             @param _vec3        The vector to set
             */
            void setVec3Uniform(const string & _uniformName , const Vec3& _vec3);
            
            /*!
             @brief  Set 4D vector Uniform value of the program
             @param _uniformName The uniform variable name
             @param _vec4        The vector to set
             */
            void setVec4Uniform(const string & _uniformName , const Vec4 & _vec4);
            
            /*!
             @brief  Get Attribute location in the OpenGL program
             @param attribName The attribute name
             @return The location of the attribute in the OpenGL program
             */
            GLint attribLocation(const string& attribName )
            {
                return glGetAttribLocation(m_programID,attribName.c_str());
            }

			/*!
			 @brief  Get uniform location in the OpenGL program
			 @param uniformName The uniform name
			 @return The location of the uniform in the OpenGL program
			*/
			GLint uniformLocation(const string& uniformName)
			{
				return glGetUniformLocation(m_programID, uniformName.c_str());
			}
            
        protected:
            GLuint m_programID; ///< OpenGL program object ID
            std::map<GLShaderType,GLShader> m_shaders; ///< Shader map
            std::map<GLShaderType,GLuint> m_shaderIDS; ///< Shader ID map
        };
    }
}

#endif
