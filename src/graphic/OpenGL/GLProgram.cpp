//
//  GLProgram.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/6.
//
//

#include "OpenGL.h"
namespace Walnut
{
    namespace OpenGL
    {
        //Compile and link
        void GLProgram::validate()
        {
            //Compile and attach shaders to program
            for ( const auto & curPair : m_shaders )
            {
                const GLShader & shader = curPair.second;
                if( shader.valid() )
                {
                    GLuint shaderID = glCreateShader( shader.type() );
                    if( shaderID )
                    {
                        shader.compile(shaderID);
                        glAttachShader(m_programID,shaderID);
                        glDeleteShader(shaderID);
                        m_shaderIDS[shader.type()] = shaderID;
                    }
                    else
                        throw Exception("OpenGL error: create shader failed!",RENDER_EXCEPTION);
                }
            }
            
            //Link program
            glLinkProgram(m_programID);
            
            //Check link errors
            GLint succeed;
            glGetProgramiv(m_programID,GL_LINK_STATUS,&succeed);
            if(!succeed)
            {
                GLint logLenght;
                glGetProgramiv(m_programID,GL_INFO_LOG_LENGTH,&logLenght);
                GLchar * logInfo = new GLchar[logLenght];
                glGetProgramInfoLog(m_programID,logLenght,0,logInfo);
                string error = "Link GLSL Program error!\n";
                error += logInfo;
                delete logInfo;
                throw Exception(error,RENDER_EXCEPTION);
            }
        }
        
        //      Uniform Setters
        //--------------------------------------
        
        void GLProgram::setMatrix4Uniform(const string & _uniformName , const Matrix4 & _mat4)
        {
            GLuint loc = glGetUniformLocation(m_programID, _uniformName.c_str());
            glUniformMatrix4fv(loc, 1, GL_FALSE, _mat4);
        }
        
        void GLProgram::setVec3Uniform(const string & _uniformName , const Vec3 & _vec3)
        {
            GLuint loc = glGetUniformLocation(m_programID, _uniformName.c_str());
            glUniform3fv(loc,1,_vec3);
        }
        
        void GLProgram::setVec4Uniform(const string & _uniformName , const Vec4 & _vec4)
        {
            GLuint loc = glGetUniformLocation(m_programID, _uniformName.c_str());
            glUniform4fv(loc,1,_vec4);
        }
        //----------------------------------------
    }
}