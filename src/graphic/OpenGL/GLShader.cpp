//
//  GLSLShader.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/5.
//
//
#include "OpenGL.h"
#include <fstream>

namespace Walnut
{
    namespace OpenGL
    {
        
        //Compile the shader source code
        void GLShader::compile(GLuint shaderID)const
        {
            if( valid() != true )
                throw std::runtime_error("Compile Shader error!\n"
                                         "Shader is invalid.\n");
            
            const GLchar * ptr = m_strSourceCode.c_str();
            glShaderSource(shaderID, 1, &ptr, nullptr);
            glCompileShader(shaderID);
            
            //Check errors
            GLint succeed;
            glGetShaderiv(shaderID,GL_COMPILE_STATUS,&succeed);
            if(!succeed)
            {
                GLint logLength;
                glGetShaderiv(shaderID,GL_INFO_LOG_LENGTH,&logLength);
                GLchar * compileInfo = new GLchar[logLength];
                glGetShaderInfoLog(shaderID,logLength,nullptr,compileInfo);
                string error = "Compile Shader error!\nShader type is : ";
                error += to_string(m_type);
                error += "\nGLSL Complier:\n";
                error += compileInfo;
                error += "\nShader code:\n";
                error += m_strSourceCode;
                delete compileInfo;
                throw std::runtime_error(error);
            }
        }
        

        // Load GLSL Source code from file
        void  GLShader::loadSourceFromeFile(const string& path)
        {
            std::fstream sourceCodeFile(path,std::ios::in);
            if(!sourceCodeFile.is_open())
            {
                string error("Load Shader error\n");
                error += "Open source file from ";
                error += path;
                error += " failed!\n";
                throw std::runtime_error(error);
            }
            std::stringstream strm;
            strm << sourceCodeFile.rdbuf();
            m_strSourceCode = strm.str();
            sourceCodeFile.close();
        }
    }
}
