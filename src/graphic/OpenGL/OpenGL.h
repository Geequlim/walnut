//
//  OpenGL.h
//  Walnut
//
//  Created by Geequlim on 15/8/5.
//
//

#ifndef __Walnut_OpenGL_h__
#define __Walnut_OpenGL_h__

#include "Macros.h"
#ifdef __ANDROID__
    #include <EGL/egl.h>
    #include <GLES2/gl2.h>
    #include <GLES2/gl2ext.h>
    #	define GL_RGBA8              GL_RGBA
    #	define glBindVertexArray     glBindVertexArrayOES
    #	define glDeleteVertexArrays  glDeleteVertexArraysOES
    #	define glGenVertexArrays     glGenVertexArraysOES
#elif __DESKTOP__
    #include <glad/glad.h>
    #include "GLFW/glfw3.h"
#endif

#include "math/Math.hpp"
#include "../../core/Object.h"
#include "../../core/Exception.hpp"

#include "GLShader.h"
#include "GLProgram.h"

#endif
