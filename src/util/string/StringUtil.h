﻿//
//  StringUtil.h
//  Walnut
//
//  Created by Geequlim on 15/5/10.
//
//

#ifndef __Walnut_Util_STRING_H__
#define __Walnut_Util_STRING_H__

namespace Walnut
{
    namespace Util
    {
        namespace String
        {
            /**
             *  @brief Split a string by a string seperator
             *  @param soueceStr     The source string to be splited
             *  @param pattern The string seperator
             *  @return A vector contains all substrings splited from source string
             */
            vector<string> split(const string& soueceStr,const string& pattern);
            
            /**
             *  @brief Set all characters to upper style
             *  @param str The string need to change
             */
            void toUpper(string & str);
            
            /**
             *  @brief Get a upper style string from a source string
             *  @param sourceStr The source string
             *  @return Return a string with all characters is upper style
             */
            string getUpper(const string& sourceStr);
            
            /**
             *  @brief Set all characters to lower style
             *  @param str The string need to change
             */
            void toLower(string & str);
            

            /**
             *  @brief Get a lower style string from a source string
             *  @param sourceStr The source string
             *  @return Return a string with all characters is lower style
             */
            string getLower(const string& sourceStr);
            
            /**
             *  @brief Set the string to C-liked language indentifier,all illlegal characters will be replaced to @e replaceChar
             *  @param sourceStr  The string need change to indentifier
             *  @param replaceChar The replacement character, all illlegal characters will be replaced to this character.@e '_' as the default
             */
            void toIdentifier(string & sourceStr ,const char & replaceChar='_');
           
            /**
             *  @brief Get the string to C-liked language indentifier from  source string ,all illlegal characters will be replaced to @e replaceChar
             *  @param sourceStr  The string need change to indentifier
             *  @param replaceChar The replacement character, all illlegal characters will be replaced to this character.@e '_' as the default
             *  @return Return a string for indentifier name
             */
            string getIdentifier(const string & sourceStr,const char & replaceChar='_');


#if  __WINDOWS__

			/*! 
				@brief Convert utf-8 string to wide string
				@param utf8Str The source string to convert
				@return The generated wide string 
			 */
			wstring string2wstring(const string& utf8Str);

			/*!
				@brief Convert wide string to utf-8 string
				@param wideStr The source string to convert
				@return The generated string
			*/
			string wstring2string(const wstring& wideStr);

			/*!
				@brief Convert multibyte string to wide string
				@param multibyteStr The source string to convert
				@return The generated wide string
			*/
			wstring multibyteString2wstring(const string& multibyteStr);

			/*!
				@brief Convert wide  string to wide string
				@param wideStr The source string to convert
				@return The generated multibyte string
			*/
			string wstring2multibyteString(const wstring& wideStr);
#endif
        }
    }
}

#endif
