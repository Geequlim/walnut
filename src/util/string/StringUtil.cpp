//
//  StringUtil.cpp
//  Walnut
//
//  Created by Geequlim on 15/5/10.
//
//
#include "Macros.h"
#include "../util.h"

#if __WINDOWS__
#include <Windows.h>
#include <Winnls.h>
#endif

namespace Walnut
{
    namespace Util
    {
        namespace String
        {

            // Set to upper style
            void toUpper(string & str)
            {
                std::transform(str.begin(),str.end(),str.begin(),::toupper);
            }

            // Get upper style string
            string getUpper(const string& sourceStr)
            {
                string str(sourceStr);
                toUpper(str);
                return str;
            }

            // Set to lower style
            void toLower(string & str)
            {
                std::transform(str.begin(),str.end(),str.begin(),::tolower);
            }


            // Get lower style string
            string getLower(const string& sourceStr)
            {
                string str(sourceStr);
                toLower(str);
                return str;
            }

            // Set to indentifier
            void toIdentifier(string & sourceStr ,const char & replaceChar)
            {
                const string avaliableCharSet =
                    "_$0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
                //set replace character
                string replaceHolder(1,replaceChar);
                if( replaceHolder.find_first_not_of(avaliableCharSet)!= string::npos || isdigit( replaceChar) )
                    replaceHolder = "_";
                // replace all illegel characters
                string::size_type firstIllegalPos = sourceStr.find_first_not_of(avaliableCharSet);
                while( firstIllegalPos !=string::npos )
                {
                    sourceStr.replace(firstIllegalPos, 1, replaceHolder );
                    firstIllegalPos = sourceStr.find_first_not_of(avaliableCharSet);
                }
                // Correct first character is number
                if( isdigit( (*sourceStr.c_str()) ))
                     sourceStr.replace(0, 1, replaceHolder );
            }

            // Get indentifier string
            string getIdentifier(const string & sourceStr,const char & replaceChar)
            {
                std::string identifier(sourceStr);
                toIdentifier(identifier,replaceChar);
                return identifier;
            }

            // split string to vector
            std::vector<string> split(const string & soueceStr,const string& pattern)
            {
                string::size_type pos;
                std::vector<string> result;
                //expend for convenience
                string str( soueceStr + pattern);
                string::size_type  size =str.size();
                for( string::size_type i=0; i<size; i++)
                {
                    pos=str.find(pattern,i);
                    if(pos<size)
                    {
                        string s=str.substr(i,pos-i);
                        result.push_back(s);
                        i=pos+pattern.size()-1;
                    }
                }
                return result;
            }

#if __WINDOWS__
			wstring string2wstring(const string& utf8Str)
			{
				int len = MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, NULL, 0);
				if (len == 0)
					return nullwstr;
				vector<wchar_t> unicode(len);
				MultiByteToWideChar(CP_UTF8, 0, utf8Str.c_str(), -1, &unicode[0], len);
				return wstring(&unicode[0]);
			}

			string wstring2string(const wstring& wideStr)
			{
				int len = WideCharToMultiByte(CP_UTF8, 0, wideStr.c_str(), -1, NULL, 0, NULL, NULL);
				if (len == 0)
					return nullstr;
				vector<char> utf8(len);
				WideCharToMultiByte(CP_UTF8, 0, wideStr.c_str(), -1, &utf8[0], len, NULL, NULL);
				return string(&utf8[0]);
			}

			wstring multibyteString2wstring(const string& multibyteStr)
			{
				int len = MultiByteToWideChar(CP_ACP, 0, multibyteStr.c_str(), -1, NULL, 0);
				if (len == 0)
					return nullwstr;
				vector<wchar_t> unicode(len);
				MultiByteToWideChar(CP_ACP, 0, multibyteStr.c_str(), -1, &unicode[0], len);
				return wstring(&unicode[0]);
			}

			string wstring2multibyteString(const wstring& wideStr)
			{
				int len = WideCharToMultiByte(CP_ACP, 0, wideStr.c_str(), -1, NULL, 0, NULL, NULL);
				if (len == 0)
					return nullstr;
				vector<char> utf8(len);
				WideCharToMultiByte(CP_ACP, 0, wideStr.c_str(), -1, &utf8[0], len, NULL, NULL);
				return string(&utf8[0]);
			}
#endif
        }
    }
}
