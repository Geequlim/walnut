#include "../util.h"
#ifdef __WIN32__
#include <fstream>
#include <io.h>
#include <direct.h>
#include <Windows.h>
#else
#include <dirent.h>
#include <unistd.h>
#include <cstring>
#endif

namespace Walnut
{
    namespace Util
    {
        vector<string> getFilePathes(const string & basePath , const bool recurseSubDir)
        {
            vector<string> pathes;
            //verify basePath
#ifdef WIN32
            long   hFile = 0;//windows file
            struct _finddata_t fileinfo;
            if ((hFile = _findfirst((basePath + "/*").c_str(), &fileinfo)) == -1)
                return pathes;
#else
            DIR *dir;
            struct dirent *ptr;
            if ((dir = opendir(basePath.c_str())) == NULL)
                return pathes;
#endif
            //ignore '.' and '..'
#ifdef WIN32
            do{
                if (strcmp(fileinfo.name, ".") == 0 || strcmp(fileinfo.name, "..") == 0)
                    continue;
#else
                while ((ptr = readdir(dir)))
                {
                    if (strcmp(ptr->d_name, ".") == 0 || strcmp(ptr->d_name, "..") == 0)
                        continue;
#endif
                    //Get files in this directory
                    std::string curFilePath;
                    //Recursiving searching
#ifdef WIN32
                    curFilePath = basePath + "/" + fileinfo.name;
                    if ((fileinfo.attrib &  _A_SUBDIR) && recurseSubDir)
#else
                        curFilePath = basePath + "/" + ptr->d_name;
                    if (ptr->d_type == 4 && recurseSubDir)
#endif
                    {
                        vector<string> subPathes = getFilePathes(curFilePath, recurseSubDir);
                        //Recursiving searching children directories,and insert to pathes
                        pathes.insert(pathes.end(), subPathes.begin(),subPathes.end());
                    }
                    //Add to files list
                    pathes.push_back(curFilePath);
                    //Close file
#ifdef WIN32
                }while( _findnext(hFile, &fileinfo)==0);
                _findclose(hFile);
#else
            }
            closedir(dir);
#endif
            return pathes;
        }
    }
}