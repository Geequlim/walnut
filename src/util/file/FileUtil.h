//
//  Created by Geequlim on 15/5/15.
//
//

#ifndef __Walnut__FileUtil__
#define __Walnut__FileUtil__

namespace Walnut
{
    namespace Util
    {
        /**
         *  @brief List all files in directory with absolute pathes
         *  @param basePath      Search directory
         *  @param recurseSubDir Should search children directories recursively
         *  @return Return a vector contains all found files
         */
        vector<string> getFilePathes(const string & basePath , const bool recurseSubDir = false);
    }
}

#endif
