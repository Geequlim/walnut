#ifndef __Walnut_NativeMessageDialog_H__
#define __Walnut_NativeMessageDialog_H__

namespace Walnut
{
	namespace Util
	{
		/// Meesage dialog with native user interfaces
		class MessageDialog final: public EventDispatcher
		{
			using super = EventDispatcher;
		public:
            /*!
             @brief  Construct a message dialog
             @param title   The dialog title
             @param message The message content
             @param buttons Button titles
             */
			MessageDialog(const string& title,
						  const string& message ,
						  const vector<string>& buttons
						 ):super()
			{
				m_title = title;
				m_message = message;
				m_buttons = buttons;
			}
			
            /// Get button titles
			const vector<string>& buttons()const{ return m_buttons; }
			
            /// Get writable button titles
            vector<string>& buttons(){ return m_buttons; }
			
            /// Set button titles
            void setButtons(const vector<string>& buttons){ m_buttons = buttons;}
            
            /// Get message content
			const string& message()const { return m_message; }
            
            /// Set message content
            void setMessage(const string& msg){ m_message = msg;}
			
            /// Get dialog title
			const string& title()const{ return m_title; }
            
            /// Set dialog title
			void setTitle(const string& title){ m_title = title;}
			
            /// Show the dialog
			void show();
			
            /// Get the response button title
			const string& responseButtonTitle()const
			{
				const string* buttonTitle = &nullstr;
				if( m_responseIndex>=0 && m_responseIndex<m_buttons.size() )
					buttonTitle = &m_buttons.at(m_responseIndex);
				return *buttonTitle;
			}
            
			/// Get the response button index starts with 0
            int responseButtonIndex()const { return m_responseIndex; }
            
		private:
            /// The reso
			int m_responseIndex;
            /// Dialog title
			string m_title;
            /// Message content
			string m_message;
            /// Button title
			vector<string> m_buttons;
		};
	}
}
#endif