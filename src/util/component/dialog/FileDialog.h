﻿#ifndef __Walnut_FileDialog_H__
#define __Walnut_FileDialog_H__
namespace Walnut
{
    namespace Util
    {
        /*!
          @brief File dialog for multi-platforms to select open and save file path
          @par Support select file(s) and directory(directories)
          @par Support set select filters
          @par It use native user interface on Windows,Mac OSX and Linux(GTK+3.0 is required)
		  @attention Select both file and directory mode is not supported on Linux if you set to this mode the dialog only allows folder selection
		  @attention Select both file and directory mode is not full supported on windows for select multiple both file and dirctory mode  
          @note Dispatching NormalEventType::CONFIRMED event when selection confirmed or dispatching NormalEventType::CANCELED event when selection canceled
         */
        class FileDialog  final: public EventDispatcher
		{
            typedef EventDispatcher super;
		public:
            
            /// Dialog selection mode
            enum FileDialogMode : unsigned int
            {
				/// Select file mode
                SELECT_FILE     = 1,
				/// Select directory mode
                SELECT_DIR      = 2,
                /// Select file and directory mode
                SELECT_FILE_DIR = SELECT_FILE | SELECT_DIR ,
                /// Allows multi selection , Windows does not support multiselect directorys
                MULTI_SELECT    = 4,
                SELECT_TO_SAVE  = 8///< Select save file mode
            };
            
            ///Default constructor
			FileDialog() :super()
			{
                m_eventOwner = nullptr;
                m_pHostWnd   = nullptr;
				cleanUp();
			}
            
            /*!
             @brief  Construct a file dialog
             @param title    The title text of the dialog
             @param mode     Selection mode
             @param eventOwner The event owner pointer
             @param pHostWnd The host window pointer
             */
            FileDialog(const string& title,int mode,
                       Object* eventOwner = nullptr,RenderWindowBase* pHostWnd = nullptr):super()
            {
                m_eventOwner = eventOwner;
                m_pHostWnd   = pHostWnd;
                m_mode = mode;
                m_wndTitle = title;
            }
            
			~FileDialog()
            {
                if(m_eventOwner)
                {
                    m_eventOwner->release();
                    m_eventOwner = nullptr;
                }
                if(m_pHostWnd)
                {
                    m_pHostWnd->release();
                    m_pHostWnd = nullptr;
                }
            }

			/// Clean up all configrations and options
			inline void cleanUp()
			{
                m_mode        = SELECT_FILE;
                m_wndTitle    = nullstr;
                m_defaultPath = nullstr;
                setEventOwner(nullptr);
                setHostWindow(nullptr);
                m_filter.clear();
			}

            /// Show dialog
            void show();
            
            /*!
             @brief Check if is saving file mode
             @return Is save mode
             */
            bool saveMode()const{ return m_mode & SELECT_TO_SAVE; }
            
            /*!
             @brief Set to if is  saving file mode
             @param saveMode Is save mode
             */
            void setSaveMode( bool saveMode)
            {
                if(saveMode)
                    m_mode |= SELECT_TO_SAVE;
                else
                    m_mode &= ~SELECT_TO_SAVE;
            }
            
            /*!
             @brief  Check allows to multi-selection
             @return Allows multi-selection
             */
            bool allowsMultipleSelection()const{ return m_mode & MULTI_SELECT; }
            
            /*!
             @brief  Set to if is allowed to multi-selection
             @param allowded Allows multi-selection
             */
            void setAllowsMultipleSelection(bool allowded)
            {
                if(allowded)
                    m_mode |= MULTI_SELECT;
                else
                    m_mode &= ~MULTI_SELECT;
            }
            

            /*!
             @brief  Check if allows to select file(s)
             @return Select file(s) allowded
             */
            bool allowsFileSelection()const { return m_mode & SELECT_FILE; }
            
            /*!
             @brief  Set if is allowed to select file(s)
             @param allowed Allows select file
             */
            void setAllowsFileSelection(bool allowed)
            {
                if(allowed)
                    m_mode |= SELECT_FILE;
                else
                    m_mode &= ~SELECT_FILE;
            }
            
            /*!
             @brief  Check if allows to select directory(directories)
             @return Allowed to select directory
             */
            bool allowsDirectorySelection()const { return m_mode & SELECT_DIR; }
            
            /*!
             @brief  Set if is allowed to select directory(directories)
             @param allowded Allowded to select directory
             */
            void setAllowsDirectorySelection(bool allowded )
            {
                if(allowded)
                    m_mode |= SELECT_DIR;
                else
                    m_mode &= ~SELECT_DIR;
            }
            
            /*!
             @brief  Set dialog title text
             @param title Title text
             */
            void setTitle(const string& title){ m_wndTitle = title; }
            
            /*!
             @brief  Get dialog title text
             @return Dialog title text
             */
            const string& title()const { return m_wndTitle;}
            
            /*!
             @brief Set default selected path
             @param initDir The default selected path(absolute path)
             */
            void setDefaultPath(const string& initDir){ m_defaultPath = initDir;}
            
            /*!
             @brief  Get default selected path
             @return The default selected path(absolute path)
             */
            const string& defaultPath()const { return m_defaultPath;}
            
            /*!
             @brief Add selection filter
             @param label Filter name. Invisiable on Mac OSX
             @param types Fileter's extention name.
                    @n Does not contains '.'
                    @n '*' to allow all files.for more extentions,seperate with ';' e.g “txt;TXT” to allow select files name ends with '.txt' and '.TXT'
             @attention:
                   @li It is not allowed to use '*' to select all files on Mac OSX.
                   @li If you have to select all files just leave the filter empty for all platform.
             */
            void addFilter(const string & label ,const string &types)
            {
                m_filter.push_back(std::pair<string, string>(label,types));
            }
            
            /// Clear filters that means allow to select all kind of files
            void clearFilters(){ m_filter.clear(); }
            
            /*!
             @brief  Get selected pathes
             @return The vector contains all selected pathes( abusolute pathes )
             */
            const vector<string>& selectedPathes()const{  return m_selectedPathes; }
            
            /*!
             @brief  Get the read only dialog's event owner
             @return The dialog event owner
             */
            const Object* eventOwner()const{  return m_eventOwner; }
            
            /*!
             @brief  Get the writable dialog's event owner
             @return The dialog event owner
             */
            Object* eventOwner(){ return m_eventOwner;}
            
            /*!
             @brief  Set the dialog's event owner
             @param obj The event owner object
             */
            void setEventOwner(Object* obj)
            {
                if (m_eventOwner != obj)
                {
                    if (m_eventOwner)
                        m_eventOwner->release();
                    m_eventOwner = obj;
                    if (m_eventOwner)
                        m_eventOwner->retain();
                }
            }
            
            /*!
             @brief  Get the read only host window of the dialog
             @return The host window
             */
            const RenderWindowBase* hostWindow()const{ return m_pHostWnd; }

            /*!
             @brief  Get the writable host window of the dialog
             @return The host window
             */
            RenderWindowBase* hostWindow(){return m_pHostWnd;}
            
            /*!
             @brief  Set the host window of the dialog
             @param pWnd The window to host the dialog
             */
            void setHostWindow(RenderWindowBase* pWnd)
            {
                if (m_pHostWnd != pWnd)
                {
                    if (m_pHostWnd)
                        m_pHostWnd->release();
                    m_pHostWnd = pWnd;
                    if (m_pHostWnd)
                        m_pHostWnd->retain();
                }
            }
            
            /**
             *  @brief Set dialog mode
             *
             *  @param mode The mode e.g (SELECT_FILE | SELECT_DIR | MULTI_SELECT) allows to select files and dirctories
             */
            void setMode(unsigned int mode){ m_mode = mode; }

            /*!
             @brief  Get the dialog selection mode
             @return The selection mode
             */
            unsigned int mode()const{ return m_mode;}
            
		protected:
			/// Dialog title text
			string m_wndTitle; 
			/// Default selected path
			string m_defaultPath;
			/// Dialog select mode
			unsigned m_mode;
			/// Filters for select files
            vector<std::pair<string,string> > m_filter;
			/// Selected pathes (absolute pathes)
            std::vector<string> m_selectedPathes;
			/// The event owner
            Object * m_eventOwner;
			/// Dialog's host window 
            RenderWindowBase * m_pHostWnd; 
		};
    }
}
#endif
