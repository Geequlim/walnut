#ifndef __Walnut_ColorPicker_Dialog_H__
#define __Walnut_ColorPicker_Dialog_H__


namespace Walnut
{
    namespace Util
    {
        /// Color Chooser Dialog
        class ColorPickerDialog final: public EventDispatcher 
        {
            using super = EventDispatcher;
        public:
            /*!
             @brief  Construct a color picker dialog
             @param title The dialog title
             */
            ColorPickerDialog(const string& title):super(),m_title(title) {}
            ~ColorPickerDialog();
            
            /// Show the color chooser dialog
            void show();
            
            /// Set selected color of the dialog
            void setColor( const Color& color ){ m_color = color; }
            
            /// Get selected color of the dialog
            const Color& color()const{return m_color;}
            
            /// Get dialog title content
            const string& title()const{ return m_title; }
            
            /// Set dialog title content
            void setTitle(const string& title){ m_title = title; }
            
        private:
            /// Selected color
            Color m_color;
            /// Dialog title content
            string m_title;
        };
    }
}

#endif