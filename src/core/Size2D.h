#ifndef __Walnut_Size2D_H__
#define __Walnut_Size2D_H__

namespace Walnut
{
    /*! 
     @brief 2D Size is vector that contains two components called width and height
     */
    struct Size2D
    {
        Number width; ///< The width component
        Number height;///< The height component
        
        ///Construct a zero size whose width and height are both 0
        Size2D(){ width = height = 0;}

        /*!
         @brief  Construct a 2D size , it's width and height are the same value
         @param _w_h The width and height value
         */
        Size2D(Number _w_h) { width = height = _w_h; }
        
        /*!
         @brief  Construct a 2D size with its width and height
         @param _width The width of the size
         @param _height The height of the size
         */
        Size2D(Number _width , Number _height)
        {
            width  = _width;
            height = _height;
        }

        /*!
         @brief  Convert to a 2D Point
         @return The point converted from this size
         */
        inline Point toPoint()const { return Point(width , height);}
        
        /*!
         @brief  Reset the the values of the size components
         @param _width  The new width value
         @param _height The new height value
         @return The changed size itself
         */
        inline Size2D& set(Number _width , Number _height)
        {
            width = _width;
            height = _height;
            return *this;
        }

        /*!
         @brief  Get scaled the size from this one
         @param scale The scale for width and height
         @return The size scaled from this one
         */
        inline Size2D operator*(Number scale)const
        {
            Size2D Size2D(width,height);
            Size2D *= scale;
            return Size2D;
        }

        /*!
         @brief  Scale the size
         @param scale The scale for width and height
         @return The changed size itself
         */
        inline Size2D& operator*=(Number scale )
        {
            width  *= scale;
            height *= scale;
            return *this;
        }
        
        /*!
         @brief  Get scaled the size from this one
         @param scale The scale vector,the @e x to scale width and the @e y to scale height
         @return The size scaled from this one
         */
        inline Size2D operator*(const Point& scale)const
        {
            return Size2D( width*scale.x , height*scale.y );
        }
        
        /*!
         @brief  Scale the size
         @param scale The scale vector,the @e x to scale width and the @e y to scale height
         @return The changed size itself
         */
        inline Size2D operator*=(const Point& scale)
        {
            width  *= scale.x;
            height *= scale.y;
            return *this;
        }
        
        /*!
         @brief  Check if is equivalent to another size
         @param another The size to compare with
         @return Is equivalent to @e another
         */
        inline bool operator==(const Size2D& another)const
        {
            return width == another.width && height == another.height;
        }

        /*!
         @brief  Check if is  NOT equivalent to another size
         @param another The size to compare with
         @return Is NOT equivalent to @e another
         */
        inline bool operator!=(const Size2D& another)const
        {
            return ! operator==(another);
        }

        /*!
         @brief  Check if is smaller to another size
         @par That means compare the areas form by the two 2D size
         @param another The size to compare with
         @return Smaller to @e another size
         */
        inline bool operator<(const Size2D& another)
        {
            return Point(width,height) < Point(another.width,another.height);
        }
    };

    /// 2D Size
    typedef Size2D Size;
    
    extern const Size zeroSize; ///< The size whose width = height = 0
}

#endif
