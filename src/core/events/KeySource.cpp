#include "core/core.h"
#include <cctype>

namespace Walnut
{
    using namespace KeySource;

    Byte getCharFromKey( int key, bool withShift )
    {
        Byte theChar = '\0';
        if( key >= KeyCode::Numpad0 && key <= KeyCode::NumpadEqual )
        {
            if( key <= KeyCode::Numpad9 )
                theChar =  KeyCode::Number0 + (key - KeyCode::Numpad0);
            else
            {
                switch(key)
                {
                    case KeyCode::NumpadDecimal:
                        theChar = '.';
                        break;
                    case KeyCode::NumpadDivide:
                        theChar = '/';
                        break;
                    case KeyCode::NumpadMultiply:
                        theChar = '*';
                        break;
                    case KeyCode::NumpadSubtract:
                        theChar = '-';
                        break;
                    case KeyCode::NumpadAdd:
                        theChar = '+';
                        break;
                    case KeyCode::NumpadEnter:
                        theChar = KeyCode::Return;
                        break;
                    case KeyCode::NumpadEqual:
                        theChar = '=';
                        break;
                    default:
                        break;
                }
            }
        }
        else if( isprint(key) )
        {
            theChar = key;
            if( withShift )
            {
                switch(key)
                {
                    case KeyCode::Apostrophe:
                        theChar = '"';
                        break;
                    case KeyCode::Comma:
                        theChar = '<';
                        break;
                    case KeyCode::Hyphen:
                        theChar = '_';
                        break;
                    case KeyCode::Point:
                        theChar = '>';
                        break;
                    case KeyCode::Slash:
                        theChar = '?';
                        break;
                    case KeyCode::Semicolon:
                        theChar = ':';
                        break;
                    case KeyCode::Equals:
                        theChar = '+';
                        break;
                    case KeyCode::LeftBracket:
                        theChar = '{';
                        break;
                    case KeyCode::BackSlash:
                        theChar = '|';
                        break;
                    case KeyCode::RightBracket:
                        theChar = '}';
                        break;
                    case KeyCode::Accent:
                        theChar = '~';
                        break;
                    case KeyCode::Number0:
                        theChar = ')';
                        break;
                    case KeyCode::Number1:
                        theChar = '!';
                        break;
                    case KeyCode::Number2:
                        theChar = '@';
                        break;
                    case KeyCode::Number3:
                        theChar = '#';
                        break;
                    case KeyCode::Number4:
                        theChar = '$';
                        break;
                    case KeyCode::Number5:
                        theChar = '%';
                        break;
                    case KeyCode::Number6:
                        theChar = '^';
                        break;
                    case KeyCode::Number7:
                        theChar = '&';
                        break;
                    case KeyCode::Number8:
                        theChar = '*';
                        break;
                    case KeyCode::Number9:
                        theChar = '(';
                        break;
                    default:
                        break;
                }
            }
        }
        return theChar;
    }
}
