//
//  Event.h
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#ifndef __Walnut__Event__
#define __Walnut__Event__

namespace Walnut
{
    ///Walnut Event class
    class Event : public Object
    {
        typedef Object super;
        friend class EventDispatcher;
    public:

        ///Construct a default Unknowned event
        Event() :super()
        {
            m_nType = NormalEventType::UNKNOWN;
            m_pTarget = nullptr;
            m_boBubbles = false;
            m_boStoped = false;
            m_boCancelable = false;
            m_boCanceled = false;
        }


        /*!
           @brief Construct a Event object
           @param _type      Event type
           @param bubbles    Can be bubbles in dispatcher
           @param cancelable If the event is cancelable
         */
        Event(int _type, bool bubbles = false, bool cancelable = false):super()
        {
            m_nType = _type;
            m_pTarget = nullptr;
            m_boBubbles = bubbles;
            m_boStoped = false;
            m_boCancelable = cancelable;
            m_boCanceled = false;
        }

        /*!
           @brief Construct a Event object by another one
           @param another source event object
         */
        Event(const Event& another):super()
        {
            m_nType = another.m_nType;
            m_boStoped = another.m_boStoped;
            m_boBubbles = another.m_boBubbles;
            m_boCancelable = another.m_boCancelable;
            m_boCanceled = another.m_boCanceled;
            setTarget(another.m_pTarget);
        }

        ///Disable assgin to copy
        Event& operator=(const Event& another) = delete;

        virtual ~Event()
        {
//            if(m_pTarget)
//            {
//                m_pTarget->release();
//                m_pTarget = nullptr;
//            }
        }

        /*!
         @brief  Get the event type
         @return The type code of the event
         */
        inline int type() const { return m_nType; }

        /*!
         @brief  Get the read only target of the event
         @return The pointer of target object
         */
        inline Object* target() { return m_pTarget; }

        /*!
         @brief  Get the writable target of the event
         @return The pointer to target object
         */
        inline const Object* target() const { return m_pTarget; }

        /*!
         @brief  Set the target of the event
         @param target The pointer of target object
         */
        inline void setTarget(Object * target)
        {
//            if (m_pTarget != target)
//            {
//                if (m_pTarget)
//                    m_pTarget->release();
//                m_pTarget = target;
//                if (m_pTarget)
//                    m_pTarget->retain();
//            }
            m_pTarget = target;
        }

        /*!
         @brief  Check if the event can be bulled in dispatchers
         @return Can bubbles in dispatchers
         */
        inline bool bulles() const { return m_boBubbles; }

        /*!
         @brief  Check if the event is cancelable
         @return Can be canceled
         */
        inline bool cancelable() const { return false; }

        /*!
         @brief  Check if the event has been canceled
         @return Been canceled
         */
        inline bool canceled() const { return m_boCanceled; }

        /*!
         @brief  Check if the event has been stopped dispatching
         @return Been stoped
         */
        inline bool stoped() const { return m_boStoped; }

        ///Stop dispatching the event
        inline void stop(){ m_boStoped = true; }

        ///Cancel dispatching the event
        inline void cancel(){ if (m_boCancelable) m_boCanceled = true; }
        
        /*!
         @brief  Check if equals to another event
         @param another The event to compare with
         @return Return true if the types,targets,bublles,cancelable of two events are in the same value
         */
        bool operator==(const Event& another)const
        {
            return m_nType == another.m_nType     &&
                   m_pTarget == another.m_pTarget &&
                   m_boBubbles == another.m_boBubbles &&
                   m_boCancelable == another.m_boCancelable;
        }

    protected:
        int m_nType;        ///< Event type
        Object* m_pTarget;  ///< The target
        bool m_boStoped;    ///< Is stopped
        bool m_boBubbles;   ///< Can bubbles in dispatchers
        bool m_boCancelable;///< Is cancelable
        bool m_boCanceled;  ///< Is canceled
    };
}

#endif
