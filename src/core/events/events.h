#ifndef __Walnut_events_h__
#define __Walnut_events_h__

namespace Walnut
{
    ///Enumeration for some inorganized event types
    enum NormalEventType : int
    {
        UNKNOWN = 0,        ///< Unknown
        RESIZE = 0x70000000,///< Resized
        CLOSEING,           ///< Closing
        CLOSED,             ///< Closed
        OBTAIN_FOCUS,       ///< Obtained focus
        LOST_FOCUS,         ///< Lost focus
        DROP_IN,            ///< Drop in
        DROP_OUT,           ///< Drop out
        SELECTED,           ///< Selected
        UNSELECTED,         ///< Cancel selected
        CONFIRMED,          ///< Confirmed
        CANCELED            ///< Canceled
    };
}

#include "Event.h"
#include "EventListener.h"
#include "EventDispatcher.h"
#include "KeySource.h"
#include "KeyboardEvent.h"
#include "MouseEvent.h"

#endif