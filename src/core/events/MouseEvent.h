//
//  MouseEvent.h
//  Walnut
//
//  Created by Geequlim on 15/8/9.
//
//

#ifndef Walnut_MouseEvent_h
#define Walnut_MouseEvent_h

namespace Walnut
{
    /// Mouse event clas of Walnut
    class MouseEvent : public Event
    {
    public:
        typedef Event super;
        
        /// Mouse event types
        enum Action : int
        {
            MOUSE_MOVE = 0x70000410,///< Mouse Move
            MOUSE_LDOWN,            ///< Mouse left button down
            MOUSE_LUP,              ///< Mouse left button released
            MOUSE_LCLICK,           ///< Mouse left button clicked
            MOUSE_LDOUBLECLICK,     ///< Mouse left button double clicked
            MOUSE_RDOWN,            ///< Mouse right button down
            MOUSE_RUP,              ///< Mouse right button released
            MOUSE_RCLICK,           ///< Mouse right button clicked
            MOUSE_RDOUBLECLICK,     ///< Mouse right button double clicked
            MOUSE_MDOWN,            ///< Mouse middle button down
            MOUSE_MUP,              ///< Mouse middle button released
            MOUSE_MCLICK,           ///< Mouse middle button clicked
            MOUSE_MDOUBLECLICK,     ///< Mouse middle button double clicked
            MOUSE_SCROLL,           ///< Mouse scrolled
            MOUSE_ENTER,            ///< Mouse move enter
            MOUSE_OUT               ///< Mouse move out
        };
        
    
        
    public:
        
        /**
         *  @brief Construct a mouse event
         *  @param type    Mouse event action type
         *  @param bubbles If the event bubbles
         *  @param wndPos  The position of the mouse on the window
         *  @param scroll  The scroll delta information
         *  @param addKeys  Additional keyboard keys
         */
        MouseEvent(int type, bool bubbles, const Point &wndPos,
                 const Point& scroll = zeroPoint, const vector<int>& addKeys = vector<int>() )
         :super(type,bubbles,false)
        {
            m_wndPos  = wndPos;
            m_scroll  = scroll;
            m_addKeys = addKeys;
        }
   
        /*!
         @brief  Get the scroll delta information
         @return The scroll detail
         */
        const Point& scroll()const { return m_wndPos;}
        
        /*!
         @brief  Get the position of the mouse on the window coordinate
         @return The position of the mouse on the window coordinate
         */
        const Point& position()const{ return m_wndPos;}
        
        /*!
         @brief  Get additional keyboard keys
         @return The pressed keycodes in vector
         */
        const vector<int> addKeys()const{ return m_addKeys;}

        /*!
         @brief Check if the KeyCode key is pressed
         @param key The key code to check
         @return Is the key pressed
         */
        bool withKey(KeyCode key)const
        {
           return m_addKeys.end() != std::find(m_addKeys.begin(), m_addKeys.end(), key);
        }
        
    protected:
        Point m_wndPos;       ///< The position of the mouse on the window coordinate
        Point m_scroll;       ///< The scroll delta information
        vector<int> m_addKeys;///< Additional pressed keyboard keys
    };
}

#endif
