//
//  EventDispatcher.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#include "../core.h"

namespace Walnut
{
    //A new listener's ID
    unsigned long long EventDispatcher::newListenerID = 0ULL;
    
    EventDispatcher::EventDispatcher():super()
    {
        m_pDispatchingListener = nullptr;
        m_pPrarent = nullptr;
    }
    
    EventDispatcher::~EventDispatcher()
    {
        for (auto pcurChild : m_childList)
            if(pcurChild) pcurChild->release();
        m_childList.clear();
    }
    
    // Dispatch event
    bool EventDispatcher::dispatchEvent(Event &event)
    {
        if( event.m_boStoped || (event.m_boCancelable && event.m_boCanceled ) )
            return false;
        
        if(!event.target())
            event.setTarget(this);
        //find the listeners listening the event and call thier callbacks
        auto typeMapIt =  m_listeners.find(event.type());
        if( typeMapIt != m_listeners.end() )
        {
            SameTypeListenerMap & typeListeners = typeMapIt->second;
            for (auto it = typeListeners.rbegin(); it!=typeListeners.rend(); ++it)
            {
                const ListenerList & listeners = it->second;
                
                for( const auto & listener : listeners )
                {
                    m_pDispatchingListener = &listener;
                    listener.handler()(event);
                    m_pDispatchingListener = nullptr;
                }
            }
        }
        //bullbus if necessary
        if( event.bulles() && m_pPrarent )
            m_pPrarent->dispatchEvent(event);
        
        return true;
    }
    
    // Broadcast event to children dispatchers and this one
    void EventDispatcher::broadcastEvent(Event& event)
    {
        if( dispatchEvent(event) )
        {
            for( auto child : m_childList)
                child->broadcastEvent(event);
        }
    }
    
    // Add event listener
    void EventDispatcher::addEventListener( EventListener& listener)
    {
        SameTypeListenerMap & typeListeners  = m_listeners[listener.eventType()];
        ListenerList & priorityListeners = typeListeners[listener.priority()];
        
        // avoid repeat operations
        if( std::find(priorityListeners.begin(),priorityListeners.end(),listener) == priorityListeners.end() )
        {
            listener.m_id = ++newListenerID;
            priorityListeners.push_back(listener);
        }
    }
    
    
    void EventDispatcher::removeEventListener(const EventListener& listener)
    {
        auto typeMapIt =  m_listeners.find(listener.eventType());
        if( typeMapIt != m_listeners.end() )
        {
            SameTypeListenerMap & typeListeners = typeMapIt->second;
            auto priorityIt =  typeListeners.find(listener.priority());
            if( priorityIt != typeListeners.end() )
            {
                ListenerList & priorityListeners = priorityIt->second;
                
                auto listenerIt = std::find(priorityListeners.begin(), priorityListeners.end(), listener);
                if( listenerIt != priorityListeners.end()  )
                    priorityListeners.erase(listenerIt);
            }
        }
    }
    
    // Remove all listeners
    void EventDispatcher::removeAllEventListeners()
    {
        m_listeners.clear();
    }
    
    // add child
    void EventDispatcher::addChild(EventDispatcher* child)
    {
        if(child)
        {
            if(child->m_pPrarent)
            {
                if(child->m_pPrarent==this )
                    return;
                else
                {
                    child->retain();
                    child->removeFromParent();
                }
            }
            else
                child->retain();
            //TODO: Lock
            m_childList.push_back(child);
            child->m_pPrarent = this;
            //TODO: unLock
        }
    }
    
    // remove child
    void EventDispatcher::removeChild(EventDispatcher* child)
    {
        if(child)
        {
            auto it = std::find(m_childList.begin(), m_childList.end(), child);
            if( it != m_childList.end() )
            {
                //TODO:lock
                child->m_pPrarent = nullptr;
                m_childList.erase(it);
                child->release();
                //TODO:unlock
            }
        }
    }
    
    void EventDispatcher::removeFromParent()
    {
        if(m_pPrarent)
            m_pPrarent->removeChild(this);
    }
    
    // Remove children by index range
    void EventDispatcher::removeChildren(unsigned startIndex, unsigned endIndex)
    {
        list<EventDispatcher*>::iterator sIndex = m_childList.end();
        list<EventDispatcher*>::iterator eIndex = m_childList.end();
        
        if( endIndex >= startIndex &&  startIndex <= m_childList.size() )
        {
            sIndex = m_childList.begin();
            for( size_t s = 0 ;s!=startIndex;++s )
                ++sIndex;
            
            endIndex = endIndex > m_childList.size() ? m_childList.size() : endIndex;
            eIndex = m_childList.begin();
            for( size_t e = 0 ;e!=startIndex;++e )
                ++eIndex;
        }
        
        for (auto it = sIndex; it!=eIndex && it!=m_childList.end(); ++it)
        {
            //TODO:lock
            (*it)->m_pPrarent = nullptr;
            (*it)->release();
            //TODO:unlock
        }
        //TODO:lock
        m_childList.erase(sIndex, eIndex);
        //TODO:unlock
    }
}