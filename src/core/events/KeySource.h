//
//  KeySource.h
//  Walnut
//
//  Created by Geequlim on 15/8/8.
//

#ifndef Walnut_KeySource_h
#define Walnut_KeySource_h

namespace Walnut
{
    namespace KeySource
    {
        /// Enumeration for keys on keyboards
        enum KeyCode : int
        {
            Unknown = '\0',
            Escape = '\033',
            Return = '\r',
            Tab = '\t',
            Backspace = '\b',
            Insert,
            Delete = '\177',
            Right,
            Left,
            Down,
            Up,
            PageUp,
            PageDown,
            Home,
            End,
            CapsLock,
            ScrollLock,
            NumLock,
            PrintScreen,
            Pause,

            Space = ' ',

            Apostrophe = '\'',
            Comma = ',',
            Hyphen = '-',
            Point = '.',
            Slash = '/',
            Semicolon = ';',
            Equals = '=',
            LeftBracket = '[',
            BackSlash = '\\',
            RightBracket = ']',
            Accent = '`',

            Number0 = '0',
            Number1 = '1',
            Number2 = '2',
            Number3 = '3',
            Number4 = '4',
            Number5 = '5',
            Number6 = '6',
            Number7 = '7',
            Number8 = '8',
            Number9 = '9',

            A = 'a',
            B = 'b',
            C = 'c',
            D = 'd',
            E = 'e',
            F = 'f',
            G = 'g',
            H = 'h',
            I = 'i',
            J = 'j',
            K = 'k',
            L = 'l',
            M = 'm',
            N = 'n',
            O = 'o',
            P = 'p',
            Q = 'q',
            R = 'r',
            S = 's',
            T = 't',
            U = 'u',
            V = 'v',
            W = 'w',
            X = 'x',
            Y = 'y',
            Z = 'z',

            Numpad0,
            Numpad1,
            Numpad2,
            Numpad3,
            Numpad4,
            Numpad5,
            Numpad6,
            Numpad7,
            Numpad8,
            Numpad9,
            NumpadDecimal,
            NumpadDivide,
            NumpadMultiply,
            NumpadSubtract,
            NumpadAdd,
            NumpadEnter,
            NumpadEqual,

            F1,
            F2,
            F3,
            F4,
            F5,
            F6,
            F7,
            F8,
            F9,
            F10,
            F11,
            F12,
            F13,
            F14,
            F15,
            F16,
            F17,
            F18,
            F19,
            F20,
            F21,
            F22,
            F23,
            F24,
            F25,

            LeftShift,
            LeftCtrl,
            LeftAlt,
            LeftSuper,
            RightShift,
            RightCtrl,
            RightAlt,
            RightSuper,
            Menu,

            Star,
            Pound,
            DPadUp,
            DPadDown,
            DPadLeft,
            DPadRight,
            DPadCenter,
            VolumeUp,
            VolumeDown,
            Power,
            Camera,
            Clear,

            Back,
            Mode,
            Mute,
            Call,
            EndCall,

            Search,
            MediaPlayPause,
            MediaStop,
            MediaNext,
            MediaPrevious ,
            MediaRewind,
            MediaRewindFastForward,
        };

    }

    Byte getCharFromKey( int keyCode , bool withShift);

    using KeySource::KeyCode;
}

#endif
