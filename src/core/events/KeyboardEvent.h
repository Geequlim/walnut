//
//  KeyboardEvent.h
//  Walnut
//
//  Created by Geequlim on 15/8/8.
//
//

#ifndef Walnut_KeyboardEvent_h
#define Walnut_KeyboardEvent_h

namespace Walnut
{
    /// Keyboard evnet class of Walnut
    class KeyboardEvent : public Event
    {
        typedef Event super;
    public:
        
        /// Keyboard event types
        enum Action : int
        {
            KeyDown = 0x70000400,///< Key down
            KeyUp,               ///< Key up(release)
            KeyRepeat            ///< Key repeat(hold)
        };
        
        
        /**
         *  @brief Construct a keyboard event
         *  @param action  action type
         *  @param keyCode key code of the event
         *  @param bubbles if the event bullbs
         *  @param addKeys additional pressed keys
         */
        KeyboardEvent(int action, int keyCode ,bool bubbles ,const vector<int>& addKeys = vector<int>() )
            :super(action,bubbles,false)
        {
            m_keyCode = keyCode;
            m_addKeys = addKeys;
        }
        
        /*!
         @brief  Get the key code of the event
         @return The  key code of the event
         */
        inline int keyCode()const{ return m_keyCode;}

        /*!
         @brief  Get the key action type
         @return The key action type
         */
        inline int action()const{ return m_action; }
        
        /*!
         @brief  Get additional pressed keys
         @return Pressed key codes in a vector
         */
        const vector<int>& addKeys()const{ return m_addKeys; }
        
        /*!
         @brief  Check if the KeyCode key is pressed
         @param key The keycode of the checking key
         @return Is the key pressed
         */
        bool withKey(KeyCode key)const
        {
            return m_addKeys.end() != std::find(m_addKeys.begin(), m_addKeys.end(), key);
        }
        
    protected:
        /// The key code of the event
        int m_keyCode;   
        /// The event type       
        int m_action;    
        /// additional pressed keys       
        vector<int> m_addKeys;  
    };
}

#endif
