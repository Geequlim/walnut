﻿//
//  EventListener.h
//  Walnut
//
//  Created by Geequlim on 15/8/8.
//
//

#ifndef Walnut_EventListener_h
#define Walnut_EventListener_h

namespace Walnut
{
    /// Event listeners' callback type
    typedef std::function<void(Event&)> EventCall;
    
    /// Event listener
    class EventListener : public Object
    {
        typedef Object super;
        friend class EventDispatcher;
    public:
        
        /*!
         @brief  Construct an event listener
         @param eventType The event type to listen
         @param handler   The dispatching callback
         @param priority  The dispatching priority
         */
        EventListener(int eventType ,EventCall handler , int priority = 0):super()
        {
            m_eventType = eventType;
            m_priority  = priority;
            m_handler   = handler;
            m_id        = 0U;
        }
        
        /*!
         @brief  Get the priority value
         @return The priority
         */
        inline int priority()const { return m_priority;}
        
        /*!
         @brief  Reset the priority
         @param priority The new dispatching priority
         @note That will invalidate the listener's ID
         */
        inline void setPriority(int priority)
        {
            m_priority = priority;
            invalidate();
        }
        
        /*!
         @brief  Get the event type of the listener
         @return The event type of the listener
         */
        inline int eventType() const { return m_eventType;}
        
        /*!
         @brief  Reset the event type to listen
         @param type The event type to listen
         @note That will invalidate the listener's ID
         */
        inline void setEventType(int type)
        {
            m_eventType = type;
            invalidate();
        }
        
        /*!
         @brief  Get the listener's callback
         @return The callback of this listener
         */
        const EventCall& handler()const{ return m_handler;}
        
        
        /*! 
         @brief Set the listener's callback
         @note That will invalidate the listener's ID
         @param handler The new dispatching callback
         */
        void setHandler(EventCall handler )
        {
            m_handler = handler;
            invalidate();
        }
        
        /*!
         @brief  Check if is equivalent to another listene
         @param anothor The listener compare with
         @return Return @b true if two listeners have same listening types,priorities and ids
         */
        bool operator==(const EventListener& anothor)const
        {
            return m_eventType == anothor.m_eventType &&
            m_priority == anothor.m_priority && m_id == anothor.m_id;
        }
        
        /*!
         @brief  Get the id of the listener
         @return The id of the listener
         */
        unsigned long long id()const{ return m_id;}
    
    protected:
        ///Invalidate the listener's ID
        void invalidate(){ m_id = 0U; }
        
        int m_priority;         ///< Dispatching priority
        int m_eventType;        ///< Listening event type
        EventCall m_handler;    ///< Dispatching callback
        unsigned long long m_id;///< Listener's ID
    };
}
#endif
