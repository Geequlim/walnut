//
//  EventDispatcher.h
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#ifndef __Walnut__EventDispatcher__
#define __Walnut__EventDispatcher__

#include <functional>

#include <list>
#include <map>

namespace Walnut
{
    ///Event dispatcher class of Walnut
    class EventDispatcher : public Object
    {
        typedef Object super;
    public:
        EventDispatcher();
        virtual ~EventDispatcher();
        
        /*!
         @brief  Dispatch event
         @param event The event to dispatch
         @return Return @b false if the event is cancelable and canceled or is stoped ,return @b true else
         */
        virtual bool dispatchEvent(Event& event);
        
        /*!
         @brief  Broadcast event to children dispatchers and this one
         @param event The event to broadcast
         @attention This method will call @e this->dispatchEvent() internal so DO NOT broadcast the events recived
         */
        virtual void broadcastEvent(Event& event);
        
        /*!
         @brief Add event listener into the dispatching  listeners list
         @param listener The listener to add
         @attention  The mothod will change the id of the @e listener
         */
        void addEventListener(EventListener& listener);
        
        /*!
           @brief Remove event listener from dispacting listeners list
           @param listener The event listener to remove
           @note About Removing:
               @li This method will find the parameter listenr in the dispatching listeners list
               @li If find the same listener , remove it frome the dispatching listeners list
               @li Or do noting with the dispatching listeners list
         
           @attention These conditions are required to check if two listeners are the same one:
               @li Listeners has same listening event type
               @li Listeners has same priority
               @li Listeners has same id
         
           @see bool EventListener::operator==(const EventListener& anothor)const
         */
        void removeEventListener(const EventListener& listener);
        
        ///Remove all event listeners from the dispatching listeners list
        void removeAllEventListeners();
        
        /*!
         @brief  Get the number of children
         @return The number of childeren
         */
        inline virtual unsigned childrenCount()const { return m_childList.size(); }
        
    protected:

        /*!
         @brief  Add a child dispatcher
         @param child Child dispatcher
         */
        virtual void addChild(EventDispatcher* child);
        
        /*!
         @brief Remove a child dispatcher
         @param child Child dispatcher
         */
        virtual void removeChild(EventDispatcher* child);
        
        ///Remove from parent dispatcher
        virtual void removeFromParent();
        
        /*! @brief Remove children by index range
            @param startIndex Start remove index
            @param endIndex   End remove index(not contains)
         */
        virtual void removeChildren(unsigned startIndex = 0U , unsigned endIndex = UINT_MAX);
        
        /// A new listener's ID
        static unsigned long long newListenerID;
        
        /// Listeners has same listening event type and priority
        typedef std::list<EventListener> ListenerList;
        
        /// Listeners has same listening event type
        typedef std::map<int, ListenerList> SameTypeListenerMap;
        
        /// The dispatching listeners list(container)
        std::map<int, SameTypeListenerMap> m_listeners;
        
        /// Dispatcing listener for this dispatcher
        const EventListener* m_pDispatchingListener;
        
        /// Children list
        std::list<EventDispatcher*> m_childList;
        
        EventDispatcher *m_pPrarent;///< Parent
    };
}

#endif
