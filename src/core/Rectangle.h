#pragma once

#include <algorithm>

namespace Walnut
{
    /*! @brief The rectangle class */
    struct Rectangle
    {
        Number x;      ///< The x coordinate of the rectangle's origin
        Number y;      ///< The y coordinate of the rectangle's origin
        Number width;  ///< The width of the rectangle
        Number height; ///< The height of the rectangle

        ///Construct a empty rectangle
        Rectangle(){ x = y = width = height = 0; }
        
        /*! 
            @brief Construct a rectangle
         
            @param _x The @e x coordinate of the rectangle's origin
            @param _y The @e y coordinate of the rectangle's origin
            @param _width The @e width of the rectangle
            @param _height The @e height of the rectangle
         */
        Rectangle(Number _x, Number _y , Number _width , Number _height)
        {
            set(_x, _y, _width, _height);
        }
        
        /*! 
            @brief Construct a rectangle with its origin coordinates and its size
         
            @param _pos The rectangle's origin in 2D coordinates
            @param _size A 2D size that means the rectangle's size
         */
        Rectangle(const Point& _pos , const Size& _size){ set(_pos, _size);}
        
        /*!
            @brief Reset the rectangle's values
            @param _x The x coordinate of the rectangle's origin
            @param _y The y coordinate of the rectangle's origin
            @param _width The width of the rectangle
            @param _height The height of the rectangle
            @return The changed rectangle itself
         */
        inline Rectangle& set(Number _x, Number _y , Number _width , Number _height)
        {
            x = _x;
            y = _y;
            width = _width;
            height = _height;
            return *this;
        }
        
        /*!
            @brief Reset the rectangle's values
            @param _pos The rectangle's origin in 2D coordinates
            @param _size A 2D size that means the rectangle's size
            @return The changed rectangle itself
         */
        inline Rectangle& set(const Point& _pos , const Size& _size)
        {
            x      = _pos.x;
            y      = _pos.y;
            width  = _size.width;
            height = _size.height;
            return *this;
        }
        
        /*!
         @brief  Get the rectangle's origin position in 2D coordinates
         @return The position of the rectangle's origin position
         */
        Point position()const { return Point(x,y); }
        
        /*!
         @brief  Set the rectangle's origin position in 2D coordinates
         @param _pos The rectangle's origin position
         @return The changed rectangle itself
         */
        inline Rectangle& setPosition(const Point& _pos)
        {
            x = _pos.x;
            y = _pos.y;
            return *this;
        }
        
        /*!
         @brief  Set the rectangle's origin position in 2D coordinates
         @param _x The @e x coordinate of the position for the origin
         @param _y The @e y coordinate of the position for the origin
         @return The changed rectangle itself
         */
        inline Rectangle& setPosition(Number _x, Number _y)
        {
            return setPosition( Point(_x,_y) );
        }
        
        /*!
         @brief  Get the size of the rectangle
         @return The size of the rectangle
         */
        inline Size size()const { return Size(width,height); }
        
        
        /*!
         @brief  Reset the size of the rectangle
         @param _width  The @e width to set
         @param _height The @e height to set
         @return The changed rectangle itself
         */
        inline Rectangle& setSize(Number _width , Number _height)
        {
            width  = _width;
            height = _height;
            return *this;
        }
        
        /*!
         @brief  Reset the size of the rectangle
         @param _size The @e size to set
         @return The changed rectangle itself
         */
        inline Rectangle& setSize(Size _size)
        {
            return setSize(_size.width, _size.height);
        }

        /*!
         @brief  Get the @e x coordinate of the rectangle's left side
         @return The @e x coordinate of the rectangle's left side
         */
        inline Number left()const { return x; }
        
        /*!
         @brief  Get the @e x coordinate of the rectangle's  right side
         @return The @e x coordinate of the rectangle's  right side
         */
        inline Number right()const { return x+width;}
        
        /*!
         @brief  Get the @e y coordinate of the rectangle's top side
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The @e y coordinate of the rectangle's  top side
         */
        inline Number top( bool Y_down_coordinate = true)const
        {
            return (Y_down_coordinate) ? (y + height) : y ;
        }
        
        /*!
         @brief  Get the @e y coordinate of the rectangle's  bottom side
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The @e y coordinate of the rectangle's  bottom side
         */
        inline Number bottom(bool Y_down_coordinate = true)const
        {
            return (Y_down_coordinate) ? y :(y + height) ;
        }
        
        /*! 
         brief Get the @e top-left corner coordinates of the rectangle
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The position of the @e top-left corner in 2D coordinates
         */
        inline Point topLeft(bool Y_down_coordinate = true )const
        {
            return Point( left() , top(Y_down_coordinate) );
        }
        
        /*!
         @brief  Get the top-right corner coordinates of the rectangle
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The position of the @e top-right corner in 2D coordinates
         */
        inline Point topRight(bool Y_down_coordinate = true )const
        {
            return Point( right() , top(Y_down_coordinate) );
        }

        /*!
         @brief  Get the @e bottom-left corner coordinates of the rectangle
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The position of the @e bottom-left corner in 2D coordinates
         */
        inline Point bottomLeft(bool Y_down_coordinate = true )const
        {
            return Point( left() , bottom(Y_down_coordinate) );
        }
        
        /*!
         @brief  Get the @e bottom-right corner coordinates of the rectangle
         @param Y_down_coordinate The @e Y axis is down-growthed
         @return The position of the @e bottom-right corner in 2D coordinates
         */
        inline Point bottomRight(bool Y_down_coordinate = true )const
        {
            return Point( right() , bottom(Y_down_coordinate) );
        }
        
        /*!
         @brief  Get the center point of the rectangle in 2D coordinate
         @return The center point of the rectangle
         */
        inline Point center()const
        {
            return Point( x + width/2 , y + height/2 );
        }

        /*!
         @brief  Get the diagonal length of the rectangle
         @return The diagonal length of the rectangle
         */
        inline Number diagonalLength() const
        {
            return sqrt((width * width) + (height * height));
        }

        /*!
         @brief  Get the area of the rectangle
         @return The area of the rectangle
         */
        inline Number area() const { return width * height;}
        
        /*!
         @brief  Get the perimeter of the rectangle
         @return The perimeter of the rectangle
         */
        inline Number perimeter() const { return (width + height) * 2;}
        
        /*!
         brief Get the max @e Y coordinate in the rectangle
         @par It means the @e y coordinate of the top side in @b up-growthed Cartesian coordinate systems
         @par It means the @e y coordinate of the bottom side in @b down-growthed Cartesian coordinate systems
         @return The max @e y value of in the rectangle
         */
        inline Number maxY() const { return y + height; }
        
        /*!
         @brief  Get the max @e X coordinate in the rectangle
         @par It means the @e x coordinate of the right side
         @return The max @e x value of in the rectangle
         */
        inline Number maxX() const { return x + width; }
        
        /// Set the rectangle's width and height to 0
        inline void setEmpty() { width = height = 0; }
        
        /*!
         @brief  Check if is the area of the recatangle is 0
         @return If is the area of the recatangle is 0
         */
        inline bool isEmpty() const { return width == 0 || height == 0; }
        
        /*!
         @brief  Check the rectangle is eqlus to another rectangle
         @param _r The rectangle to compare with
         @return Return @b true if all attributes are equled with the rectangle _r
         */
        inline bool operator==(const Rectangle& _r)const
        {
            return  x == _r.x && y == _r.y && width==_r.width && height==_r.height;
        }
        
        /*!
         @brief  Check the rectangle is NOT eqlus to another rectangle
         @param _r The rectangle to compare with
         @return Return @b true if not all attributes are equled with the rectangle _r
         */
        inline bool operator!=(const Rectangle& _r)const { return !operator==(_r);}
        
        /*!
         @brief  Comparing the area of the rectangle with another's
         @param _r The rectangle to compare with
         @return Return @b true if the area of this rectangle is smaller than another's
         */
        inline bool operator<(const Rectangle & _r)const { return area()<_r.area();}
        
        /*!
         @brief  Check if the rectangle contains a 2D poisition
         @param _pos The position to check
         @return Return @b true if the point @e _pos in the rectangle
         */
        inline bool contains(const Point& _pos)const
        {
            bool c1 = _pos.x >= x && _pos.y >= y;
            bool c2 = _pos.x <= right() && _pos.y <= maxY();
            
            bool c3 = (_pos.x <= x) && (_pos.x > (x + width));
            bool c4 = (_pos.y <= y) && (_pos.y > (y + height));
            
            return (c1 && c2 ) || (c3 && c4);
        }
        
        /*!
         @brief  Check if the rectangle contains a 2D poisition
         @param _x The @e x position of the position to check
         @param _y The @e y position of the position to check
         @return Return @b true if the point @e ( @e x ,@e y ) in the rectangle
         */
        inline bool contains(Number _x , Number _y)const
        {
            return contains( Point(_x,_y) );
        }
        
        /*!
         @brief  Check if the rectangle contains another rectangle
         @param another The rectangle to compare with
         @return Return @b true if another rectangle in the rectangle
         */
        inline bool contians(const Rectangle &another) const
        {
            Number r1 = another.x + another.width;
            Number b1 = another.y + another.height;
            Number r2 = x + width;
            Number b2 = y + height;
            
            return (another.x >= x) && (another.x < r2)
            && (another.y >= y) && (another.y < b2)
            && (r1 > x) && (r1 <= r2)
            && (b1 > y) && (b1 <= b2);
        }
        

        /*!
         @brief  Check if the rectangle intersects with another one
         @param another The rectangle to compare with
         @return Return @b true if intersects with another rectangle
         */
        inline bool intersectsRect(const Rectangle &another) const
        {
            if (isEmpty() || another.isEmpty())
                return false;
            
            Number rx = std::max(x, another.x);
            Number ry = std::max(y, another.y);
            Number rw = std::min(x + width, another.x + another.width) - rx;
            Number rh = std::min(y + height, another.y + another.height) - ry;

            return (rw <= 0 || rh <= 0) ? false : true;
        }
        
        /*!
         @brief  Check if the rectangle intersects with a circle
         @param center The center point position of the circle
         @param radius The radius of the circle
         @return Return @b true if intersects with the circle
         */
        bool intersectsCircle(const Point & center, float radius) const
        {
            Point rectCenter( (x + width / 2),(y + height / 2) );
            
            float w = width / 2;
            float h = height / 2;
            float dx = fabs(center.x - rectCenter.x);
            float dy = fabs(center.y - rectCenter.y);
            
            if (dx > (radius + w) || dy > (radius + h))
                return false;
            
            Point circleDistance( fabs(center.x - x - w) , fabs(center.y - y - h) );
            
            if (circleDistance.x <= (w) || circleDistance.y <= (h) )
                return true;
            
            float cornerDistanceSq = powf(circleDistance.x - w, 2) + powf(circleDistance.y - h, 2);
            return (cornerDistanceSq <= (powf(radius, 2)));
        }
        
        /*!
         @brief  Scale the rectangle
         @param _sw How much you want to scale to the horizontal side
         @param _sh How much you want to scale to the vertical side
         @return The changed rectangle itself
         */
        inline Rectangle& scale( Number _sw , Number _sh )
        {
            width  *= _sw;
            height *= _sh;
            return *this;
        }

        /*!
         @brief  Scale the rectangle
         @param _s How much you want to scale to the horizontal and vertical side
         @return The changed rectangle itself
         */
        inline Rectangle& scale( Number _s) { return scale(_s, _s); }
        
        /*!
         @brief  Get the rectangle scaled form this one
         @param _s Scale ratio
         @return A new rectangle scale from this
         */
        inline Rectangle getScaled(Number _s)const
        {
            Rectangle r = *this;
            return r.scale(_s);
        }
        
        /*!
         @brief  Get the rectangle scaled form this one
         @param _sw How much you want to scale to the horizontal side
         @param _sh How much you want to scale to the vertical side
         @return A new rectangle scale from this
         */
        inline Rectangle getScale(Number _sw , Number _sh)const
        {
            Rectangle r = *this;
            return r.scale(_sw, _sh);
        }
        
        /*!
         @brief  Get the rectangle region that clipped by a rectangle
         @param rect The clip rectangle
         @return A new rectangle cliped from this with the rectangle @e rect
         */
        inline Rectangle getClipped(const Rectangle& rect) const
        {
            Rectangle result;
            result.x = std::min(std::max(x, rect.x), rect.right());
            result.width = std::max(std::min(right(), rect.right()), result.x) - result.x;
            
            result.y = std::min(std::max(y, rect.y), rect.maxY());
            result.height = std::max(std::min(maxY(), rect.maxY()), result.y) - result.y;
            return result;
        }
        
        /*!
         @brief  Get the minimal rectangle region that can contains this and another rectangle
         @param rect The rectangle to calculate with
         @return A new rectangle can contains this and another rectangle @e rect
         */
        inline Rectangle getUnion(const Rectangle& rect)const
        {
            Rectangle result;
            result.x      = std::min(x, rect.x);
            result.y      = std::min(y, rect.y);
            result.width  = std::max(right(), rect.right()) - result.x ;
            result.height = std::max(maxY(), rect.maxY()) - result.y ;
            
            return result;
        }
        
        /*!
         @brief  Get the intersection region with another rectangle
         @param another The rectangle to calculate with
         @return A new rectangle intersection of this and another rectangle
         */
        inline Rectangle getIntersection(const Rectangle& another) const
        {
            Rectangle result;
            if (isEmpty() || another.isEmpty())
                return result;
            
            result.x      = std::max(x, another.x);
            result.y      = std::max(y, another.y);
            result.width  = std::min( right(), another.right()) - result.x;
            result.height = std::min( maxY(), another.maxY()  ) - result.y;
            
            return result;
        }
    };
    
    /// The rectangle class
    typedef Rectangle Rect;
}