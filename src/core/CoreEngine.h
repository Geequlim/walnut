//
//  CoreEngine.h
//  Walnut
//
//  Created by Geequlim on 15/8/7.
//
//

#ifndef Walnut_CoreEngine_h
#define Walnut_CoreEngine_h
#include "platform/common/InputSource.h"
#include "platform/common/RenderWindowBase.h"
#include <functional>
#include <list>

namespace Walnut
{
    /*!
     @brief The Walnut Engine
     
         The CoreEngine is the core of Walnut@n
         You can treat it as an application, a window manager or the boss of the event dispatchers
     @see engine
    */
    class CoreEngine : public EventDispatcher
    {
        friend class AndroidNativeWindow;
        typedef EventDispatcher super;
    public:
        
        CoreEngine();
        virtual ~CoreEngine();
        
        CoreEngine(const CoreEngine&) = delete;
        CoreEngine& operator=(const CoreEngine&) = delete;
        
        
        /*! @brief Initialize the engine
            @return Return true if initialized successfully
         */
        virtual bool initialize(void* nativeSrc);
        
        /// Uninitialize the engine
        virtual void uninitialize();
        
        /*! @brief Check if is the engine has been initialized successfully
            @return Return true if initialized successfully
         */
        bool initialized()const{ return m_boInitialized; }
        
        /*! @brief Check is the engine is runing
            @return Return true if it is running
         */
        inline bool running()const{ return m_boRunning; }
        
        /*! @brief Check will the engine be stopped automatically after all windows be closed
            @return Return should the engine be closed when all windows are removed
         */
        inline bool autoStop()const{ return m_boAutoStop;}
        
        /*! @brief Set the engine will be stoped or not after all windows be closed
            @param flag Should the engine be closed when all windows are removed
         */
        inline void setAutoStop(bool flag){ m_boAutoStop = flag; }
        
        /// Start the the engine
        virtual void start();
        
        /// Stop the the engine
        virtual void stop();
        
        /*! @brief Add the window to the engine
            @param window The window to add into engine
         */
        void addWindow(RenderWindowBase* window);
        
        /*! @brief Remove the window from the engine
            @param window The window to remove from the engine
         */
        void removeWindow(RenderWindowBase* window);
        
        /*!
         @brief  Get the writable active window
         @return The active window pointer or return nullptr if there is no window actived
         */
        RenderWindowBase* activeWindow();

        /*!
         @brief  Get the read only active window
         @return The active window pointer or return nullptr if there is no window actived
         */
        const RenderWindowBase* activeWindow()const;

        /*!
         @brief  Get the read only list that contains all the windows added to the engine
         @return The window list
         */
        const std::list<RenderWindowBase*>& windows()const{ return m_windows; }

        /*!
         @brief  Get the writable list that contains all the windows add to the engine
         @return The window list
         */
        std::list<RenderWindowBase*>& windows(){ return m_windows; }
        
        /*!
         @brief  Get the string on the clipboard
         @return The clipboard content as a string
         @note Return Walnut::nullstr if get string content from system clipboard failed
         */
        const string& clipboardString();
        
        /*!
         @brief  Set the clipboard string content
         @param str The string content set onto clipboard
         */
        void setClipboardString(const string& str);
        
        /*!
         @brief Dispatching and handling events
         @param event The event object to handle
         @return Return @b false if the event is cancelable and canceled or is stoped ,return @b true else
         @see EventDispatcher::dispatchEvent
         */
        virtual bool dispatchEvent(Event& event) override;
        
        

        /// Get Native application state
        void* nativeState(){ return m_nativeState; }

    protected:
        /// The main loop of the engine
        virtual void run();
        
        /// The copy of clipboard string
        string m_clipBoard;
        /// Is the engine running
        bool m_boRunning;
        /// allows automatically stop
        bool m_boAutoStop;
        /// Has been initialized successfully
        bool m_boInitialized;
        /// The list contains all windows
        std::list<RenderWindowBase*> m_windows;

        void* m_nativeState;
    };
    
    /*! @brief The Walnut engine singleton
        
        You can treat it as the object for your application,window manager or the boss of the event dispatchers
     */
    extern CoreEngine* engine;
    
    /*! @brief Get the window from the Walnut::engine singleton by native window pointer
        @param w Native window pointer
        @return The window pointer whose host native window equls to @e w
        @note If not found the window @e w in the window list of Walnut::engine return nullptr
     */
    RenderWindowBase * getWindowFromEngine(void* w);
}


#endif
