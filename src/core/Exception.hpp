//
//  Created by Geequlim on 15/8/8.
//
//

#ifndef Walnut_Error_h
#define Walnut_Error_h

#include <exception>
#include <string>
using std::string;

namespace Walnut
{
    /// Enumatroation for exception types , use as error codes
    enum ExceptionType : int
    {
        UNDEFINED_EXCEPTION = 0, ///< Undefined exceptions
        RENDER_EXCEPTION,        ///< Rendering exceptions
        IO_EXCEPTION,            ///< I/O exceptions
        LOGIC_EXCEPTION          ///< Logical errors
    };
    
    /*! 
     @brief Walnut Exception class
     @par An Exception can be treated as an error
     */
    class Exception : public std::runtime_error
    {
        typedef std::runtime_error super;
    public:
        /*!
         @brief  Construct an Exception
         @param msg  Description of the exception
         @param code Error code
         */
        Exception(const string& msg , int code = UNDEFINED_EXCEPTION ) noexcept :super(msg.c_str())
        {
            m_errorCode = code;
        }
        
        /*!
         @brief  Get the description of the exception
         @return The C-styled string of the description of the exception
         */
        inline const char* what()const  noexcept { return super::what(); }
        
        /*!
         @brief  Get the error code of the exception
         @return The key code of the exception
         */
        inline int code()const  noexcept { return m_errorCode; }
        
    protected:
        int m_errorCode;///< Error code
    };
}

#endif
