//
//  Created by Geequlim on 15/8/7.
//
//

#include "core.h"
#include "platform/platform.h"
#include <iostream>
#include "graphic/graphic.h"

namespace Walnut
{
    CoreEngine::CoreEngine():super()
    {
        m_boRunning     = false;
        m_boAutoStop    = true;
        m_boInitialized = false;
        m_nativeState   = nullptr;
    }

    CoreEngine::~CoreEngine()
    {
        for ( auto w :m_windows )
        {
            if(w) w->release();
        }
        m_windows.clear();
    }

    // Initialize the engine
    bool CoreEngine::initialize(void* nativeSrc)
    {
        m_nativeState = nativeSrc;
        m_boInitialized = platformInitialize(nativeSrc);
        return m_boInitialized;
    }

    // Uninitialize the engine
    void CoreEngine::uninitialize()
    {
        m_boRunning = false;
        for(auto w :m_windows)
        {
            if(w) w->release();
        }
        m_windows.clear();
        m_boInitialized = false;
        platformUnnitialize(m_nativeState);
    }

    // Start the main loop of the application
    void CoreEngine::start()
    {
        if(!m_boRunning)
        {
            if( m_windows.size() )
            {
                // print informations about the version of OpenGL 
                printf ("OpenGL version: %s\n", glGetString (GL_VERSION));
                printf ("Renderer: %s\n", glGetString (GL_RENDERER));
            }
            m_boRunning = true;
            run();
        }
    }

    // Stop the main loop of the application
    void CoreEngine::stop()
    {
        #if __DESKTOP__
            m_boRunning = false;
        #elif defined __ANDROID__
            stopActivity();
        #endif
    }

    // The main loop of the engine
    void CoreEngine::run()
    {
        while(m_boRunning)
        {
            pollPlatformEvents();
            for (auto w : m_windows )
            {
                w->render();
            }
            // stop the engine if necessary
            if( m_boAutoStop && !m_windows.size() )
            {
                stop();
            }
        }
    }

    // Add the window to the application
    void CoreEngine::addWindow(RenderWindowBase* window)
    {
        if(window)
        {
            if( std::find(m_windows.begin(),m_windows.end(),window) == m_windows.end() )
            {
                if( !window->hostWindow()) window->initialize();
                //TODO:Lock
                super::addChild(window);
                m_windows.push_back(window);
                //TODO:unlock
            }
        }
    }

    // Remove the window from the application
    void CoreEngine::removeWindow(RenderWindowBase* window)
    {
        if(window)
        {
            auto foundIt = std::find(m_windows.begin(),m_windows.end(),window);
            if( foundIt != m_windows.end() )
            {
                //TODO:Lock
                m_windows.erase(foundIt);
                super::removeChild(window);
                //TODO:unlock
            }
        }
    }

    //Get the active window
    RenderWindowBase* CoreEngine::activeWindow()
    {
        RenderWindowBase* window = nullptr;
        for( auto w : m_windows )
        {
            if( w->actived() )
            {
                window = w;
                break;
            }
        }
        return window;
    }

    //Get the active window
    const RenderWindowBase* CoreEngine::activeWindow()const
    {
        const RenderWindowBase* window = nullptr;
        for( const auto w : m_windows )
        {
            if( w->actived() )
            {
                window = w;
                break;
            }
        }
        return window;
    }

    //Get the string on the clipboard
    const string& CoreEngine::clipboardString()
    {
        string temp  = platformGetClipboardString();
        if(temp!="")
            m_clipBoard = temp;
        return m_clipBoard;
    }

    //Set the clipboard string to str
    void CoreEngine::setClipboardString(const string& str)
    {
        m_clipBoard = str;
        platformSetClipboardString(m_clipBoard);
    }

    // Dispatching and handling events
    bool CoreEngine::dispatchEvent(Event& event)
    {
        switch (event.type())
        {
            case NormalEventType::CLOSED:
            {
                auto targetWnd = dynamic_cast<RenderWindowBase*>(event.target());
                if( targetWnd )
                {
                    removeWindow(targetWnd);
                }
            }
                break;
            default:
                break;
        }
        return super::dispatchEvent(event);
    }

    //Get the window from the Walnut engine singleton by native window pointer
    RenderWindowBase * getWindowFromEngine(void* w)
    {
        RenderWindowBase * window = nullptr;
        auto & windows =  engine->windows();
        for (auto it = windows.begin(); it!=windows.end(); ++it)
        {
            if( (*it)->hostWindow() == w )
            {
                window = *it;
                break;
            }
        }
        return window;
    };
}
