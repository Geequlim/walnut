#ifndef __Walnut_Object_H__
#define __Walnut_Object_H__

namespace Walnut
{
    class Object
    {
    public:
        Object()
        {
            m_refCount =1;
        };
        
        virtual ~Object(){}
        
        int release()
        {
            --m_refCount;
            if( m_refCount==0 )
                delete this;
            return m_refCount;
        }
        
        int retain()
        {
            ++m_refCount;
            return m_refCount;
        }
    protected:
        unsigned m_refCount;
    };
}

#endif

