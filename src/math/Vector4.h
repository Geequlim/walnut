#ifndef __Walnut_Vector4_h__
#define __Walnut_Vector4_h__

namespace Walnut
{
    /// 4D Vector class 
    template<typename T>
    struct Vector4 : public VectorBase<T, 4U>
    {
        /// The value type of the elements 
        typedef T value_type;
        
        /// Super type 
        typedef VectorBase<value_type, 4U> super;
        
        value_type & x = this->mem[0]; ///< The first element
        value_type & y = this->mem[1]; ///< The second element
        value_type & z = this->mem[2]; ///< The third element
        value_type & w = this->mem[3]; ///< The fourth element
        
        /// Default constructor 
        Vector4():super(){ }
        
        /// All elements assigned to _v 
        Vector4(const value_type _v):super(_v){}
        
        /// Construct with x,y,z and w value 
        Vector4(value_type _x,value_type _y,value_type _z,value_type _w)
        {
            set(_x, _y, _z , _w);
        }
        
        /// Copy constructor 
        Vector4(const Vector4& _v):super(_v) {}
        
        /** @brief Construct with another kind of vector
         *
         *  @param _v The vector to copy data.If the number of the elements of _v is samller, the rest elements of this vector are initialized as the default
         */
        template<typename U , unsigned _elements>
        Vector4(const VectorBase<U,_elements>& _v):super(_v){}
        
        /// Construct with the same length matrix 
        Vector4(const typename super::match_matrix_type& _m):super(_m){}
        
        /** @brief Construct with an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will be initialized as default.
         */
        Vector4(const std::initializer_list<value_type> & _a ):super(_a){}
        
        /// Assigning to another vector 
        inline Vector4& operator=(const Vector4 & _v)
        {
            return set(_v);
        }
        
        /// Assigning with another kind of vector 
        template<typename U , unsigned _elements>
        inline Vector4& operator=(const VectorBase<U,_elements>& _v)
        {
            return set(_v);
        }
        
        /// Assigning with a matrix that has the same number of elements 
        Vector4& operator=(const typename super::match_matrix_type& _m)
        {
            return set(_m);
        }
        
        /** @brief Assinging elements values to an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will not be changed
         */
        Vector4& operator=(const std::initializer_list<value_type> & _a )
        {
            return set(_a);
        }
        
        /// Convert to an array 
        inline operator const value_type*()const{ return this->mem;}
        
        /// Convert to a writable array 
        inline operator value_type*(){ return this->mem; }
        
        /// Set elements value by _x,_y,_z and _w 
        Vector4& set(value_type _x,value_type _y,value_type _z,value_type _w)
        {
            x = _x;
            y = _y;
            z = _z;
            w = _w;
            return *this;
        }
        
        /// Set elements value by super vector type object 
        Vector4& set(const super & _sv)
        {
            super::set(_sv);
            return *this;
        }
    };
}

#endif
