#ifndef __Walnut_MathUtil_h_
#define __Walnut_MathUtil_h_

namespace Walnut
{
    /// The PI constant definition
    constexpr long double PI = 3.14159265359L;
    
    /**
     *  @brief Convert degree to radians
     *
     *  @param _degree The degree to convert
     *
     *  @return Return the radians
     */
    t_math_value degree2Radians(const t_math_value _degree);
    
    /**
     *  @brief Conver radians to degree
     *
     *  @param _radians The radians to convert
     *
     *  @return Return the degree
     */
    t_math_value radians2Degree(const t_math_value _radians);
    
    /**
     * @brief Get a number in range @b [a,b]
     * @return The value between @e a and @e b
     * @li if @e a <= @e x <= @e b returns @e x
     * @li if @e x < @e a returns @e a
     * @li if @e x > @e b returns @e b
     */
    template<typename T>
    inline T clamp(T x, T a, T b)
    {
        return x < a ? a : (x > b ? b : x);
    }
}
#endif
