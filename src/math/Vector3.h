#ifndef __Walnut_Vector3_h__
#define __Walnut_Vector3_h__

namespace Walnut
{
    /// 3D Vector class
    template<typename T>
    struct Vector3 : public VectorBase<T, 3U>
    {
        /// The value type of the elements 
        typedef T value_type;
        
        /// Super type 
        typedef VectorBase<value_type, 3U> super;
        
        value_type & x = this->mem[0]; ///< The first element
        value_type & y = this->mem[1];///< The second element
        value_type & z = this->mem[2];///< The third element
        
        /// Default constructor 
        Vector3():super(){}
        
        /// All elements assigned to _v 
        Vector3(value_type _v):super(_v){}
        
        /// Construct with x,y and z value 
        Vector3(value_type _x,value_type _y,value_type _z) { set(_x, _y,_z);}
        
        /// Copy constructor 
        Vector3(const Vector3& _v):super(_v){}
        
        /** @brief Construct with another kind of vector
         *
         *  @param _v The vector to copy data.If the number of the elements of _v is samller, the rest elements of this vector are initialized as the default
         */
        template<typename U , unsigned _elements>
        Vector3(const VectorBase<U,_elements>& _v):super(_v){}
        
        /// Construct with the same length matrix 
        Vector3(const typename super::match_matrix_type& _m):super(_m){}
        
        /** @brief Construct with an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will be initialized as default.
         */
        Vector3(const std::initializer_list<value_type> & _a ):super(_a){}
        
        /// Assigning to another vector 
        inline Vector3& operator=(const Vector3 & _v)
        {
            return set(_v);
        }
        
        /// Assigning with another kind of vector 
        template<typename U , unsigned _elements>
        inline Vector3& operator=(const VectorBase<U,_elements>& _v)
        {
            return set(_v);
        }
        
        /// Assigning with a matrix that has the same number of elements 
        inline Vector3& operator=(const typename super::match_matrix_type& _m)
        {
            return set(_m);
        }
        
        /** @brief Assinging elements values to an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will not be changed
         */
        inline Vector3& operator=(const std::initializer_list<value_type> & _a )
        {
            return set(_a);
        }
        
        /// Convert to an array 
        inline operator const value_type*()const{ return this->mem;}
        
        /// Convert to a writable array 
        inline operator value_type*(){ return this->mem; }
        
        /// Set elements value by _x,_yand _z 
        inline Vector3& set(value_type _x,value_type _y ,value_type _z)
        {
            x = _x;
            y = _y;
            z = _z;
            return *this;
        }
        
        /// Set elements value by super vector type object 
        inline Vector3& set(const super & _sv)
        {
            super::set(_sv);
            return *this;
        }
    };
}

#endif
