#ifndef __Walnut_Matrix3x2_h__
#define __Walnut_Matrix3x2_h__

namespace Walnut
{
    /// 3x2 matrix class
    template<typename T>
    struct Matrix3x2 : public MatrixBase<T, 3, 2>
    {
        typedef MatrixBase<T,3,2> super;
        
        ///Value type of elements
        typedef T value_type;

        value_type & a =  this->mem2D[0][0];
        value_type & b =  this->mem2D[0][1];
        value_type & c =  this->mem2D[1][0];
        value_type & d =  this->mem2D[1][1];
        value_type & tx = this->mem2D[2][0];
        value_type & ty = this->mem2D[2][1];

        ///Default constructor,all elements on matrix[i][i] were set to 1
        Matrix3x2() = default;

        ///Construct a matrix with a 2D array
        Matrix3x2(const value_type _m[super::row][super::columns] ):super(_m){}

        ///Construct a matrix with an array
        Matrix3x2(const value_type _m[super::element_size] ):super(_m){}

        ///Copy constructor 
        Matrix3x2(const Matrix3x2& _m):super(_m){}

        /// Construct with super 
        Matrix3x2(const super & _sm): super::set(_sm){}

        /// Construct with an initializer list 
        Matrix3x2(const std::initializer_list<value_type> & _elements)
        {
            set(_elements);
        }

        /// Construct with elements 
        Matrix3x2(value_type _a ,value_type _b ,value_type _c,
                  value_type _d ,value_type _tx,value_type _ty )
        {
            set( _a , _b , _c , _d , _tx , _ty);
        }

        /// Assigning elements value with a 2D array 
        Matrix3x2& operator=( const value_type _m[super::row][super::columns] )
        {
            return super::set(_m);
        }

        /// Assigning elements value with an array 
        Matrix3x2& operator=(const value_type _m[super::element_size] )
        {
            return super::set(_m);
        }

        /// Assigning elements value with another matrix 
        Matrix3x2& operator=(const Matrix3x2& _m){return super::set(_m);}

        /// Assigning elements value with a matrix 
        inline Matrix3x2& operator=(const super & _sm)
        {
            super::set(_sm);
            return *this;
        }
        
        /// Assigning elements value with a initializer list 
        Matrix3x2& operator=(const std::initializer_list<value_type> & _elements)
        {
            return set(_elements);
        }

        /// Set elements value with initializer list 
        Matrix3x2& set(const std::initializer_list<value_type>& _elements)
        {
            for(size_t i=0 ;i!=super::element_size && i!= _elements.size() ; ++i )
                this->mem[i] = _elements;
            return *this;
        }

        /// Set elements value 
        Matrix3x2& set(value_type _a,value_type _b,value_type _c ,
                       value_type _d,value_type _tx,value_type _ty)
        {
            this->mem[0][0] = _a; this->mem[0][1] = _b;
            this->mem[1][0] = _c; this->mem[1][1] = _d;
            this->mem[2][0] = _tx; this->mem[2][1] = _ty;
            return *this;
        }


        /**
         *  @brief Check if is equls to another matrix
         *  @param _m The matrix to compare
         *  @return  Return true if all elements with same index in two matrices take the same value
         */
        inline bool operator==(const Matrix3x2 & _m) const
        {
            return super::operator==(_m);
        }


        /// Convert to an array 
        inline operator const value_type *()const{ return this->mem;}

        /// Convert to a writable array 
        inline operator value_type *(){ return this->mem;}

        /**
         *  @brief Multiply(transform) a 3D vector
         *  @param _vec The vector to multiply(transform)
         *  @return Return the result vector(2D Point)
         */
        inline Point operator *(const Vector & _vec)
        {
            return _vec.toMatrix() * (*this);
        }

        /**
         *  @brief Multiply(transform) a 2D vector
         *  @param _vec The vector to multiply(transform)
         *  @return Return the result vector(2D Point)
         */
        inline Point operator *(const Point & _vec)
        {
            Vector v({_vec.x , _vec.y , 1});
            return operator*(v);
        }

        /**
         *  @brief Create a scale matrix
         *  @param _sx scalar in x axis
         *  @param _sy scalar in y axis
         *  @return Return the scale matrix
         */
        static Matrix3x2 scaleMatrix(value_type _sx ,value_type _sy )
        {
            Matrix3x2 smat;
            smat.a = _sx;
            smat.d = _sy;
            return smat;
        }

        /**
         *  @brief Create a scale matrix
         *  @param _vec scalars
         *  @return Return the scale matrix
         */
        static Matrix3x2 scaleMatrix(const Point& _vec)
        {
            return scaleMatrix(_vec.x , _vec.y);
        }


        /**
         *  @brief Scale the matrix
         *  @param _sx scalar in x axis
         *  @param _sy scalar in y axis
         *  @return Return the scaled matrix
         */
        inline Matrix3x2& scale(value_type _sx ,value_type _sy)
        {
            a *= _sx;
            d *= _sy;
            return *this;
        }

        /**
         *  @brief Scale the matrix
         *  @param _vec scalars
         *  @return Return the scaled matrix
         */
        inline Matrix3x2& scale(const Point & _vec )
        {
            scale( _vec.x , _vec.y );
            return *this;
        }

        /**
         *  @brief Create a translation matrix
         *  @param _tx translation in x axis
         *  @param _ty translation in y axis
         *  @return Return the translation matrix
         */
        static Matrix3x2 translateMatrix(value_type _tx ,
                                         value_type _ty)
        {
            Matrix3x2 t_mat;
            t_mat.tx  = _tx;
            t_mat.ty  = _ty;
            return t_mat;
        }

        /**
         *  @brief Create a translation matrix
         *  @param _vec translation vector
         *  @return Return the translation matrix
         */
        inline Matrix3x2& translate(const Point & _vec )
        {
            tx += _vec.x;
            ty += _vec.y;
            return *this;
        }

        /**
         *  @brief Translate the matrix
         *  @param _tx translation in x axis
         *  @param _ty translation in y axis
         *  @return Return the translated matrix
         */
        inline Matrix3x2& translate(value_type _tx , value_type _ty )
        {
            tx += _tx;
            ty += _ty;
            return *this;
        }


        /**
         *  @brief Translate the matrix
         *  @param _vec translation vector
         *  @return Return the translated matrix
         */
        static Matrix3x2 translateMatrix(const Point& _vec)
        {
            return translateMatrix(_vec.x, _vec.y );
        }

        /**
         *  @brief Create rotate matrix
         *  @param _r   The angle to rotate in radians
         *  @return Return the rotate matrix
         */
        inline Matrix3x2& rotate( t_math_value _r )
        {
            value_type u = cos(_r);
            value_type v = sin(_r);
            value_type result_a = (u * a) - (v * b);
            value_type result_b = (v * a) + (u * b);
            value_type result_c = (u * c) - (v * d);
            value_type result_d = (v * c) + (u * d);
            value_type result_tx = (u * tx) - (v * ty);
            value_type result_ty = (v * tx) + (u * ty);
            a = result_a;
            b = result_b;
            c = result_c;
            d = result_d;
            tx = result_tx;
            ty = result_ty;
            return *this;
        }

        /**
         *  @brief Rotate matrix
         *  @param _r  The angle to rotate in radians
         *  @return Return the rotated matrix
         */
        static Matrix3x2 rotateMatrix(value_type _r)
        {
            Matrix3x2 rmat;
            return rmat.rotate(_r);
        }
    };
}

#endif
