#ifndef __Walnut_Vector2_h__
#define __Walnut_Vector2_h__
namespace Walnut
{
    /// 2D Vector class
    template<typename T>
    struct Vector2 : public VectorBase<T, 2U>
    {
        /// The value type of the elements 
        typedef T value_type;
        
        /// Super type 
        typedef VectorBase<value_type, 2U> super;
        
        value_type & x = this->mem[0]; ///< The first element
        value_type & y = this->mem[1]; ///< The second element
        
        /// Default constructor 
        Vector2():super(){}
        
        /// All elements assigned to _v 
        Vector2(value_type _v):super(_v){}
        
        /// Construct with x and y value 
        Vector2(value_type _x,value_type _y) { set(_x, _y); }
        
        /// Copy constructor 
        Vector2(const Vector2& _v):super(_v){}
        
        /** @brief Construct with another kind of vector
         *
         *  @param _v The vector to copy data.If the number of the elements of _v is samller, the rest elements of this vector are initialized as the default
         */
        template<typename U , unsigned _elements>
        Vector2(const VectorBase<U,_elements>& _v):super(_v){}
        
        /// Construct with the same length matrix 
        Vector2(const typename super::match_matrix_type& _m):super(_m){}
        
        /** @brief Construct with an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will be initialized as default.
         */
        Vector2(const std::initializer_list<value_type> & _a ):super(_a){}

        /// Assigning to another vector 
        inline Vector2& operator=(const Vector2 & _v){ return set(_v);}
        
        /// Assigning with another kind of vector 
        template<typename U , unsigned _elements>
        inline Vector2& operator=(const VectorBase<U,_elements>& _v)
        {
            return set(_v);
        }
        
        /// Assigning with a matrix that has the same number of elements 
        inline Vector2& operator=(const typename super::match_matrix_type& _m)
        {
            return set(_m);
        }
        
        /** @brief Assinging elements values to an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will not be changed
         */
        inline Vector2& operator=(const std::initializer_list<value_type> & _a )
        {
            return set(_a);
        }
        
        /// Convert to an array 
        inline operator const value_type*()const{ return this->mem;}
        
        /// Convert to a writable array 
        inline operator value_type*(){return this->mem;}
        
        /// Set elements value by _x and _y 
        Vector2& set(value_type _x,value_type _y)
        {
            x = _x;
            y = _y;
            return *this;
        }
        
        /// Set elements value by super vector type object 
        Vector2& set(const super & _sv)
        {
            super::set(_sv);
            return *this;
        }
    };
}
#endif
