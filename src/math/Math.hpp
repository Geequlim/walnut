#ifndef __Walnut__Math_h__
#define __Walnut__Math_h__

#include "Macros.h"
#include <cassert>
#include <cmath>

namespace Walnut
{
    ///The data type for mathematics
    typedef float t_math_value;
}

#include "MathUtil.h"

namespace Walnut
{
    ///The data type for vectors
    typedef t_math_value t_vector_value;
    ////The data type for matices
    typedef  t_math_value t_matrix_value;
}
#include "MatrixBase.h"
#include "VectorBase.h"
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

namespace Walnut
{
    ///2D Vector or 2D Position
    typedef Vector2<t_vector_value> Point;
    ///2D Vector or 2D Position
    typedef Point Vec2;
    ///3D Vector or 3D Position
    typedef Vector3<t_vector_value> Vector;
    ///3D Vector or 3D Position
    typedef Vector Vec3;
    ///4D Vector or 4D Position
    typedef Vector4<t_vector_value> Vec4;
    
    extern const Point zeroPoint;  ///< (0,0) 2D position
    extern const Vector zeroVector; ///< (0,0,0) 3D position
}

#include "Matrix4x4.h"
#include "Matrix3x2.h"
namespace Walnut
{
    ///4x4 Matrix 
    typedef Matrix4x4<t_matrix_value> Matrix4;
    
    ///3x2 Matrix
    typedef Matrix3x2<t_matrix_value> Matrix32;
    
    ///4x4 Matrix
    typedef Matrix4 Matrix;
}


#endif
