#include "Math.hpp"

namespace Walnut
{
    //Convert degree to radians
    t_math_value degree2Radians(const t_math_value _degree)
    {
        return _degree * ( PI / (t_math_value)180);
    }
    
    //Conver radians to degree
    t_math_value radians2Degree(const t_math_value _radians)
    {
        return _radians * ( ( (t_math_value)180 )/ PI) ;
    }
}
