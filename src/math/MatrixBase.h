#ifndef __Walnut_MatrixBase_h__
#define __Walnut_MatrixBase_h__

#include <ostream>
#include <cstring>
namespace Walnut
{
    /// The Matrix class defines a lot of elements and basic methods
    template <typename T , unsigned _row , unsigned _columns>
    struct MatrixBase
    {
        /// Value type of the matrix elements
        typedef T value_type;

        /// The number of rows for the matrix
        static constexpr unsigned row = _row;

        /// The number of columns for the matrix 
        static constexpr unsigned columns = _columns;

        /// The number of elements for the matrix 
        static constexpr unsigned element_size = row*columns;

        union
        {
            value_type mem[element_size]; ///< The member elements in array
            value_type mem2D[row][columns];///< The member elements in 2D array
        };

        /// All elements at matrix[i][i] are set to 1 , others are set to 0 
        MatrixBase(){ identity(); }

        /// Construct a matrix with a 2D array 
        MatrixBase( const value_type _m[row][columns] ) { set(_m); }

        /// Construct a matrix with an array 
        MatrixBase( const value_type _m[element_size] ) { set(_m); }

        /// Copy constructor 
        MatrixBase( const MatrixBase& _m){ set(_m.mem); }

        /// Construct a matrix with an initializer list 
        MatrixBase(const std::initializer_list<value_type>& _il){ set(_il);}

        /// Assigning elements values from a 2D array 
        inline MatrixBase& operator=( const value_type _m[row][columns] )
        {
            return set(_m);
        }

        /// Assigning elements values from an array 
        inline MatrixBase& operator=(const value_type _m[element_size] )
        {
            return set(_m);
        }

        /// Assigning elements values from another matrix 
        inline MatrixBase& operator=(const MatrixBase& _m) { return set(_m); }

        /// Assigning elements values from an initializer list 
        inline MatrixBase& operator=(const std::initializer_list<value_type>& _il)
        {
            return set(_il);
        }

        /**
         *  @brief Comparing if is equlas to another matrix
         *
         *  @param _m Matrix to compare with
         *
         *  @return Return true if all elements with same index in two matrices take the same value
         */
        inline bool operator==(const MatrixBase & _m) const
        {
            bool equls = (row == _m.row) && (columns == _m.columns);
            if(equls)
            {
                for( size_t i=0; i!=element_size ; ++i)
                {
                    equls = mem[i] == _m.mem[i];
                    if( !equls )
                        break;
                }
            }
            return equls;
        }


        /// Convert to an array 
        inline operator const value_type *()const{ return mem;}

        /// Convert to a writable array 
        inline operator value_type *(){ return mem;}

        /// Set to identity matrix. All elements at matrix[i][i] are set to 1 , others are set to 0
        inline MatrixBase& identity()
        {
            for(size_t curRow = 0 ; curRow != row ; ++curRow )
            {
                for(size_t curCol = 0 ; curCol != columns ; ++curCol )
                {
                    if( curRow == curCol )
                        mem2D[curRow][curCol] = 1;
                    else
                        mem2D[curRow][curCol] = 0;
                }
            }
            return *this;
        }

        /// Set elements value with an array 
        inline MatrixBase& set( const value_type _m[element_size] )
        {
            memcpy( mem, _m, sizeof(value_type) * element_size );
            return *this;
        }

        /// Set elements value with a 2D array 
        inline MatrixBase& set(const value_type _m[row][columns] )
        {
            for(size_t curRow = 0 ; curRow != row ; ++curRow )
                memcpy(mem2D[curRow],_m[curRow] , sizeof(value_type)*columns );
            return *this;
        }

        /// Set elements value with an initializer list 
        inline MatrixBase& set(const std::initializer_list<value_type>& _il)
        {
            for(size_t i=0 ; i!=element_size && i!=_il.size() ; ++i )
                mem[i] =  *(_il.begin()+i);
            return *this;
        }

        /// Set elements value with another matrix 
        inline MatrixBase& set(const MatrixBase& _m){ return set(_m.mem); }

        /**
         *  @brief All elements add by a scalar
         *
         *  @param _s The scalar to add
         */
        inline MatrixBase& operator+=(value_type _s)
        {
            for( value_type & e : mem )
                e += _s;
            return *this;
        }

        /**
         *  @brief Each element add by a scalar
         *
         *  @param _s The scalar to add
         */
        inline MatrixBase operator+(value_type _s)const
        {
            MatrixBase mat(mem);
            return mat += _s;
        }

        /**
         *  @brief Each element add by the same index element of another matrix
         *
         *  @param _m The matrix to add
         */
        inline MatrixBase& operator+=(const MatrixBase& _m )
        {
            for(size_t i=0; i!=element_size ; ++i)
                mem[i] += _m.mem[i];
            return *this;
        }

        /**
         *  @brief Each element add by the same index element of another matrix
         *
         *  @param _m The matrix to add
         */
        inline MatrixBase operator+(const MatrixBase & _m)const
        {
            MatrixBase mat(mem);
            return mat += _m;
        }

        /**
         *  @brief Each element subtract by a scalar
         *
         *  @param _s The scalar to subtract
         */
        inline MatrixBase& operator-=(value_type _s){ return operator+=(-_s); }

        /**
         *  @brief Each element subtract by a scalar
         *
         *  @param _s The scalar to subtract
         */
        inline MatrixBase operator-(value_type _s)const{ return operator+(-_s);}

        /**
         *  @brief Each element subtract by the same index element of another matrix
         *
         *  @param _m The matix to subtract
         */
        inline MatrixBase& operator-=(const MatrixBase & _m){ return operator+=(-_m);}

        /**
         *  @brief Each element subtract by the same index element of another matrix
         *
         *  @param _m The matix to subtract
         */
        inline MatrixBase operator-(const MatrixBase & _m)const { return operator+(-_m); }


        /// Get negative of the matrix 
        inline MatrixBase operator-()const
        {
            MatrixBase mat(*this);
            for(auto & m : mat.mem)
                m = -m;
            return mat;
        }

        /**
         *  @brief Each element multipy by a scalar
         *
         *  @param _s The scalar to multipy
         */
        inline MatrixBase& operator*=(value_type _s)
        {
            for( value_type & e : mem )
                e *= _s;
            return *this;
        }

        /**
         *  @brief Each element multipy by a scalar
         *
         *  @param _s The scalar to multipy
         */
        inline MatrixBase operator*(value_type _s)const
        {
            MatrixBase mat(mem);
            mat *= _s;
            return mat;
        }

        /**
         *  @brief Each element divide by a scalar
         *
         *  @param _s The scalar to multipy
         */
        inline MatrixBase& operator/=(value_type _s)
        {
            return operator*=( ((value_type)1)/_s );
        }

        /**
         *  @brief Each element divide by a scalar
         *
         *  @param _s The scalar to multipy
         */
        inline MatrixBase operator/(value_type _s)const
        {
            return operator*( ((value_type)1)/_s );
        }


        /**
         *  @brief Get a matrix by multiplied with another matrix
         *
         *  @param _m The matrix to multiply，its the number of row must be the same as the number of the columns
         *
         *  @return Returns the matrix multiplied by another matrix @e _m
         */
        template<typename U ,const unsigned _columns2>
        MatrixBase<value_type,row,_columns2>
        operator*(const MatrixBase<U,columns,_columns2>& _m)const
        {
            typedef MatrixBase<T,row,_columns2> ResultMatrixBase;
            ResultMatrixBase mat;
            for( size_t curRow =0 ;curRow!=ResultMatrixBase::row ; ++curRow )
            {
                for( size_t curCol = 0 ; curCol!= ResultMatrixBase::columns ; ++curCol)
                {
                    value_type curEleValue = 0.0f;
                    for( size_t curCursor = 0; curCursor!= columns ; ++curCursor)
                    {
                        value_type  pro1 = 0;
                        if( curCursor < columns )
                            pro1 = mem2D[curRow][curCursor];
                        else if(curRow == curCursor )
                            pro1 = 1;
                        curEleValue +=  pro1 * _m.mem2D[curCursor][curCol];
                    }
                    mat.mem2D[curRow][curCol] = curEleValue;
                }
            }
            return mat;
        }

        /**
         *  @brief Set to the matrix multiplied by another matrix
         *
         *  @param _m The matrix to multiply，its the number of row must be the same as the number of the columns
         *
         *  @return Returns the matrix multiplied
         */
        MatrixBase& operator*=(const MatrixBase & _m)
        {
            assert( row == columns );
            return set( operator*(_m) );
        }

        /// Get the transpose matrix 
        inline MatrixBase<value_type, columns, row > getTranspose()const
        {
            MatrixBase<value_type, columns, row > mat;
            for( size_t curRow =0 ;curRow!=mat.row ; ++curRow )
                for( size_t curCol = 0 ; curCol!=mat.columns ; ++curCol)
                {
                    mat.mem2D[curRow][curCol] = mem2D[curCol][curRow];
                }
            return mat;
        }

        /**
         *  @brief Set to the matrix that transposed from this matrix
         *
         *  @attention This method can be called with square matrices only
         */
        inline MatrixBase& transpose()
        {
            assert( row == columns );
            value_type temp[row][columns];
            for( size_t curRow =0 ;curRow!=row ; ++curRow )
                for( size_t curCol = 0 ; curCol!=columns ; ++curCol)
                {
                    temp[curRow][curCol] = mem2D[curCol][curRow];
                }
            set(temp);
            return *this;
        }

    };

    /**
     *  @brief Output elements data of matrix to ostream
     *
     *  @param os   The output stream
     *  @param _mat The data source matrix
     */
    template <typename T , const unsigned row , const unsigned columns>
    std::ostream& operator << (std::ostream& os , const MatrixBase<T, row, columns>& _mat)
    {
        typedef MatrixBase<T,row,columns> MatrixBaseType;

        for(size_t curRow = 0 ; curRow != MatrixBaseType::row ; ++curRow )
        {
            for(size_t curCol = 0 ; curCol != MatrixBaseType::columns ; ++curCol )
            {
                os<<_mat.mem2D[curRow][curCol];
                if(curCol!= MatrixBaseType::columns-1)
                    os<<',';
            }
            os<<std::endl;
        }
        return os;
    }
    
    /*!
     @brief Get string from a matrix
     @param _mat  The matrix to string
     @return The matrix string
     */
    template <typename T , const unsigned row , const unsigned columns>
    string to_string(const MatrixBase<T, row, columns>& _mat)
    {
        stringstream mss;
        mss << _mat;
        return mss.str();
    }
}
#endif
