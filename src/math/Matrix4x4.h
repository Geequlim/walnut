﻿#ifndef __Walnut_Matrix4x4_h__
#define __Walnut_Matrix4x4_h__
#include "VectorBase.h"

namespace Walnut
{
    /// 4x4 Matrix class
    template<typename T>
    struct Matrix4x4 : public MatrixBase<T,4,4>
    {
        typedef MatrixBase<T,4,4> super;

        /// Value type of the matrix elements 
        typedef T value_type;

        /// The vector3 type define 
        typedef Vector3<value_type> vec3;
        
        /// The vector4 type define 
        typedef Vector4<value_type> vec4;

        /// Default constructor 
        Matrix4x4() = default;

        /// Construct a matrix with a 2D array 
        Matrix4x4( const value_type _m[super::row][super::columns] ):super(_m){}

        /// Construct a matrix with an array 
        Matrix4x4(const value_type _m[super::element_size] ):super(_m){}

        /// Copy constructor 
        Matrix4x4(const Matrix4x4& _m):super(_m){}

        /// Construct with super 
        Matrix4x4(const super & _sm):super(_sm){}

        /// Construct with an initializer list 
        Matrix4x4(const std::initializer_list<value_type>& _il):super(_il){}

        /// Assigning elements value with a 2D array 
        inline Matrix4x4& operator=( const value_type _m[super::row][super::columns])
        {
            super::set(_m);
            return *this;
        }

        /// Assigning elements value with an array 
        inline Matrix4x4& operator=(const value_type _m[super::element_size])
        {
            super::set(_m);
            return *this;
        }

        /// Assigning elements value with another matrix 
        inline Matrix4x4& operator=(const Matrix4x4& _m)
        {
            super::set(_m);
            return *this;
        }

        /// Assigning elements value with a matrix 
        inline Matrix4x4& operator=(const super & _sm)
        {
            super::set(_sm);
            return *this;
        }

        /// Assigning elements value with a initializer list 
        inline Matrix4x4& operator=(const std::initializer_list<value_type>& _il)
        {
            super::set(_il);
            return *this;
        }

        /**
         *  @brief Check if is equls to another matrix
         *
         *  @param _m The matrix to compare
         *
         *  @return  Return true if all elements with same index in two matrices take the same value
         */
        inline bool operator==(const Matrix4x4 & _m) const
        {
            return super::operator==(_m);
        }

        /// Convert to an array 
        inline operator const value_type *()const{ return this->mem;}

        /// Convert to a writable array 
        inline operator value_type *(){ return this->mem; }

        /**
         *  @brief Multiply with a vector(1x4 matrix)
         *
         *  @param _vec The vector to multiply with
         *
         *  @return Return the result vector(1x4 matrix)
         */
        inline vec4 operator *(const vec4 & _vec)
        {
            return vec4( _vec.toMatrix() * (*this) );
        }


        /**
         *  @brief Create a scale matrix
         *
         *  @param _sx scalar in x axis
         *  @param _sy scalar in y axis
         *  @param _sz scalar in z axis
         *
         *  @return Return the scale matrix
         */
        static Matrix4x4 scaleMatrix(value_type _sx ,
                                     value_type _sy ,
                                     value_type _sz)
        {
            Matrix4x4 smat;
            smat.mem[0]  = _sx;
            smat.mem[5]  = _sy;
            smat.mem[10] = _sz;
            return smat;
        }

        /**
         *  @brief Create a scale matrix
         *
         *  @param _vec scalars
         *
         *  @return Return the scale matrix
         */
        static Matrix4x4 scaleMatrix(const vec3& _vec)
        {
            return scaleMatrix(_vec.x , _vec.y,_vec.z);
        }

        /**
         *  @brief Create a translation matrix
         *
         *  @param _tx translation in x axis
         *  @param _ty translation in y axis
         *  @param _tz translation in z axis
         *
         *  @return Return the translation matrix
         */
        static Matrix4x4 translateMatrix(value_type _tx ,
                                         value_type _ty ,
                                         value_type _tz )
        {
            Matrix4x4 t_mat;
            t_mat.mem[12]  = _tx;
            t_mat.mem[13]  = _ty;
            t_mat.mem[14] = _tz;
            return t_mat;
        }

        /**
         *  @brief Create a translation matrix
         *
         *  @param _vec Translation vector
         *
         *  @return Return the translation matrix
         */
        static Matrix4x4 translateMatrix(const vec3& _vec)
        {
            return translateMatrix(_vec.x, _vec.y, _vec.z);
        }

        /**
         *  @brief Create a rotate matrix
         *
         *  @param _r                The angle to rotate in radians
         *  @param arbitraryUnitAxis The arbitrary unit axis
         *
         *  @return Return the rotate matrix
         */
        static Matrix4x4 rotateMatrix(value_type _r,
                                      const vec3 &  arbitraryUnitAxis )
        {
            value_type rx = arbitraryUnitAxis.x ,
            ry = arbitraryUnitAxis.y ,
            rz = arbitraryUnitAxis.z ;
            value_type V1C = 1-cosf(_r);
            value_type data[ Matrix4x4::element_size ]=
            {
                cosf(_r)  + (rx*rx)*V1C , rx*ry*V1C-rz*sinf(_r) , rx*rz*V1C+ry*sinf(_r) , 0 ,
                ry*rx*V1C + rz*sinf(_r) , cosf(_r)+(ry*ry)*V1C  , ry*rz*V1C-rx*sinf(_r) , 0 ,
                rz*rx*V1C - ry*sinf(_r) , rz*ry*V1C+rx*sinf(_r) , cosf(_r)+(rz*rz)*V1C  , 0 ,
                0,                  0,                      0             , 1
            };
            return Matrix4x4(data);
        }

        /**
         *  @brief Create a rotate matrix
         *
         *  @param _r  The angle to rotate in radians
         *  @param _ax The rotation x axis
         *  @param _ay The rotation y axis
         *  @param _az The rotation z axis
         *
         *  @return Return the rotate matrix
         */
        static Matrix4x4 rotateMatrix(value_type _r  ,
                                      value_type _ax ,
                                      value_type _ay ,
                                      value_type _az )
        {
            return rotateMatrix(_r,vec3({_ax,_ay,_az}));
        }

        /**
         *  @brief Create a LookAt matrix
         *
         *  @param _pos    The position of camera(observer)
         *  @param _direct The direction of target
         *  @param _up     The up direction of target
         *
         *  @return Return a LookAt matrix created
         */
        static Matrix4x4 lookAtMatrix(const Vector& _pos ,
                                      const Vector& _direct,
                                      const Vector& _up)
        {
            Vector up = _up;
            up.normalize();
            Vector zaxis = _pos - _direct;
            zaxis.normalize();
            Vector xaxis = _up.cross(zaxis);
            xaxis.normalize();
            Vector yaxis=zaxis.cross(xaxis);
            yaxis.normalize();
            value_type transform[ Matrix4x4::element_size ]=
            {
                xaxis.x , yaxis.x , zaxis.x , 0.0f,
                xaxis.y , yaxis.y , zaxis.y , 0.0f,
                xaxis.z , yaxis.z , zaxis.z , 0.0f,
                -xaxis.dot(_pos) , -yaxis.dot(_pos) ,-zaxis.dot(_pos) , 1.0f
            };
            return transform;
        }


        /**
         *  @brief Create a perspective projection matrix
         *
         *
         *  @param _field      The field angle in degree
         *  @param aspectRatio The ratio of width/height of the viewport
         *  @param _near       The near panel deepth of the frustum
         *  @param _far        The far panel deepth of the frustum
         *
         *  @return Return a perspective projection matrix created
         */
        static Matrix4x4 perspectiveMatrix(value_type      _field ,
                                           value_type aspectRatio ,
                                           value_type       _near ,
                                           value_type        _far  )
        {
            assert(_near!=_far);
            value_type f_n = 1.0f / (_far - _near);
            value_type theta = degree2Radians(_field) * 0.5f;
            value_type divisor = tan(theta);
            assert(divisor);
            value_type factor = 1.0f / divisor;
            assert(aspectRatio);

            Matrix4x4 mat;
            mat.mem[0] = (1.0f / aspectRatio) * factor;
            mat.mem[5] = factor;
            mat.mem[10] = (-(_far + _near)) * f_n;
            mat.mem[11] = -1.0f;
            mat.mem[14] = -2.0f * _far * _near * f_n;
            return mat;
        }

        /**
         *  @brief Translate the matrix
         *
         *  @param _vec The translation vector
         *
         *  @return Return the translated matrix
         */
        inline Matrix4x4& translate(const Vector & _vec )
        {
            super::operator*=( translateMatrix(_vec.x, _vec.y, _vec.z) );
            return *this;
        }

        /**
         *  @brief Translate the matrix
         *
         *  @param _tx The translation in x axis direction
         *  @param _ty The translation in y axis direction
         *  @param _tz The translation in z axis direction
         *
         *  @return Return the translated matrix
         */
        inline Matrix4x4& translate(value_type _tx ,value_type _ty ,value_type _tz)
        {
            super::operator*=(translateMatrix(_tx, _ty, _tz) );
            return *this;
        }


        /**
         *  @brief Scale the matrix
         *
         *  @param _vec The scalar vector
         *
         *  @return Return the scaled matrix
         */
        inline Matrix4x4& scale(const Vector & _vec )
        {
            super::operator*=( scaleMatrix( _vec.x , _vec.y , _vec.z) );
            return *this;
        }

        /**
         *  @brief Scale the matrix
         *
         *  @param _sx The scalar in x axis
         *  @param _sy The scalar in y axis
         *  @param _sz The scalar in z axis
         *
         *  @return Return the scaled matrix
         */
        inline Matrix4x4& scale(value_type _sx ,value_type _sy ,value_type _sz)
        {
            super::operator*=( scaleMatrix( _sx , _sy , _sz) );
            return *this;
        }

        /**
         *  @brief Rotate the matrix
         *
         *  @param _r                The rotation angle in radians
         *  @param arbitraryUnitAxis The rotation axis vector
         *
         *  @return Return the rotated matrix
         */
       inline Matrix4x4& rotate(t_math_value _r , const Vector & arbitraryUnitAxis )
        {
            super::operator*=( rotateMatrix(_r, arbitraryUnitAxis));
            return *this;
        }

        /**
         *  @brief Rotate the matrix
         *
         *  @param _r  The rotation angle in radians
         *  @param _ax The rotation in x axis
         *  @param _ay The rotation in y axis
         *  @param _az The rotation in z axis
         *
         *  @return Return the rotated matrix
         */
        inline Matrix4x4& rotate(t_math_value _r ,value_type _ax ,
                                 value_type _ay  ,value_type _az )
        {
            operator*=( rotateMatrix(_r, Vector({_ax, _ay, _az})));
            return *this;
        }


        /**
         * @brief Get the position from the matrix.
         * @return Position.
         */
        inline vec3 getPosition() const
        {
            return vec3(this->mem2D[3][0] ,this->mem2D[3][1] ,this->mem2D[3][2]);
        }

        /**
         * S@brief et the position in the matrix.
         * @param x X coordinate.
         * @param y Y coordinate.
         * @param z Z coordinate.
         */
        inline void setPosition(value_type x, value_type y, value_type z)
        {
            this->mem2D[3][0] = x;
            this->mem2D[3][1] = y;
            this->mem2D[3][2] = z;
        }

        /**
         *  @brief Set the position for the matrix
         *
         *  @param pos The destination position vector
         */
        inline void setPosition(const vec3 & pos)
        {
            setPosition(pos.x, pos.y, pos.z);
        }

        /**
         *  @brief Get the scale of the matrix
         *
         *  @return Return the scale vector
         */
        inline vec3 getScale()const
        {
            return vec3(this->mem2D[0][0],
                        this->mem2D[1][1],
                        this->mem2D[2][2] );
        }

        /**
         * @brief Set the scale in the matrix
         * @param sx X scale.
         * @param sy Y scale.
         * @param sz Z scale.
         */
        inline void setScale(value_type sx , value_type sy , value_type sz)
        {
            this->mem2D[0][0] = sx;
            this->mem2D[1][1] = sy;
            this->mem2D[2][2] = sz;
        }

        /**
         *  @brief Set the scale in the matrix
         *
         *  @param sv the vector contains scale values
         */
        inline void setScale(vec3 sv)
        {
            this->mem2D[0][0] = sv.x;
            this->mem2D[1][1] = sv.y;
            this->mem2D[2][2] = sv.z;
        }


        ///设置为其逆矩阵
        Matrix4x4 inverse()
        {
            auto m = this->mem2D;

            value_type m00 = m[0][0], m01 = m[0][1], m02 = m[0][2], m03 = m[0][3];
            value_type m10 = m[1][0], m11 = m[1][1], m12 = m[1][2], m13 = m[1][3];
            value_type m20 = m[2][0], m21 = m[2][1], m22 = m[2][2], m23 = m[2][3];
            value_type m30 = m[3][0], m31 = m[3][1], m32 = m[3][2], m33 = m[3][3];

            value_type v0 = m20 * m31 - m21 * m30;
            value_type v1 = m20 * m32 - m22 * m30;
            value_type v2 = m20 * m33 - m23 * m30;
            value_type v3 = m21 * m32 - m22 * m31;
            value_type v4 = m21 * m33 - m23 * m31;
            value_type v5 = m22 * m33 - m23 * m32;

            value_type t00 = + (v5 * m11 - v4 * m12 + v3 * m13);
            value_type t10 = - (v5 * m10 - v2 * m12 + v1 * m13);
            value_type t20 = + (v4 * m10 - v2 * m11 + v0 * m13);
            value_type t30 = - (v3 * m10 - v1 * m11 + v0 * m12);

            value_type invDet = 1 / (t00 * m00 + t10 * m01 + t20 * m02 + t30 * m03);

            value_type d00 = t00 * invDet;
            value_type d10 = t10 * invDet;
            value_type d20 = t20 * invDet;
            value_type d30 = t30 * invDet;

            value_type d01 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
            value_type d11 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
            value_type d21 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
            value_type d31 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

            v0 = m10 * m31 - m11 * m30;
            v1 = m10 * m32 - m12 * m30;
            v2 = m10 * m33 - m13 * m30;
            v3 = m11 * m32 - m12 * m31;
            v4 = m11 * m33 - m13 * m31;
            v5 = m12 * m33 - m13 * m32;

            value_type d02 = + (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
            value_type d12 = - (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
            value_type d22 = + (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
            value_type d32 = - (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

            v0 = m21 * m10 - m20 * m11;
            v1 = m22 * m10 - m20 * m12;
            v2 = m23 * m10 - m20 * m13;
            v3 = m22 * m11 - m21 * m12;
            v4 = m23 * m11 - m21 * m13;
            v5 = m23 * m12 - m22 * m13;

            value_type d03 = - (v5 * m01 - v4 * m02 + v3 * m03) * invDet;
            value_type d13 = + (v5 * m00 - v2 * m02 + v1 * m03) * invDet;
            value_type d23 = - (v4 * m00 - v2 * m01 + v0 * m03) * invDet;
            value_type d33 = + (v3 * m00 - v1 * m01 + v0 * m02) * invDet;

            return super::set({d00, d01, d02, d03,
                               d10, d11, d12, d13,
                               d20, d21, d22, d23,
                               d30, d31, d32, d33});
        }

        /// Get the inverse matrix 
        Matrix4x4 getInverse()const { return Matrix4x4(*this).inverse(); }

        /// Get the inverse affine matrix 
        Matrix4x4 inverseAffine() const
        {
            auto m = this->mem2D;

            value_type m10 = m[1][0], m11 = m[1][1], m12 = m[1][2];
            value_type m20 = m[2][0], m21 = m[2][1], m22 = m[2][2];

            value_type t00 = m22 * m11 - m21 * m12;
            value_type t10 = m20 * m12 - m22 * m10;
            value_type t20 = m21 * m10 - m20 * m11;

            value_type m00 = m[0][0], m01 = m[0][1], m02 = m[0][2];

            value_type invDet = 1 / (m00 * t00 + m01 * t10 + m02 * t20);

            t00 *= invDet; t10 *= invDet; t20 *= invDet;

            m00 *= invDet; m01 *= invDet; m02 *= invDet;

            value_type r00 = t00;
            value_type r01 = m02 * m21 - m01 * m22;
            value_type r02 = m01 * m12 - m02 * m11;

            value_type r10 = t10;
            value_type r11 = m00 * m22 - m02 * m20;
            value_type r12 = m02 * m10 - m00 * m12;

            value_type r20 = t20;
            value_type r21 = m01 * m20 - m00 * m21;
            value_type r22 = m00 * m11 - m01 * m10;

            value_type m03 = m[0][3], m13 = m[1][3], m23 = m[2][3];

            value_type r03 = - (r00 * m03 + r01 * m13 + r02 * m23);
            value_type r13 = - (r10 * m03 + r11 * m13 + r12 * m23);
            value_type r23 = - (r20 * m03 + r21 * m13 + r22 * m23);

            return Matrix4x4({ r00, r01, r02, r03,
                               r10, r11, r12, r13,
                               r20, r21, r22, r23,
                                0,   0,   0,   1});
        }


        /// Get the teterminant 
        value_type determinant() const
        {
            auto m = this->mem2D;
            const value_type *cols[4] = {m[0],m[1],m[2],m[3]};
            return generalDeterminant(cols, 4);
        }


        // Determinant function by Edward Popko
        // Source: http://paulbourke.net/miscellaneous/determinant/
        //==============================================================================
        // Recursive definition of determinate using expansion by minors.
        //
        // Notes: 1) arguments:
        //             a (double **) pointer to a pointer of an arbitrary square matrix
        //             n (int) dimension of the square matrix
        //
        //        2) Determinant is a recursive function, calling itself repeatedly
        //           each time with a sub-matrix of the original till a terminal
        //           2X2 matrix is achieved and a simple determinat can be computed.
        //           As the recursion works backwards, cumulative determinants are
        //           found till untimately, the final determinate is returned to the
        //           initial function caller.
        //
        //        3) m is a matrix (4X4 in example)  and m13 is a minor of it.
        //           A minor of m is a 3X3 in which a row and column of values
        //           had been excluded.   Another minor of the submartix is also
        //           possible etc.
        //             m  a b c d   m13 . . . .
        //                e f g h       e f . h     row 1 column 3 is elminated
        //                i j k l       i j . l     creating a 3 X 3 sub martix
        //                m n o p       m n . p
        //
        //        4) the following function finds the determinant of a matrix
        //           by recursively minor-ing a row and column, each time reducing
        //           the sub-matrix by one row/column.  When a 2X2 matrix is
        //           obtained, the determinat is a simple calculation and the
        //           process of unstacking previous recursive calls begins.
        //
        //                m n
        //                o p  determinant = m*p - n*o
        //
        //        5) this function uses dynamic memory allocation on each call to
        //           build a m X m matrix  this requires **  and * pointer variables
        //           First memory allocation is ** and gets space for a list of other
        //           pointers filled in by the second call to malloc.
        //
        //        6) C++ implements two dimensional arrays as an array of arrays
        //           thus two dynamic malloc's are needed and have corresponsing
        //           free() calles.
        //
        //        7) the final determinant value is the sum of sub determinants
        //
        //==============================================================================
        value_type generalDeterminant(value_type const* const*a,int n)
        {
            int i,j,j1,j2;           // general loop and matrix subscripts
            value_type det = 0;      // init determinant
            value_type **m = NULL;   // pointer to pointers to implement 2d
            // square array

            if (n < 1)    {   }     // error condition, should never get here

            else if (n == 1) {      // should not get here
                det = a[0][0];
            }

            else if (n == 2)  {     // basic 2X2 sub-matrix determinate
                // definition. When n==2, this ends the
                det = a[0][0] * a[1][1] - a[1][0] * a[0][1];// the recursion series
            }

            // recursion continues, solve next sub-matrix
            else {                  // solve the next minor by building a
                // sub matrix
                det = 0;            // initialize determinant of sub-matrix
                // for each column in sub-matrix
                for (j1 = 0; j1 < n; j1++) {
                    // get space for the pointer list
                    m = new value_type*[n-1];

                    for (i = 0; i < n-1; i++)
                        m[i] = new value_type[n-1];
                    //     i[0][1][2][3]  first malloc
                    //  m -> +  +  +  +   space for 4 pointers
                    //       |  |  |  |          j  second malloc
                    //       |  |  |  +-> _ _ _ [0] pointers to
                    //       |  |  +----> _ _ _ [1] and memory for
                    //       |  +-------> _ a _ [2] 4 doubles
                    //       +----------> _ _ _ [3]
                    //
                    //                   a[1][2]
                    // build sub-matrix with minor elements excluded
                    for (i = 1; i < n; i++) {
                        j2 = 0;               // start at first sum-matrix column position
                        // loop to copy source matrix less one column
                        for (j = 0; j < n; j++) {
                            if (j == j1) continue; // don't copy the minor column element

                            m[i-1][j2] = a[i][j];   // copy source element into new sub-matrix
                            // i-1 because new sub-matrix is one row
                            // (and column) smaller with excluded minors
                            j2++;                  // move to next sub-matrix column position
                        }
                    }

                    det += pow(-1.0,1.0 + j1 + 1.0) * a[0][j1] * generalDeterminant(m,n-1);
                    // sum x raised to y power
                    // recursively get determinant of next
                    // sub-matrix which is now one
                    // row & column smaller

                    for (i = 0 ; i < n-1 ; i++) delete[] m[i];// free the storage allocated to
                    // to this minor's set of pointers
                    delete[] m;  // free the storage for the original
                    // pointer to pointer
                }
            }
            return(det) ;
        }
    };
}
#endif
