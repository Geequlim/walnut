#ifndef __Walnut_VectorBase_hpp__
#define __Walnut_VectorBase_hpp__

namespace Walnut
{
    /// The vector class, defines a lot of basic elements and methods
    template<typename T , unsigned elements>
    struct VectorBase
    {
        /// The value type of the elements 
        typedef T value_type;

        /// The number of the elements 
        static constexpr unsigned element_count = elements;

        /// The matrix style type of this kind of vector 
        typedef MatrixBase<value_type,1U,element_count> match_matrix_type;

        /// The elements in an array 
        value_type mem[element_count];

        /// The default constructor 
        VectorBase() { setDefault();}

        /// Construct a vector with a calar, all elements are set to the value of the scalar 
        VectorBase(value_type _v){ set(_v);}

        /// Copy constructor 
        VectorBase(const VectorBase& _v){ set(_v); }

        /** @brief Construct with another kind of vector
         *
         *  @param _v The vector to copy data.If the number of the elements of _v is samller, the rest elements of this vector are initialized as the default
         */
        template<typename U , unsigned _elements>
        VectorBase(const VectorBase<U,_elements>& _v)
        {
            setDefault();
            set(_v);
        }

        /// Construct with the same length matrix 
        VectorBase(const match_matrix_type& _m)
        {
            setDefault();
            set(_m);
        }

        /** @brief Construct with an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will be initialized as default.
         */
        VectorBase(const std::initializer_list<value_type> & _a )
        {
            setDefault();
            set(_a);
        }

        /// Assigning to another vector 
        inline VectorBase& operator=(const VectorBase & _v)
        {
            return set(_v);
        }

        /// Assigning with another kind of vector 
        template<typename U , unsigned _elements>
        inline VectorBase& operator=(const VectorBase<U,_elements>& _v)
        {
            return set(_v);
        }

        /// Assigning with a matrix that has the same number of elements 
        inline VectorBase& operator=(const match_matrix_type& _m)
        {
            return set(_m);
        }

        /** @brief Assinging elements values to an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will not be changed
         */
        inline VectorBase& operator=(const std::initializer_list<value_type> & _a )
        {
            return set(_a);
        }

        /// Convert to an array 
        inline operator const value_type*()const {return mem;}

        /// Convert to a writable array 
        inline operator value_type*(){ return mem; }

        /// Set to default initialized vector , all elements are set to 0 
        inline VectorBase& setDefault()
        {
            for( value_type & e : mem)
                e = 0;
            return *this;
        }

        /// Set values with a calar, all elements are set to the value of the scalar
        inline VectorBase& set( value_type _v)
        {
            for(value_type & e : mem)
                e = _v;
            return *this;
        }

        /** @brief Set values with another kind of vector
         *
         *  @param _v The vector to copy data.If the number of the elements of _v is samller, the rest elements of this vector will not be changed.
         */
        template<typename U , unsigned _elements>
        inline VectorBase& set(const VectorBase<U,_elements>& _v)
        {
            for(size_t i=0; i!= element_count && i!= _v.element_count ; ++i )
                mem[i] = _v.mem[i];
            return *this;
        }

        /// Set elements values by a matrix which has the same elements 
        inline VectorBase& set(const match_matrix_type& _m)
        {
            set(_m.mem);
            return *this;
        }

        /** @brief Set elements values to an initializer list
         *
         *  @param _a The elements value list. If length of _a is sorter than this vector the rest elements will not be changed
         */
        inline VectorBase& set(const std::initializer_list<value_type> & _a )
        {
            for(size_t i=0 ; i!=_a.size() && i!=element_count ; ++i  )
                mem[i] = *(_a.begin()+i);
            return *this;
        }

        /// Add by a calar  
        inline VectorBase& operator +=(value_type _s)
        {
            for(value_type & e :mem)
                e += _s;
            return *this;
        }

        /// Add by a calar  
        inline VectorBase operator+(value_type _s)const
        {
            VectorBase v(mem);
            return v+=(_s);
        }

        /// Add by a vector
        inline VectorBase& operator+=(const VectorBase & _v)
        {
            for(size_t i=0; i!=element_count; ++i )
                mem[i] += _v.mem[i];
            return *this;
        }

        /// Add by a vector
        inline VectorBase operator+(const VectorBase & _v)const
        {
            VectorBase v(_v);
            return v += _v;
        }

        /// Subtracting by a scalar 
        inline VectorBase& operator-=(value_type _s)
        {
            return operator+=( -_s );
        }

        /// Subtracting by a scalar 
        inline VectorBase operator-(value_type _s)const
        {
            return operator+(-_s);
        }

        /// Subtracting by a vector 
        inline VectorBase operator-(const VectorBase & _v)const
        {
            return operator+(-_v);
        }

        /// Subtracting by a vector 
        inline VectorBase& operator-=(const VectorBase & _v)
        {
            return operator+=(-_v);
        }

        /// Get negative vector 
        inline VectorBase operator-()const
        {
            VectorBase v(*this);
            for( auto & e  : mem)
                e = -e;
            return v;
        }

        /// Multiplied by a scalar 
        inline VectorBase& operator*=(value_type _s)
        {
            for(value_type & e : mem )
                e *= _s;
            return *this;
        }

        /// Multiplied by a scalar 
        inline VectorBase operator*(value_type _s)const
        {
            VectorBase v;
            v.set(mem);
            return v *= _s;
        }

        /// Multiplied by a vector 
        inline VectorBase& operator*=(const VectorBase & _v)
        {
            for(size_t i=0; i!=element_count; ++i )
                mem[i] *= _v.mem[i];
            return *this;
        }

        /// Multiplied by a vector 
        inline VectorBase operator*(const VectorBase & _v)const
        {
            VectorBase v;
            v.set(mem);
            return v *= _v;
        }


        /**
         *  @brief Multiplied(transformed) by a matrix
         *
         *  @param _m  The multiply(transform) matrix, the row number must be as same as the number of this vector
         *
         *  @return Return the transformed result vector
         */
        template<unsigned _columns>
        VectorBase<value_type,_columns>
        operator*(const MatrixBase<value_type,element_count, _columns>& _m )
        {
            auto result = toMatrix() * _m;
            return result;
        }

        /// Get the matrix style type of the vector
        inline match_matrix_type toMatrix()const { return match_matrix_type(mem); }

        /**
         *  @brief Calculate the dot multiply result with another vector
         *
         *  @param _v The vector to dot multiply with
         *  @return Return the result scalar
         */
        inline  value_type dot(const VectorBase & _v)const
        {
            value_type result = 0;
            for(size_t i=0; i!=element_count; ++i )
                result += mem[i] * _v.mem[i];
            return result;
        }

        /**
         *  @brief Get the result of cross multiplied by another vector
         *
         *  note The result vector is vertical to this and @e _v
         *       @li (v1 cross v2) != (v2 cross v1)
         *
         *  @param  _v The vector to cross multiply with
         *
         *  @return Return the result vector
         */
        inline VectorBase<value_type,3> cross(const VectorBase<value_type,3> & _v)const
        {
            VectorBase<value_type,3> v1;
            v1.set(*this);
            return VectorBase<value_type,3>( {
                v1.mem[1] * _v.mem[2] - v1.mem[2] * _v.mem[1] ,
                v1.mem[2] * _v.mem[0] - v1.mem[0] * _v.mem[2] ,
                v1.mem[0] * _v.mem[1] - v1.mem[1] * _v.mem[0] }
            );
        }

        /// Normalize the vector,set the length to 1 
        inline  VectorBase& normalize()
        {
            value_type s = 0;
            for(const value_type & e : mem )
                s += e*e;
            s = sqrt(s);
            if (s == 1.0f)
                return *this;
            if(s > 1e-08 )
            {
                s = 1.0f / s;
                for(value_type & e : mem )
                    e *= s;
            }
            return *this;
        }

        /// Get normalized vector form this vector 
        inline VectorBase getNormalized() const
        {
            VectorBase v(mem );
            return v.normalize();
        }

        /** @brief Get length of the vecotr
         *
         *  @return Return the length of the vector
         */
        inline value_type length()const
        {
            value_type sum = 0;
            for(const value_type & e : mem )
                sum += e*e;
            return sqrt( sum );
        }

        /** @brief Get distance between two vectors
         *
         *  @param _v The target vector
         *
         *  @return Return the distance between _v
         */
        inline value_type distance(const VectorBase& _v)
        {
            return (*this - _v).lenghth();
        }

        /**
         *  @brief Get the angle in radian between another vector
         *
         *  @param _v The vector to form the angle with this vector
         *
         *  @return Return the angle in radian
         */
        inline value_type angleBetween(const VectorBase& _v)
        {
            value_type lenProduct = length() * _v.length();
            if(lenProduct < 1e-6f)
                lenProduct = 1e-6f;
            value_type f = dot(_v) / lenProduct;
            f = clamp<value_type>(f, -1.0, 1.0);
            return acos(f);
        }

        /**
         *  @brief Check if is the vectors are parallel
         *
         *  @param _v The vector to check with
         */
        inline bool equls(const VectorBase<value_type, 3> & _v)
        {
            return getNormalized() == _v.getNormalized();
        }

        /// Check if is all elements in the same index of two vectors take the same value 
        inline bool operator==(const VectorBase & _v)const
        {
            bool equls = false;
            for(size_t i=0; i!=element_count; ++i )
            {
                equls = mem[i] == _v.mem[i];
                if(!equls) break;
            }
            return equls;
        }

        /// Check if is vectors contains diffent elements 
        inline bool operator!=(const VectorBase & _v)const
        {
            return !operator==(_v);
        }

        /// Define the < operator to enable storing in containers of std , in fact it comparing the length of the vectors 
        inline bool operator<(const VectorBase & _vec)
        {
            return length() < _vec.length();
        }

    protected:

        /// Set elements value by an value array 
        inline VectorBase& set(const value_type _ad[element_count])
        {
            memcpy(mem, _ad, sizeof(value_type)*element_count);
            return *this;
        }
    };

    /**
     *  @brief Output elements data to ostream
     *
     *  @param strm         The output string
     *  @param _v           The data source vector
     */
    template<typename T,unsigned elements>
    std::ostream & operator << (std::ostream & strm , const VectorBase<T,elements> & _v)
    {
        for(size_t i=0; i!=_v.element_count; ++i )
        {
            strm << _v.mem[i];
            if(i!=_v.element_count-1)
                strm<<',';
        }
        return strm;
    }

    /// Get string from a vector 
    template<typename T,unsigned elements>
    string to_string( const VectorBase<T,elements> & _v)
    {
        stringstream vss;
        vss << _v;
        return vss.str();
    }

}
#endif
