#pragma once
#include "Macros.h"
#include "../util/util.h"
//Json
#include "json/parser/jsonxx/jsonxx.h"
#include "json/JsonPreference.h"

//Resources
#include "res/Resource.h"
#include "res/ResourcePack.h"
#include "res/ResourceBox.h"
#include "res/ResDepot/ResDepot.h"
