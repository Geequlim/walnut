#ifndef __Walnut__ResourcePack_H_
#define __Walnut__ResourcePack_H_

namespace Walnut
{
    namespace Extension
    {
        /// The Resource package class
        class ResourcePack
        {
        protected:
            /// The resources in the package
            vector<Resource> mResources;
        public:
            /// Construct a defalut resource package
            ResourcePack() = default;
            
            /*!
             @brief  Construct a resource package with a resource vector
             @param resources The vector contains resources
             */
            ResourcePack(const std::vector<Resource> & resources):mResources(resources){}
            
            virtual ~ResourcePack(){}
            
            /**
             *  @brief Get the resources of the package
             *  @return Return the vector contains all the resources in the package
             */
            virtual std::vector<Resource> & resources(){ return mResources; }
            
            /**
             *  @brief Get the resources of the package
             *  @return Return the vector contains all the resources in the package
             */
            virtual const std::vector<Resource> & resources()const{ return mResources;}
            
            /**
             *  @brief Set the resources of the package
             *  @param resources The vector contains resources to set to the package
             */
            void setResources( const std::vector<Resource> & resources)
            {
                mResources = resources;
            }
            
            /**
             *  @brief Get resource content by the index
             *  @param index Index of the resource
             *  @return Return nullstr if index is invalid
             */
            std::string operator[](unsigned index)const
            {
                if(index<mResources.size())
                    return mResources[index].content;
                else
                    return nullstr;
            }
            
            /**
             *  @brief Get resource content by the resource name
             *  @param resName Name of the resource
             *  @return Return nullstr if doesn't contains the resource
             */
            std::string operator[](const std::string & resName)const
            {
                Resource res;
                res.name = resName;
                auto  foundIt = find( mResources.begin() ,mResources.end() , res );
                if(foundIt!=mResources.end())
                    res.content = foundIt->content;
                return res.content;
            }
            
            /**
             *  @brief Add resource into the package
             *  @param res The resource to add
             */
            void addResource(const Resource & res) { mResources.push_back(res); }
            
            /**
             *  @brief Get the number of resources in the package
             *  @return Return the resources count
             */
            size_t resourceCount()const { return mResources.size(); }
            
            /**
             *  @brief Get resource by index
             *  @param index The index of the resource to get
             *  @return Return the found resource or nullres
             */
            Resource resAt(size_t index )
            {
                if( index < mResources.size() )
                    return mResources[index];
                else
                    return nullres;
            }
            
            /**
             *  @brief Get resource by name
             *  @param resName Name of the resource to find
             *  @return Return the found resource or the nullres
             */
            Resource getResource( const std::string & resName)
            {
                Resource res = nullres;
                for (size_t i=0; i!=mResources.size(); ++i)
                {
                    if( mResources[i].name == resName )
                    {
                        res = mResources[i];
                        break;
                    }
                }
                return res;
            }
        };
    }
}

#endif
