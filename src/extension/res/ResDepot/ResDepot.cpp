#include "../../extention.h"

namespace Walnut
{
    namespace Extension
    {
        bool ResDepot:: parseJson(const std::string & jsonContent)
        {
            bool succeed = false;
            jsonxx::Object rootObject;
            if ( (succeed = rootObject.parse(jsonContent) ) )
            {
                //load resources
                mResources.clear();
                if( rootObject.has<jsonxx::Array>("resources") )
                {
                    jsonxx::Array resources = rootObject.get<jsonxx::Array>("resources");
                    for (unsigned int i=0; i!=resources.size(); ++i)
                    {
                        jsonxx::Object curReource = resources.get<jsonxx::Object>(i);
                        ResDepotResource resData;
                        if(curReource.has<jsonxx::String>("name"))
                            resData.name = curReource.get<jsonxx::String>("name");
                        if(curReource.has<jsonxx::String>("type"))
                            resData.stype = curReource.get<jsonxx::String>("type");
                        if(curReource.has<jsonxx::String>("url"))
                            resData.content = curReource.get<jsonxx::String>("url");
                        if(curReource.has<jsonxx::String>("scale9grid"))
                            resData.scale9grid = curReource.get<jsonxx::String>("scale9grid");
                        mResources.push_back(resData);
                    }
                    succeed = true;
                }
                //load groups
                mGroups.clear();
                if( rootObject.has<jsonxx::Array>("groups") )
                {
                    jsonxx::Array groups = rootObject.get<jsonxx::Array>("groups");
                    for (unsigned int i=0; i!=groups.size(); ++i)
                    {
                        jsonxx::Object curGroup = groups.get<jsonxx::Object>(i);
                        
                        //get current group name
                        std::string curGroupName("UnTitled");
                        if(curGroup.has<jsonxx::String>("name"))
                            curGroupName = curGroup.get<jsonxx::String>("name");
                        mGroups[curGroupName].name = curGroupName;
                        // get current group
                        ResourceGroup & curResGroup = mGroups[curGroupName];
                        
                        if(curGroup.has<jsonxx::String>("keys"))
                        {
                            std::string keysStr = curGroup.get<jsonxx::String>("keys");
                            std::vector<std::string> keys =  Util::String::split(keysStr, ",");
                            for (size_t keyIndex=0; keyIndex!=keys.size(); ++keyIndex)
                            {
                                ResDepotResource curGroupEle;
                                curGroupEle.name = keys[keyIndex];
                                std::vector<ResDepotResource>::iterator foundResIt =
                                    find( mResources.begin(),mResources.end(),curGroupEle);
                                if(foundResIt!= mResources.end())
                                {
                                    curResGroup.resources.push_back(&*foundResIt);
                                }
                            }
                        }
                    }
                }
                // Add all resources ungrouped to the group 'UnGrouped'
                for (size_t i=0; i!=mResources.size(); ++i)
                {
                    ResDepotResource * curResource = &mResources[i];
                    bool isUngrouped = true;
                    for (std::map<std::string,ResourceGroup>::iterator curGroupIt = mGroups.begin();
                            curGroupIt!=mGroups.end(); ++curGroupIt)
                    {
                        if ( find( curGroupIt->second.resources.begin() , curGroupIt->second.resources.end(),curResource )
                            != curGroupIt->second.resources.end() )
                        {
                            isUngrouped = false;
                            break;
                        }
                    }
                    if(isUngrouped)
                        mGroups["UnGrouped"].resources.push_back(curResource);
                }
            }
            return succeed;
        }
        
        ResDepotResource ResDepot::getResourceByName(const std::string & resName)
        {
            ResDepotResource res;
            res.name = resName;
            auto foundResIt = find(mResources.begin() , mResources.end(),res);
            if( foundResIt!=mResources.end() )
                res = *foundResIt;
            else
                res.name = std::string();
            return res;
        }
    }
}