#ifndef __Walnut_ResData_h__
#define __Walnut_ResData_h__
#include <string>
#include <vector>
namespace Walnut
{
    namespace Extension
    {
        /// Basic abstracted resource structure
        struct Resource
        {
        public:
            std::string name;  ///< Name of the resource
            std::string stype; ///< The resource type description
            /// Content of the resource or the path(url) can find the resource
            std::string content;

            /*!
             @brief  Check two Resource object is linked to the same source
             @param anothor The resource to compare with
             @return Return @b true if they have the same name
             */
            bool operator==(const Resource & anothor)const
            {
                return anothor.name == name;
            }
        };
        
        /// The nullres can be treated as the resource means noting
        extern const Extension::Resource nullres;
    }
}


#endif
