#ifndef __JsonPreference_H__
#define __JsonPreference_H__
#include "parser/jsonxx/jsonxx.h"

namespace Walnut
{
    namespace  Extension
    {
        /*!
         @brief Json data manager
         @par It can store and retrieve booleans,numbers,strings and Json objects as key-values,vectors and maps
         @attention The set- series methods will rewrite the same named values,event they are stored in diffrent types
         */
        class JsonPreference
        {
        private:
            ///Json data object as the source data
            jsonxx::Object mRootJsonObj;
        public:
            
            ///Construct as default
            JsonPreference()  = default;
            
            /*!
             @brief  Construct with a json object
             @param jsonObject The json object
             */
            JsonPreference(const jsonxx::Object & jsonObject)
            {
                mRootJsonObj = jsonObject;
            }
            
            virtual ~JsonPreference(){};
            
            /*!
             @brief  Set boolean value
             @param key   The key name of the boolean pair
             @param value The value to set
             */
            void setBool(const std::string& key, const bool & value)
            {
                setValue<bool>(key, value);
            }
            
            /*!
              @brief Get boolean value by key name
              @param key The key name of the boolean pair
              @param defaultValue The default value
              @return Return the boolean value if it is found or return the default value
             */
            bool getBool(const std::string& key, const bool & defaultValue)const
            {
                return getValue<bool>(key, defaultValue);
            }
            
            /*!
             @brief Set a series of boolean values
             @param key   The key name of the boolean list(array)
             @param value The boolean values stored in the vector
             */
            void setBoolArray(const std::string& key, const std::vector<bool>& value)
            {
                setArray<jsonxx::Boolean>(key, value);
            }
            
            /*!
             @brief Get a series of boolean values
             @param key The key name of the boolean list(array)
             @return Return a boolean vector contains booleans or a empty boolean vector
             */
            std::vector<bool> getBoolArray(const std::string & key)const
            {
                return getArray<bool>(key);
            }
            
            /*!
             @brief Set boolean key-value map
             @param key   The name of the map as the key
             @param map   The key-value boolean map as the value
             */
            void setBoolMap(const std::string & key,
                            const std::map<std::string, bool>& map)
            {
                setMap<jsonxx::Boolean>(key, map);
            }
            
            /*!
             @brief Get boolean key-value map
             @param key The name of the map as the key
             @return Return the key-value map if it is found or return a empty map
             */
            std::map<std::string, bool> getBoolMap(const std::string & key)const
            {
                return getMap<jsonxx::Boolean>(key);
            }
            
            /*!
             @brief Set string value
             @param key   The key name of the string key-value pair
             @param value The value of the string key-value
             */
            void setString(const std::string& key, const std::string & value)
            {
                setValue<std::string>(key, value);
            }
            
            /*!
             @brief Get string
             @param key          The key name of the string key-value pair
             @param defaultValue The default string value
             @return Return the string value if it is found or return the default value
             */
            std::string getString(const std::string& key,
                                  const std::string & defaultValue)const
            {
                return getValue<std::string>(key, defaultValue);
            }
            
            /*!
             @brief Set string array(list)
             @param key   The key name of the string array(list)
             @param value The vector contains strings to set
             */
            void setStringArray(const std::string& key,
                                const std::vector<std::string>& value)
            {
                setArray<std::string>(key, value);
            }
            
            /*!
             @brief Get string array(list)
             @param key The key name of the string array(list)
             @return Return the string vector if it is found or return a empty vector
             */
            std::vector<std::string> getStringArray(const std::string & key)const
            {
                return getArray<std::string>(key);
            }
            
            /*!
               @brief Set string key-value map
               @param key  The key name of the map
               @param map  The data map to set
             */
            void setStringMap(const std::string & key,
                              const std::map<std::string, std::string> & map)
            {
                setMap<std::string>(key, map);
            }
            
            /**
             *  @brief Get string key-value map
             *  @param key The key name of the map
             *  @return Return the string map if it is found or return a empty string map
             */
            std::map<std::string, std::string> getStringMap(const std::string & key)const
            {
                return  getMap<std::string>(key);
            }
            
            
            /**
             *  @brief Set number value
             *  @param key    The key name of the number
             *  @param number The value of the number to set
             */
            void setNumber(const std::string & key, const jsonxx::Number & number)
            {
                setValue<jsonxx::Number>(key, number);
            }
            
            /**
             *  @brief Get number value by key name
             *  @param key           The key name of the number
             *  @param defaultNumber The default number value
             *
             *  @return Return the found number value if it is found or return the default number value
             */
            jsonxx::Number getNumber(const std::string & key,
                                     const jsonxx::Number & defaultNumber)const
            {
                return getValue<jsonxx::Number>(key, defaultNumber);
            }
            
            /**
             *  @brief Set number array(list)
             *  @param key   The key name of the number array(list)
             *  @param value The source number data vector
             */
            void setNumberArray(const std::string& key,
                                const std::vector<jsonxx::Number>& value)
            {
                setArray<jsonxx::Number>(key, value);
            }
            
            /**
             *  @brief Get number array(list)
             *  @param key The key name of the number array(list)
             *  @return Return the vector contains numbers if it is found or return a empty vector
             */
            std::vector<jsonxx::Number> getNumberArray(const std::string & key)const
            {
                return getArray<jsonxx::Number>(key);
            }
            
            /**
             *  @brief Set number key-value map
             *  @param key   The key name of the number map
             *  @param map   The map contains string-number key-values to set
             */
            void setNumberMap(const std::string & key,
                              const std::map<std::string, jsonxx::Number> & map)
            {
                setMap<jsonxx::Number>(key,map);
            }
            
            /**
             *  @brief Get number key-value map
             *  @param key The key name of the key-value map
             *  @return Return the map contains number key-values if it is found or return a empty map
             */
            std::map<std::string, jsonxx::Number> getNumberMap(const std::string & key)const
            {
                return getMap<jsonxx::Number>(key);
            }
            
            /**
             *  @brief Set json preference(Json data manager)
             *  @param key        The key name of the child preference
             *  @param preference The json preference data to set
             */
            void setPreference(const std::string & key , const JsonPreference & preference)
            {
                mRootJsonObj << key << preference.mRootJsonObj;
            }
            
            /**
             *  @brief Get json preference data
             *  @param key The key name of the json preference data
             *  @return Return the found preference object or return a empty preference object
             */
            JsonPreference getPreference(const std::string & key)const
            {
                if( mRootJsonObj.has<jsonxx::Object>(key) )
                    return JsonPreference( mRootJsonObj.get<jsonxx::Object>(key) );
                else
                    return JsonPreference();
            }
            
            /**
             *  @brief Get the json content of its json data
             *  @return Return the json content string
             */
            std::string jsonContent()const{ return mRootJsonObj.json(); }
            
            /**
             *  @brief Get the json object with jsonxx data structured
             *  @return Return the json object of its json data
             */
            const jsonxx::Object&  getJsonObject() const { return mRootJsonObj; }
            
            /**
             *  @brief Get the json object with jsonxx data structured
             *  @return Return the json object of its json data
             */
            jsonxx::Object& getJsonObject(){ return mRootJsonObj; }
            
        protected:
            
            ///Set value
            template<typename T>
            void setValue(const std::string& key, const T & value)
            {
                mRootJsonObj << key << value;
            }
            
            ///Get value by key
            template<typename T>
            T getValue(const std::string& key, const T & defaultValue)const
            {
                if (mRootJsonObj.has<T>(key))
                    return mRootJsonObj.get<T>(key);
                else
                    return defaultValue;
            }
            
            ///Set array(list) value
            template<typename T>
            void setArray(const std::string & key, const std::vector<T>& value)
            {
                jsonxx::Array array;
                for (size_t i = 0; i < value.size(); i++)
                    array << (T)value[i] ;
                mRootJsonObj << std::string(key) << array;
            }
            
            ///Get array(list) by key
            template<typename T>
            std::vector<T> getArray(const std::string & key)const
            {
                std::vector<T> values;
                if (mRootJsonObj.has<jsonxx::Array>(key))
                {
                    jsonxx::Array array = mRootJsonObj.get<jsonxx::Array>(key);
                    for (unsigned int i = 0; i < array.size(); i++)
                        values.push_back(array.get<T>(i));
                }
                return values;
            }
            
            ///Set map value
            template<typename T>
            void setMap(const std::string & mapName, const std::map<std::string, T> & map)
            {
                jsonxx::Object mapObj;
                for( typename std::map<std::string, T>::const_iterator it = map.begin(); it != map.end(); ++it)
                    mapObj << it->first << it->second;
                mRootJsonObj << std::string(mapName) << mapObj;
            }
            
            ///Get map by key name
            template<typename T>
            std::map<std::string,T> getMap(const std::string & mapName)const
            {
                std::map<std::string, T> returnMap;
                if (mRootJsonObj.has<jsonxx::Object>(mapName))
                {
                    jsonxx::Object mapObj = mRootJsonObj.get<jsonxx::Object>(mapName);
                    const std::map<std::string, jsonxx::Value*> & realMap = mapObj.kv_map();
                    for (std::map<std::string, jsonxx::Value*>::const_iterator it = realMap.begin(); it != realMap.end(); ++it)
                    {
                        if (it->second->is<T>())
                            returnMap[it->first] = it->second->get<T>();
                    }
                }
                return returnMap;
            }
        };
    }
}
#endif