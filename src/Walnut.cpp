//
//  Walnut.cpp
//  Walnut
//
//  Created by Geequlim on 15/8/11.
//
//

#include "Walnut.h"
#include <iomanip>
namespace Walnut
{
    
    //Define the currentPlatform value
#if __WINDOWS__
    Platform const currentPlatform = Platform::Windows;
#elif __LINUX__
    Platform const currentPlatform = Platform::Linux;
#elif __UNIX__
    Platform const currentPlatform = Platform::Unix;
#elif __POSIX__
    Platform const currentPlatform = Platform::Posix;
#elif __IOS__
    Platform const currentPlatform = Platform::iOS;
#elif __MAC_OSX__
    Platform const currentPlatform = Platform::MacOSX;
#elif __ANDROID__
    Platform const currentPlatform = Platform::Android;
#endif

    // Define the engine singleton
    CoreEngine * engine = nullptr;
    
    //Define the nullstr as empty string
    const string nullstr;

    //Define the nullwstr as empty string
    const wstring nullwstr;
    
    // Define the nullcolor as empty color
    const Color nullcolor;
    
    //(0,0) 2D Position
    const Point zeroPoint{0};
    
    //(0,0,0) 3D Position
    const Vector zeroVector{0};
    
    //The size whose width = height = 0
    const Size zeroSize;
    
    //Current system language
    const Language currentLanguage = systemLanguage();
    
    
    // Get hex code string from a color
    string to_string(const Color& color)
    {
        std::stringstream ss;
        ss << std::hex << std::setfill('0') << std::setw(2);
        unsigned r = color.r;        
        unsigned g = color.g;
        unsigned b = color.b;
        unsigned a = color.a;
        ss << r << g << b << a;
        return ss.str();
    }
    
}