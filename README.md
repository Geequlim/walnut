#### Walnut is a framwork for developing cross-platform games and applications.

Current version: `0.0.0`

---

## Build the framwork with Premake5

** Install [Premake5](https://premake.github.io/) **

** Add the environment variable  `WalnutHome` to your system **

The variable `WalnutHome` should set to the absolute path of walnut home directory seprated by `/` such as

```
C:/Users/Geequlim/Documents/Workspace/Develop/walnut
```

** Go to walnut home directory with your command line tool **

    $ cd $WalnutHome

** Generate projects with Premake5 **

 You can get help informations with `--help` option:


    $ premake5 --help


** Open projects generated with your IDE and enjoy it ! **

---

## Get dependences for your system

#### Some third-part libraries are required to build executable applications based on Walnut. You can download them from [here](https://github.com/Geequlim/Walnut-Libs) for your system environment.

Put the *binary* directory you download under the *$WalnutHome/libs* directory.

#### Dependences of Linux

The dependences are required on Linux.

Library | Description
---|---
xorg-dev  | X11 required
libglu1-mesa-dev | X11 application requred
libgtk-3-dev | For native dialogs
doxygen | For documentation generation
graphviz | For documentation generation

---

## Demos

There are a lot of demos showing you how to use the Walnut framework.

You can get demos from [here](https://github.com/Geequlim/Walnut-Demos).

---

## Generate documentations with [Doxygen](http://example.com)

1. Install [Doxygen](http://www.stack.nl/~dimitri/doxygen/download.html)
2. Install [Graphviz](http://www.graphviz.org/Download.php) (dot tool)
    * If you don't want to generate the class diagrams this is not needed as long as you disable the `CLASS_DIAGRAMS` option to `false`.
3. Run doxygen with doxyfile.


    $ cd $WalnutHome/doc
    $ doxygen doxyfile  
