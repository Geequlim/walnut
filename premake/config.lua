-- The walnut configurations
local config = {
  -- The walnut home directory
  home_dir = path.getabsolute('..'),
  -- Build OS name
  buildOS    =  os.get(),
  -- The architecture
  architecture = 'x86',
  -- Output directory
  outDir = '.',
  -- Dependences of the engine
  dependences = {'assimp','soil2','glfw','glad','freetype'},
  include_dirs = {},
  dependece_dirs = {}
}

if _ACTION == 'androidmk' or _ACTION == 'android' then
  config.buildOS = 'android'
elseif  _OPTIONS['iOS'] then
  configuration { 'xcode*' }
      config.buildOS = 'ios'
  filter {}
end

-- Set architecture
if config.buildOS == 'linux' or config.buildOS == 'macosx' then
  config.architecture = 'x86_64'
elseif config.buildOS == 'android' then
  config.architecture = '$(TARGET_ARCH_ABI)'
else
  config.architecture = 'x86'
end

-- Dependences head files
do
  for i = 1, #config.dependences do
      config.include_dirs[i] = path.getabsolute( config.home_dir..'/libs/include/'..config.dependences[i])
  end
  table.insert(config.include_dirs,config.home_dir..'/src')
end

-- Dependences directories
for i = 1, #config.dependences do
    config.dependece_dirs[i] = path.getabsolute( config.home_dir..'/libs/binary/'..config.dependences[i]..'/'..config.buildOS..'/'..config.architecture )
end

-- Add more dependences
do
  local platform_deps = {}
  if config.buildOS == 'macosx' then
      table.insert( config.dependences, {'Cocoa.framework','OpenGL.framework','IOKit.framework','CoreFoundation.framework'} )
  elseif config.buildOS == 'windows' then
      table.insert( config.dependences, {'OpenGL32'} )
  elseif config.buildOS == 'linux' then
      table.insert( config.dependences, {'GL','X11','Xrandr','Xinerama','Xcursor','Xi','pthread','Xxf86vm','gtk-3','glib-2.0','gobject-2.0','dl'} )
  elseif config.buildOS == 'android' then
      -- table.insert( config.dependences, {'EGL','GLESv2','android','log'} )
      config.dependences = {'EGL','GLESv2','android','log'}
  end
  table.insert( config.dependences, 'Walnut' )
end

function config.applyToCurrentProject( isLibProject )
  -- architecture( config.architecture)
  includedirs( config.include_dirs )
  if not isLibProject then
      links(config.dependences)
      libdirs(config.dependece_dirs)
  end
  if config.buildOS == 'linux' then
      buildoptions {'`pkg-config --cflags gtk+-3.0 glib-2.0`'}
  end
  filter {}
  configuration { 'xcode*' }
        xcodebuildsettings { ['ALWAYS_SEARCH_USER_PATHS'] = 'YES';
                             ['CLANG_CXX_LANGUAGE_STANDARD'] = 'c++0x';
       }
  configuration { 'vs*'}
        defines {'__Visual_Studio__'}
  configuration { 'codelite'}
        defines {'__codelite__'}
  filter {}
end

return config;
