-- Walnut project script
local config = require "config"

local libWalnut = {}

function libWalnut.addToWorkspace( outdir )
  project 'Walnut'
    language 'C++'
    kind 'StaticLib'

    local home_dir = config.home_dir

    config.applyToCurrentProject(true)

    -- Add source code
    files { home_dir..'/src/**' }
    -- Set source files for platforms
    removefiles { home_dir..'/src/platform/**'}
    files { home_dir..'/src/platform/platform.h',
            home_dir..'/src/platform/common/**'
          }
    if config.buildOS == 'macosx' then
      files { home_dir..'/src/platform/desktop/*',
              home_dir..'/src/platform/desktop/osx/**',
              home_dir..'/src/platform/apple/**'
            }
    elseif config.buildOS == 'windows' then
      files { home_dir..'/src/platform/desktop/*'}
      files { home_dir..'/src/platform/desktop/windows/**'}
    elseif config.buildOS == 'linux' then
      files {   home_dir..'/src/platform/desktop/*',
                  home_dir..'/src/platform/desktop/linux/**',
                  home_dir..'/src/platform/desktop/gtk/**'
                }
    elseif config.buildOS == 'android' then
        files { home_dir..'/src/platform/android/**' }
        amk_importmodules 'android/native_app_glue'
        amk_staticlinks 'android_native_app_glue'
    end

    filter {}
    configuration { 'netbeans' }
      location( outdir..'/netbeans/Walnut' )
    filter {}
end

return libWalnut;
