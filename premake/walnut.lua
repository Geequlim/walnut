-- Workspace name option
newoption {
   trigger     = 'workspace',
   value       = 'TEXT',
   description = 'Your workspace/solution name.'
}

-- Package name
newoption {
   trigger     = 'package',
   value       = 'TEXT',
   description = 'The package name of your projects like "com.walnut.hello"'
}

-- Add walnut project option
newoption {
   trigger     = 'with-walnut',
   description = 'Add Walnut project into your workspace/solution. '
}

-- Add OpenGL es
newoption{
    trigger     = 'gles',
    description = 'Use OpenGL ES  for rendering on desktop systems'
}

-- Add OpenGL es
newoption{
    trigger     = 'iOS',
    description = 'Build as iOS projects'
}

-- Android action
require 'android/androidmk/androidmk'
local android = require(  'android/android' )

local moduleWalnut = {

  -- Import config module
  config  = require 'config',
  -- Should build walnut library
  withWalnut  = _OPTIONS['with-walnut'],
  -- Use OpenGL ES on desktop systems
  withGLES = _OPTIONS['gles'],
  -- Package prefix
  package = _OPTIONS['package'] and _OPTIONS['package'] or 'com.yourCompany.project',
  -- Worksapce/solution name
  workspaceName = _OPTIONS['workspace'] and _OPTIONS['workspace'] or 'Workspace_Walnut'
}

function moduleWalnut.buildWorkspace( walnut )
    solution( walnut.workspaceName )
    configurations {'Debug','Release'}
    -- Set workspace output location

    local locationDir = walnut.config.outDir
    if _ACTION then locationDir = locationDir..'/'.._ACTION end
    if walnut.config.buildOS == 'android' then locationDir = walnut.config.outDir..'/android' end
    location(locationDir)
    if _ACTION == 'androidmk' then location(locationDir..'/jni') end


    -- C++11 and Unicode support
    flags {'C++11','Unicode'}
    -- Define __USE_GLES__
    if (not walnut.config.buildOS == 'macosx') and walnut.withGLES then defines {'__USE_GLES__'} end
    -- Configurations
    filter { 'configurations:Debug' }
      defines { 'DEBUG','_DEBUG'}
      flags {'Symbols'}
      targetdir ( locationDir..'/bin/Debug' )
      filter { 'kind:StaticLib or SharedLib' }
        targetdir ( locationDir..'/lib/Debug' )
    filter { 'configurations:Release' }
      defines { 'NDEBUG'}
      optimize 'On'
      targetdir ( locationDir..'/bin/Release' )
      filter { 'kind:StaticLib or SharedLib' }
        targetdir ( locationDir..'/lib/Release' )
    filter {}

    if walnut.config.buildOS == 'android' then
        local androidSln = require(walnut.config.home_dir..'/premake/android/androidSln')
        androidSln.attachSolution(locationDir..'/jni')
        android.walnut = walnut
    end

    -- Walnut library project
    if walnut.withWalnut then
      local libWalnut = require(walnut.config.home_dir..'/premake/libWalnut')
      libWalnut.addToWorkspace(locationDir)
    end

    project '*'
    files {}
end
return moduleWalnut
