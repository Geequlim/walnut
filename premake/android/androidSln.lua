local walnut = require '../walnut'
local androidSln = {}

function androidSln.attachSolution(locationDir)
  ndkplatform 'android-12'
  ndkstl 'gnustl_static'
  ndktoolchainversion '4.9'
  ndkabi 'armeabi x86 mips'
  rtti 'On'
  exceptionhandling "On"
  defines {'__ANDROID__'}

  -- Add MK files
  os.mkdir(locationDir)
  local app_mk = io.output( locationDir..'/Application.mk')
  io.write( 'include $(call my-dir)/'..walnut.workspaceName..'_Application.mk')
  io.flush()
  io.close()
  local android_mk=io.output(locationDir..'/Android.mk')
  io.flush() io.close()
end

return androidSln
