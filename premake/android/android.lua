local android = {
  walnut,
}
newaction {
  trigger = "android",
  description = "Generate Android project.",
  execute = function ()
    os.execute('premake5 androidmk '..( android.walnut.withWalnut and ' --with-walnut ' or '')
               .. ' --workspace='..android.walnut.workspaceName
               ..' --outdir='..android.walnut.config.outDir)
    android.buildProject(android.walnut.config.outDir..'/android')
  end
}
-- Build Android project
function android.buildProject(locationDir)
  -- AndroidManifest.xml
  os.mkdir( locationDir..'/src')
  local inputXML = android.walnut.config.home_dir..'/premake/android/AndroidManifest.xml'
  local hasSeenLabel = false;
  local outPutXML = ''
  for line in io.lines(inputXML) do
    if string.match(line,'android:label="Walnut"') then
        if hasSeenLabel then
           line = '            android:label="'..android.walnut.workspaceName..'"'
        else
           line = '        android:label="'..android.walnut.workspaceName..'"'
        end
        hasSeenLabel = true;
    elseif string.match(line,'package="com.example.walnut"') then
        line = '    package="'..android.walnut.package..'"'
    end
    outPutXML = outPutXML..(line..'\n')
  end
  local xmlOut,errOut = io.output( path.getabsolute(locationDir..'/AndroidManifest.xml'))
  if errOut then print('Open AndroidManifest.xml file failed:'..errOut) return  end
  io.write(outPutXML)
  io.flush() io.close()

  -- Add project files
  local sdkRoot = os.getenv('ANDROID_SDK_ROOT')
  if sdkRoot then
    os.execute( sdkRoot..'/tools/android list targets')
    print("----------")
    io.output(io.stdout)
    io.write 'Choose a target id from the list:'
    io.input(io.stdin)
    local target = io.read()
    local command = 'android update project --name '..android.walnut.workspaceName
                                ..' --target '..target..' --path '..path.getabsolute(locationDir )
    print( command )
    os.execute( sdkRoot..'/tools/'..command)
  else print 'ERROR:The environment ANDROID_SDK_ROOT not found.'
  end
end

return android;
