-- Walnut home directory
local walnut_home = os.getenv('WalnutHome')
-- Out put directory
local outputDir = path.getabsolute( require( walnut_home..'/premake/output' ) )
-- Configrations of walnut projects
local walnut = require( walnut_home..'/premake/walnut' )
walnut.config.outDir = outputDir
walnut.workspaceDir = outputDir
-- Add workspace/solution
walnut.buildWorkspace(outputDir)
